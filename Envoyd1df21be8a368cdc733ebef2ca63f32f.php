<?php $file = isset($file) ? $file : null; ?>
<?php $on = isset($on) ? $on : null; ?>
<?php $release = isset($release) ? $release : null; ?>
<?php $date = isset($date) ? $date : null; ?>
<?php $chmods = isset($chmods) ? $chmods : null; ?>
<?php $branch = isset($branch) ? $branch : null; ?>
<?php $repo = isset($repo) ? $repo : null; ?>
<?php $current = isset($current) ? $current : null; ?>
<?php $path = isset($path) ? $path : null; ?>
<?php $timezone = isset($timezone) ? $timezone : null; ?>
<?php $user = isset($user) ? $user : null; ?>
<?php
    // user on web-server
    $user = 'nik';

    $timezone = 'Europe/Moscow';

    // path to the directory on web-server
    $path = '/var/www/html/mySite';

    $current = $path. '/current';

    // where take your code (git repository)
    //    $repo = "hamsteripper@bitbucket.org/hamsteripper/mysite.git";

    $repo = "git@bitbucket.org:hamsteripper/mysite.git";

    $branch = 'master';


    // Directory and files with chmod
    $chmods = [
        'storage/logs'
    ];

    $date = new DateTime('now', new DateTimeZone($timezone));
    $release = $path .'/releases/'. $date->format('YmdHis');
?>

<?php $__container->servers(['production' => $user . '@80.211.184.112]); ?>


<?php $__container->startTask('clone', ['on' => $on]); ?>
    echo "#1 - Repository start"
    mkdir -p <?php echo $release; ?>


    git clone --depth 1 -b <?php echo $branch; ?> "<?php echo $repo; ?>" <?php echo $release; ?>


    echo "#1 - Repository has been cloned"
<?php $__container->endTask(); ?>

<?php $__container->startTask('composer', ['on' => $on]); ?>
    echo "#2 - Composer dependencies start"
    <?php /*composer self-update*/ ?>

    cd <?php echo $release; ?>


    composer install --no-interaction --no-dev --prefer-dist

    echo "#2 - Composer dependencies have been installed"
<?php $__container->endTask(); ?>

<?php $__container->startTask('artisan', ['on' => $on]); ?>
    echo "#3 - Production dependencies start";
    cd <?php echo $release; ?>


    ln -nfs <?php echo $path; ?>/.env .env;
    chgrp -h www-data .env;

    php artisan config:clear

    <?php /*php artisan migrate*/ ?>
    php artisan clear-compiled --env=production;
    <?php /*php artisan optimize --env=production;*/ ?>
    php artisan storage:link
    php artisan key:generate

    echo "#3 - Production dependencies have been installed";
<?php $__container->endTask(); ?>

<?php $__container->startTask('chmod', ['on' => $on]); ?>
    echo "#4 - Permission start"
    chgrp -R www-data <?php echo $release; ?>;
    chmod -R ug+rwx <?php echo $release; ?>;

    <?php foreach($chmods as $file): ?>
        chmod -R 775 <?php echo $release; ?>/<?php echo $file; ?>

        chown -R <?php echo $user; ?>:www-data <?php echo $release; ?>/<?php echo $file; ?>

        echo "Permission have been set for <?php echo $file; ?>"
    <?php endforeach; ?>

    echo "#4 - Permission has been set"
<?php $__container->endTask(); ?>

<?php $__container->startTask('update_symlinks'); ?>
    ln -nfs <?php echo $release; ?> <?php echo $current; ?>;
    chgrp -h www-data <?php echo $current; ?>;

    echo "#4 - Symlink has been set"
<?php $__container->endTask(); ?>

<?php $__container->startMacro('deploy', ['on' => 'production']); ?>
    clone
    composer
    artisan
    chmod
    update_symlinks
<?php $__container->endMacro(); ?>
