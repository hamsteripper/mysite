CREATE database IF NOT EXISTS mySite;
drop user 'mySiteUser'@'localhost';
CREATE USER IF NOT EXISTS 'mySiteUser'@'localhost' IDENTIFIED BY 'mySiteUser';
ALTER user 'mySiteUser'@'localhost' identified with mysql_native_password by 'mySiteUser';
GRANT ALL PRIVILEGES ON mySite.* TO 'mySiteUser'@'localhost';
FLUSH PRIVILEGES;
use mySite;
