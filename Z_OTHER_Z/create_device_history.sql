use mySite;

DROP TABLE IF EXISTS DH_device_history;
DROP TABLE IF EXISTS DH_device;
DROP TABLE IF EXISTS DH_counterparty;
DROP TABLE IF EXISTS DH_model_instructions_binding;
DROP TABLE IF EXISTS DH_model_instructions;
DROP TABLE IF EXISTS DH_model_specifications;
DROP TABLE IF EXISTS DH_model;
DROP TABLE IF EXISTS DH_model_manufacturer;
DROP TABLE IF EXISTS DH_type;

# Создание таблицы тип устройства
CREATE TABLE DH_type(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    type VARCHAR(20) NOT NULL UNIQUE
);

# Создание таблицы производитель
CREATE TABLE DH_model_manufacturer(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) 
);

# Создание таблицы модель
CREATE TABLE DH_model(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    model_manufacturer_id BIGINT NOT NULL,
    type_id BIGINT NOT NULL,
    FOREIGN KEY (model_manufacturer_id) REFERENCES DH_model_manufacturer (id),
    FOREIGN KEY (type_id) REFERENCES DH_type (id)
);

# Создание таблицы характеристики
CREATE TABLE DH_model_specifications(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    print_speed VARCHAR(20) NOT NULL,
    model_id BIGINT NOT NULL,
    FOREIGN KEY (model_id) REFERENCES DH_model (id)
);

# Создание таблицы инструкции
CREATE TABLE DH_model_instructions(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    file_path VARCHAR(255) NOT NULL
);

# Создание таблицы связующей таблицы для инструкции и модели
CREATE TABLE DH_model_instructions_binding(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    model_id BIGINT NOT NULL,
    model_instructions_id BIGINT NOT NULL,
    FOREIGN KEY (model_id) REFERENCES DH_model (id),
    FOREIGN KEY (model_instructions_id) REFERENCES DH_model_instructions (id)
);

# Создание таблицы контрагентов
CREATE TABLE DH_counterparty(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    address VARCHAR(20) NOT NULL
);

# Создание таблицы аппаратов
CREATE TABLE DH_device(
    #id BIGINT PRIMARY KEY AUTO_INCREMENT,
    serial_number VARCHAR(20) NOT NULL,
    model_id BIGINT NOT NULL,
    CONSTRAINT id PRIMARY KEY (serial_number, model_id),
    type_id BIGINT NOT NULL,
    counterparty_id BIGINT NOT NULL,
    FOREIGN KEY (model_id) REFERENCES DH_model (id),
    FOREIGN KEY (type_id) REFERENCES DH_type (id),
    FOREIGN KEY (counterparty_id) REFERENCES DH_counterparty (id)    
);

# Создание таблицы история
CREATE TABLE DH_device_history(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    namber_of_request VARCHAR(20),
    commentation VARCHAR(255),
    file_path VARCHAR(255),
    device_serial_number VARCHAR(255) NOT NULL,
    model_id BIGINT NOT NULL,
    FOREIGN KEY (device_serial_number, model_id) REFERENCES DH_device (serial_number, model_id)
);

