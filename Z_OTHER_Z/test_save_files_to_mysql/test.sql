use mySite;
CREATE TABLE `products` (product_id BIGINT PRIMARY KEY AUTO_INCREMENT, product_name VARCHAR(50), price DOUBLE, product_image BLOB, product_mime VARCHAR(50)) ENGINE = InnoDB;
