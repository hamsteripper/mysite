<?php
namespace App\Classes\Authorization;
use Dlnsk\HierarchicalRBAC\Authorization;

use Illuminate\Http\Request;
use App\User;

class AuthorizationClass extends Authorization
{
    public function getPermissions() {
        return [
            'Access_Level_1' => [
                // Необязательное свойство "описание"
                'description' => 'Уровень доступа 1',
                // Используется для создания цепи (иерархии) операций
                'next' => 'Access_Level_2',
            ],
            'Access_Level_2' => [
                'description' => 'Уровень доступа 2',
                    'next' => 'Access_Level_3',
            ],
            'Access_Level_3' => [
                'description' => 'Уровень доступа 3',
                // Здесь цепь заканчивается
            ],
            // Избранное
            'Other_Access_Level' => [
                'description' => 'Отдальный доступ',
            ],
            // Удаление
            'Access_Guest' => [
                'description' => 'Уровень доступа гость, такой-же как и уровень доступа 1',
                'equal' => 'Access_Level_1',  // Применяем правила аналогичные редактированию
            ],
        ];
    }

    public function getRoles() {
        return [
            'guest' => [
                'Access_Guest',
            ],
            'user' => [
                'Access_Level_1',
            ],
            'logistician' => [
                'Access_Level_2',
            ],
            'admin' => [
                'Access_Level_3',
                'Other_Access_Level',
            ],
        ];
    }

    /// FIX
    ////////////// Callbacks ///////////////
//    public function updatePostInCategory($user, $post) {
//        // Данный метод возвращает модель в случае, если $post содержит id модели
//        $post = $this->getModel(\App\User::class, $post);
//
//        return $user->category_id === $post->category_id;
//    }
//
//    public function updateOwnPost($user, $post) {
//        $post = $this->getModel(\App\User::class, $post);
//
//        return $user->id === $post->user_id;
//    }
}