<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $post = new post();
//        $post = Post::firstOrCreate(['title' => $request->title, 'body' => $request->body])
//        $this->authorize('update-post-in-category');
//        $this->authorize('Access_Level_1');
        return view('home');
    }
}
