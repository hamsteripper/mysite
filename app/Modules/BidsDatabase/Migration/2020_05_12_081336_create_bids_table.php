<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('engineer_id')->unsigned()->nullable(false);
            $table->bigInteger('manager_id')->unsigned()->nullable(false);

            $table->longText('files')->nullable(true);
            $table->longText('data')->nullable(true);
            $table->longText('comment')->nullable(true);
            $table->string('status')->nullable(true);
            $table->date('date')->nullable(false);
            $table->timestamps();

            $table->foreign('engineer_id')->references('id')->on('users');
            $table->foreign('manager_id')->references('id')->on('users');

//            $timestamps = f;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
