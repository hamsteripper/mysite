<?php


Route::group(
    [
        'prefix' => 'laravel-crud-search-sort-ajax',
        'namespace' => 'App\Modules\Crud_1\Controllers',
        'middleware' => ['web','auth'],
    ],
    function () {
        Route::get('/', 'Crud3Controller@index');
        Route::match(['get', 'post'], 'create', 'Crud3Controller@create');
        Route::match(['get', 'put'], 'update/{id}', 'Crud3Controller@update');
        Route::delete('delete/{id}', 'Crud3Controller@delete');
    }
);