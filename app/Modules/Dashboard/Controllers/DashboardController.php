<?php

namespace App\Modules\Dashboard\Controllers;

use App\Http\Controllers\Controller;
//use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

//use Illuminate\Support\Facades\Log;
use Session;

class DashboardController extends Controller{

    public function index(){
        Session::put('cpu_last_sum', 0);
        Session::put('cpu_last', array());
//        session('cpu_last_sum');
        return view('Dashboard::index');
    }

    public function serverStatus(Request $request){

        // Memory
        $fh = fopen('/proc/meminfo','r');
        $memAvailable = 0;
        $memTotal = 0;
        $check = 0;
        while ($line = fgets($fh)) {
            $pieces = array();
            if (preg_match('/^MemAvailable:\s+(\d+)\skB$/', $line, $pieces)) {
                $memAvailable = $pieces[1];
                $check++;
            }
            if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $pieces)) {
                $memTotal = $pieces[1];
                $check++;
            }

            if($check == 2) break;
        }
        fclose($fh);


        //CPU
        $cpu_now = array();
        $fh = fopen('/proc/stat','r');
        for($i = 0; $i < 5; $i++){
            $line = fgets($fh);
            $array = preg_split("/[\s]+/", $line);
            unset($array[0]);
            if (($key = array_search("", $array)) !== false) {
                unset($array[$key]);
            }
            array_push($cpu_now, $array);
        }
        fclose($fh);

        $cpu_last_sum = Session::get('cpu_last_sum'); // число
        $cpu_last = Session::get('cpu_last'); // Двумерный массив
        $cpu_sum = array_sum($cpu_now[0]);
        $cpu_delta = $cpu_sum - $cpu_last_sum;

        $cpu_idle = $cpu_now[0][4] - 0;
        if(!empty($cpu_now[0]) && !empty($cpu_last[0])){
            $cpu_idle = $cpu_now[0][4] - $cpu_last[0][4];
        }
        $cpu_used = $cpu_delta - $cpu_idle;
        $cpu_usage = 100 * ($cpu_used / $cpu_delta);
        Session::put('cpu_last', $cpu_now);
        Session::put('cpu_last_sum', $cpu_sum);

        //CPU info
        $cpuget = file('/proc/cpuinfo');
        $cpu["name"]    =   str_replace("           ", " ",substr(str_replace("model name	: ", "", $cpuget[4]),0,-1));
        $cpu["MHz"]    =   round(str_replace("           ", " ",substr(str_replace("cpu MHz		: ", "", $cpuget[7]),0,-1)), 0);
        $m_array = preg_grep('/^processor\t:\s.*/', $cpuget);
        $cpu["cores"] = count($m_array);
        $cpu["cache"]   =   substr(str_replace("cache size  : ", "", $cpuget[8]),0,-1);

        return response()->json([
            'cpuPercent' => round($cpu_usage, 2),
            'memPercent' => round(($memTotal - $memAvailable) / $memTotal, 2),
            'cpuInfo' => $cpu,
            'totalMemory' => round($memTotal / 1024, 0)
        ]);
    }

}