<?php

Route::group(
    [
        'namespace' => 'App\Modules\Dashboard\Controllers',
        'middleware' => ['web','auth'],
        'as' => 'dashboard.',
    ],
    function(){
        Route::get('/dashboard', ['uses' => 'DashboardController@index']);
        Route::post('/serverStatus', ['uses' => 'DashboardController@serverStatus'])->name('serverStatus');
    }
);

