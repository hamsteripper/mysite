@extends('layouts.mainBlade')

@section('content1')
    <div class="row mt">
        <!-- SERVER STATUS PANELS -->
        <div class="col-md-6 col-sm-6 mb">
            <div class="grey-panel donut-chart">
                <div class="grey-header">
                    <h5 id="CPU_INFO">CPU USED</h5>
                </div>
                <canvas id="CPU_USED"></canvas>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 mb">
            <div class="grey-panel donut-chart">
                <div class="grey-header">
                    <h5 id="MEMORY_INFO">MEMORY USED</h5>
                </div>
                <canvas id="MEMORY_USED"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('rightBar1')
    <div id="calendar" class="mb">
        <div class="panel green-panel no-margin">
            <div class="panel-body">
                <div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                    <div class="arrow"></div>
                    <h3 class="popover-title" style="disadding: none;"></h3>
                    <div id="date-popover-content" class="popover-content"></div>
                </div>
                <div id="my-calendar"></div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!--statuses scripts-->
    <script src="resource/lib/zabuto_calendar.js"></script>
    <!--statuses scripts-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{ asset('assets/dynamicChart.js') }}"></script>
    <!---->
    <link rel="stylesheet" type="text/css" href="{{ asset('resource/css/zabuto_calendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resource/lib/gritter/css/jquery.gritter.css')}}" />

    <script type="application/javascript">
        $(document).ready(function() {
            $("#date-popover").popover({
                html: true,
                trigger: "manual"
            });
            $("#date-popover").hide();
            $("#date-popover").click(function(e) {
                $(this).hide();
            });

            $("#my-calendar").zabuto_calendar({
                action: function() {
                    return myDateFunction(this.id, false);
                },
                action_nav: function() {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [{
                    type: "text",
                    label: "Special event",
                    badge: "00"
                },
                    {
                        type: "block",
                        label: "Regular event",
                    }
                ]
            });

            // PC statuses
            var canvases = new Map();
            canvases.set('memory', 'MEMORY_USED');
            canvases.set('cpu', 'CPU_USED');
            canvases.set('cpuInfo', 'CPU_INFO');
            canvases.set('memoryInfo', 'MEMORY_INFO');

            dynamicChartsCreate(canvases);

        });

        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
        }
    </script>

@endsection
