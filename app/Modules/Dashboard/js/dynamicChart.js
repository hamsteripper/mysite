function start(charts, data){

    // memory update
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    charts[0].data.datasets[0].data.push(data.memPercent * 100);
    charts[0].data.datasets[0].data.shift();
    charts[0].update();
    // cpu update
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    charts[1].data.datasets[0].data.push(data.cpuPercent);
    charts[1].data.datasets[0].data.shift();
    charts[1].update();

    // console.log(data.cpuInfo.name);

    // document.getElementById("CPU_INFO").innerHTML = data.cpuInfo.name + "sss";
}

function getServerStatus(charts)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "/serverStatus",
        type: 'post',
        data: "",
        dataType: 'json',
        success: function (data){
            start(charts, data);
        }
    });
}

function createLine(canvasName) {
    var labelsArray = [];
    var dataArray = [];
    var labelsBackgroundArray = [];
    for (var i = 0; i < 23; i++) {
        labelsArray[i] = '';
        dataArray[i] = 0;
        labelsBackgroundArray[i] = 'rgba(' + Math.random() * 255 + ',' + Math.random() * 255 + ',' + Math.random() * 255 + ',' + '0.2' + ')';
    }

    var ctx = document.getElementById(canvasName).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labelsArray,
            datasets: [{
                label: '# used',
                data: dataArray,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: labelsBackgroundArray,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max: 100
                    }
                }]
            }
        }
    });

    return myChart;
}

var setInnerHTML = function (canvases) {



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "/serverStatus",
        type: 'post',
        data: "",
        dataType: 'json',
        success: function (data){
            if(canvases.get("cpuInfo") !== undefined) {
                var element = document.getElementById(canvases.get("cpuInfo"));
                element.innerHTML = data.cpuInfo.name + "<br>" + "Сores used : " + data.cpuInfo.cores + "<br>" + "Core frequency : " + data.cpuInfo.MHz;
            }
            if(canvases.get("memoryInfo") !== undefined) {
                var element = document.getElementById(canvases.get("memoryInfo"));
                element.innerHTML = "Total memory : " + data.totalMemory;
            }
        }
    });
}

var dynamicChartsCreate = function(canvases) {
    var memory = createLine(canvases.get('memory'));
    var cpu = createLine(canvases.get('cpu'));
    var charts = [memory, cpu];

    //setInnerHTML
    setInnerHTML(canvases);
    // setInnerHTML(canvases.get('memoryInfo'));

    setInterval(function () {
        getServerStatus(charts);
    }, 2000);
}