<?php

namespace App\Modules\DevicesDatabase\Controllers;

//use App\Modules\Crud_1\Models\Customer;
//use App\Modules\DevicesDatabase\Models\dh_model;
use App\Modules\DevicesDatabase\Models\dh_counterparty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class DeviceCounterparties extends Controller {

    public function index(Request $request){

        $request->session()->put('searchCounterparty', $request->has('searchCounterparty') ? $request->get('searchCounterparty') : ($request->session()->has('searchCounterparty') ? $request->session()->get('searchCounterparty') : ''));
        $request->session()->put('counterparty_name', $request->has('counterparty_name') ? $request->get('counterparty_name') : ($request->session()->has('counterparty_name') ? $request->session()->get('counterparty_name') : ''));
        $request->session()->put('counterparty_field', $request->has('counterparty_field') ? $request->get('counterparty_field') : ($request->session()->has('counterparty_field') ? $request->session()->get('counterparty_field') : 'counterparty_name'));
        $request->session()->put('counterparty_sort', $request->has('counterparty_sort') ? $request->get('counterparty_sort') : ($request->session()->has('counterparty_sort') ? $request->session()->get('counterparty_sort') : 'asc'));

        $dh_counterparties = new dh_counterparty();
        if ($request->session()->get('counterparty_name') != -1)
            $dh_counterparties = $dh_counterparties
                ->where('counterparty_name', 'like', '%' . $request->session()->get('searchCounterparty') . '%')
                ->orderBy($request->session()->get('counterparty_field'), $request->session()->get('counterparty_sort'))
                ->paginate(14);

        if ($request->ajax())
            return view("DevicesDatabase::deviceCounterparties.index", compact('dh_counterparties', $dh_counterparties));
        else
            return view("DevicesDatabase::deviceCounterparties.ajax", compact('dh_counterparties', $dh_counterparties));
    }

    public function create(Request $request){
        if ($request->isMethod('get')) {
            return view('DevicesDatabase::deviceCounterparties.form');
        }else {
            $rules = [
                'counterparty_name' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_counterparty = new dh_counterparty();
            $dh_counterparty->counterparty_name = $request->counterparty_name;
            $dh_counterparty->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/deviceCounterparties')
            ]);
        }
    }

    public function delete($id){
        dh_counterparty::destroy($id);
        return redirect('devicesDatabase/deviceCounterparties');
    }

    public function update(Request $request, $id)
    {

        if ($request->isMethod('get')){
            return view('DevicesDatabase::deviceCounterparties.form', ['dh_counterparty' => dh_counterparty::find($id)]);
        }else {
            $rules = [
                'counterparty_name' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_counterparty = dh_counterparty::find($id);
            $dh_counterparty->counterparty_name = $request->counterparty_name;
            $dh_counterparty->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/deviceCounterparties')
            ]);
        }
    }

}