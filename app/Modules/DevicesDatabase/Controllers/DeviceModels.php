<?php

namespace App\Modules\DevicesDatabase\Controllers;

//use App\Modules\Crud_1\Models\Customer;
use App\Modules\DevicesDatabase\Models\dh_model;
use App\Modules\DevicesDatabase\Models\dh_trademark;
use App\Modules\DevicesDatabase\Models\dh_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class DeviceModels extends Controller {

    public function index(Request $request){

        $request->session()->put('searchModel', $request->has('searchModel') ? $request->get('searchModel') : ($request->session()->has('searchModel') ? $request->session()->get('searchModel') : ''));
        $request->session()->put('model_name', $request->has('model_name') ? $request->get('model_name') : ($request->session()->has('model_name') ? $request->session()->get('model_name') : ''));
        $request->session()->put('model_field', $request->has('model_field') ? $request->get('model_field') : ($request->session()->has('model_field') ? $request->session()->get('model_field') : 'model_name'));
        $request->session()->put('model_sort', $request->has('model_sort') ? $request->get('model_sort') : ($request->session()->has('model_sort') ? $request->session()->get('model_sort') : 'asc'));


        $dh_models = new dh_model();
        if ($request->session()->get('model_name') != -1) {

            $searchModel = $request->session()->get('searchModel');
            $model_sort = $request->session()->get('model_sort');
            $model_field = $request->session()->get('model_field');

            $dh_models = $dh_models
                ->where('model_name', 'like', '%' . $searchModel . '%')
                ->orWhereHas('types', function ($subquery) use ($searchModel) {
                    $subquery->where('type', 'like', "%{$searchModel}%"); // Это относится к таблице с типами
                })
                ->orWhereHas('trademark', function ($subquery) use ($searchModel) {
                    $subquery->where('trademark_name', 'like', "%{$searchModel}%"); // Это относится к таблице с марками
                })
//                ->orderBy($model_field, $model_sort)
                ->paginate(14);

        }


        if ($request->ajax())
            return view("DevicesDatabase::deviceModels.index", compact('dh_models'));
        else
            return view("DevicesDatabase::deviceModels.ajax", compact('dh_models'));
    }

    public function create(Request $request){
        if ($request->isMethod('get')) {
            $types = dh_type::all();
            $trademarks = dh_trademark::all();
            return view('DevicesDatabase::deviceModels.form')->with('types', $types)->with('trademarks', $trademarks);
        }else {
            $rules = [
                'model_name' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_models = new dh_model();
            $dh_models->model_name = $request->model_name;
            $dh_models->type_id = $request->type_id;
            $dh_models->trademark_id = $request->trademark_id;
            $dh_models->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/deviceModels')
            ]);
        }
    }

    public function delete($id){
        dh_model::destroy($id);
        return redirect('devicesDatabase/deviceModels');
    }

    public function update(Request $request, $id)
    {

        if ($request->isMethod('get')){
            $types = dh_type::all();
            $trademarks = dh_trademark::all();
            return view('DevicesDatabase::deviceModels.form', ['dh_model' => dh_model::find($id)])->with('types', $types)->with('trademarks', $trademarks);
        }else {
            $rules = [
                'model_name' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_model = dh_model::find($id);
            $dh_model->model_name = $request->model_name;
            $dh_model->type_id = $request->type_id;
            $dh_model->trademark_id = $request->trademark_id;
            $dh_model->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/deviceModels')
            ]);
        }
    }

}