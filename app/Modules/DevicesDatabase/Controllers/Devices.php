<?php

namespace App\Modules\DevicesDatabase\Controllers;

use App\Modules\DevicesDatabase\Models\dh_model;
use App\Modules\DevicesDatabase\Models\dh_device;
use App\Modules\DevicesDatabase\Models\dh_type;
use App\Modules\DevicesDatabase\Models\dh_counterparty;
use App\Modules\DevicesDatabase\Models\dh_trademark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;


class Devices extends Controller {

    public function index(Request $request){
        $request->session()->put('searchSN', $request->has('searchSN') ? $request->get('searchSN') : ($request->session()->has('searchSN') ? $request->session()->get('searchSN') : ''));
        $request->session()->put('SN_field', $request->has('SN_field') ? $request->get('SN_field') : ($request->session()->has('SN_field') ? $request->session()->get('SN_field') : 'SN'));
        $request->session()->put('SN_sort', $request->has('SN_sort') ? $request->get('SN_sort') : ($request->session()->has('SN_sort') ? $request->session()->get('SN_sort') : 'asc'));

        $dh_devices = new dh_device();
        if ($request->session()->get('SN') != -1)
            $dh_devices = $dh_devices->where('SN', 'like', '%' . $request->session()->get('searchSN') . '%')
            ->orderBy($request->session()->get('SN_field'), $request->session()->get('SN_sort'))
            ->paginate(14);
        if ($request->ajax())
            return view("DevicesDatabase::devices.index", compact('dh_devices'));
        else
            return view("DevicesDatabase::devices.ajax", compact('dh_devices'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            $types = dh_type::all();
            $models = dh_model::all();
            $counterparties = dh_counterparty::all();
            return view('DevicesDatabase::devices.form')->with('types', $types)->with('models', $models)->with('counterparties', $counterparties);
        }else {
            $rules = [
                'SN' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_devices = new dh_device();
            $dh_devices->SN = $request->SN;
            $dh_devices->address = $request->address;
            $dh_devices->model_id = $request->model_id;
            //adfasfa
//            $dh_devices->type_id = $request->type_id;
            $dh_devices->counterparty_id = $request->counterparty_id;
//            $dh_devices->trademark_id = $request->trademark_id;
            $dh_devices->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/devices')
            ]);
        }
    }

    public function delete($id)
    {
        dh_device::destroy($id);
        return redirect('devicesDatabase/devices');
    }

    public function update(Request $request, $id)
    {
        if ($request->isMethod('get')){
            $models = dh_model::all();
            $counterparties = dh_counterparty::all();
            return view('DevicesDatabase::devices.form', ['dh_devices' => dh_device::find($id)])->with('models', $models)->with('counterparties', $counterparties);
        }else {
            $rules = [
                'SN' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_devices = dh_device::find($id);
            $dh_devices->SN = $request->SN;
            $dh_devices->address = $request->address;
            $dh_devices->model_id = $request->model_id;
//            $dh_devices->type_id = $request->type_id;
            $dh_devices->counterparty_id = $request->counterparty_id;
            $dh_devices->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/devices')
            ]);
        }
    }

}