<?php

namespace App\Modules\DevicesDatabase\Controllers;

//use App\Modules\Crud_1\Models\Customer;
use App\Modules\DevicesDatabase\Models\dh_model;
use App\Modules\DevicesDatabase\Models\dh_trademark;
use App\Modules\DevicesDatabase\Models\dh_type;
use App\Modules\DevicesDatabase\Models\dh_history;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Zip;
use Illuminate\Support\Facades\Response;
use Exception;

class DevicesHistory extends Controller {

//    const CONSTANT = 'значение константы';

    public function show(Request $request){

    }

    public function index(Request $request){

        // Для первого вхождения bp devices
        if($request->device_id !== NULL){
            $request->session()->put('device_id', $request->device_id);
        }

        $request->session()->put('searchDevicesHistory', $request->has('searchDevicesHistory') ? $request->get('searchDevicesHistory') : ($request->session()->has('searchDevicesHistory') ? $request->session()->get('searchDevicesHistory') : ''));
        $request->session()->put('device_id', $request->has('device_id') ? $request->get('device_id') : ($request->session()->has('device_id') ? $request->session()->get('device_id') : ''));
        $request->session()->put('devicesHistory_field', $request->has('devicesHistory_field') ? $request->get('devicesHistory_field') : ($request->session()->has('devicesHistory_field') ? $request->session()->get('devicesHistory_field') : 'devicesHistory_name'));
        $request->session()->put('model_sort', $request->has('model_sort') ? $request->get('model_sort') : ($request->session()->has('model_sort') ? $request->session()->get('model_sort') : 'asc'));

        $dh_histories = new dh_history();
        if ($request->session()->get('device_id') != -1) {
            $dh_histories = $dh_histories->where('device_id', '=', $request->session()->get('device_id'))->paginate(5);
        }

        if ($request->ajax())
            return view("DevicesDatabase::devicesHistory.index", compact('dh_histories'));
        else
            return view("DevicesDatabase::devicesHistory.ajax", compact('dh_histories'));
    }

    public function create(Request $request){

        if ($request->isMethod('get')) {
            return view('DevicesDatabase::devicesHistory.form');
        }else {
            $rules = [
                'device_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);

            // Обработка файла
            // Путь к файлам
            $path = storage_path();
            $path = $path."/app/public/deviceDatabese/devicesHistory/".$request->session()->get('device_id')."/";
            // Создание директории
            $this->createDir($path);
            // Обработка файлов
            $files = $request->file('file');
            $filesName = array();
            if(!empty($files))
                foreach ($files as $file){
                    $a = strripos( $file->getClientOriginalName(), ".");
                    $extension = substr($file->getClientOriginalName(), $a+1, strlen($file->getClientOriginalName())-1);
                    $tmpFileName = uniqid(strftime('%G-%m-%d'));
                    $NewfileName = $tmpFileName.".".$extension;
                    $filesName[] = $NewfileName;
                    $this->saveFilesToFolder($path, $file, $NewfileName);
                }
            // Проверка на присутствие элементов
            if(empty($request->historyComment) && empty($files))
                return response()->json([
                    'fail' => false,
                    'redirect_url' => url('devicesDatabase/devicesHistory?device_id='.$request->session()->get('device_id'))
                ]);

            // Запись данных в базу данных
            $dh_history = new dh_history();
            $dh_history->device_id = request()->session()->get('device_id');
            $dh_history->comment = $request->historyComment;
            $dh_history->file_path = json_encode($filesName);
            $dh_history->date = $request->date;

            try {
                $dh_history->save();
            }catch (Exception $e) {
                info($e);
                return response()->json([
                    'fail' => true,
                    'errors' => $e
                ]);
            }

            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/devicesHistory?device_id='.$request->session()->get('device_id'))
            ]);
        }
    }

    private function saveFilesToFolder($path, $file, $NewfileName){
        $file->move($path, $NewfileName);
    }

    // Место хранения файлов
    private function createDir($path){
        // Создание директорий и поддиректорий
        if(!File::exists($path)){
            File::makeDirectory($path, 0775, true, true);
        }
    }

    public function delete($device_id, $id){
        // Путь к файлам
        $folderPath = storage_path();
        $folderPath = $folderPath."/app/public/deviceDatabese/devicesHistory/$device_id/";
        $dh_histories = new dh_history();
        $dh_histories = $dh_histories->where('id', '=', $id)->get();
        $paths = json_decode($dh_histories[0]->file_path);
        foreach ($paths as $path){
            File::delete($folderPath.$path);
        }
        dh_history::destroy($id);
        return redirect('devicesDatabase/devicesHistory?device_id='.$device_id);
    }


    public function addFile(Request $request, $device_id){
        $a = 0;
    }

    // Возвращает архив с файлами
    public function getFiles(Request $request, $device_id){
        if ($request->isMethod('get')) {

            $path = storage_path();
            $folderPath = $path."/app/public/deviceDatabese/devicesHistory/$device_id";
            $zipPath = $folderPath."/file.zip";

            $dh_histories = new dh_history();
            $dh_histories = $dh_histories->where('id', '=', $request->dh_history_id)->get();
            $paths = json_decode($dh_histories[0]->file_path);
            $zip = Zip::create($zipPath);
            foreach ($paths as $path){
                $zip->add("$folderPath/$path");
            }

            $zip->close();

            $headers = array(
                "Content-Type" => "application/zip",
            );

            return Response::download($zipPath, "file.zip", $headers)->deleteFileAfterSend(true);

        }else{
            return response()->json([
                'fail' => true,
                'redirect_url' => url('devicesDatabase/devicesHistory?device_id=' . $device_id)
            ]);
        }
    }

    public function update(Request $request, $id)
    {

//        if ($request->isMethod('get')){
//            $types = dh_type::all();
//            $trademarks = dh_trademark::all();
//            return view('DevicesDatabase::devicesHistory.form', ['dh_model' => dh_model::find($id)])->with('types', $types)->with('trademarks', $trademarks);
//        }else {
//            $rules = [
//                'model_name' => 'required',
//            ];
//            $validator = Validator::make($request->all(), $rules);
//            if ($validator->fails())
//                return response()->json([
//                    'fail' => true,
//                    'errors' => $validator->errors()
//                ]);
////            $dh_histories = $dh_histories
////                ->where('device_id', '=', $request->device_id)
//            $dh_history = dh_history::find($id);
//            $dh_history->model_name = $request->model_name;
////            $dh_history->type_id = $request->type_id;
////            $dh_history->trademark_id = $request->trademark_id;
//            $dh_history->save();
//            return response()->json([
//                'fail' => false,
//                'redirect_url' => url('devicesDatabase/deviceModels')
//            ]);
//        }
    }

}