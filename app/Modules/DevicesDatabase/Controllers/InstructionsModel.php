<?php

namespace App\Modules\DevicesDatabase\Controllers;

//use App\Modules\Crud_1\Models\Customer;
use App\Modules\DevicesDatabase\Models\dh_model;
//use App\Modules\DevicesDatabase\Models\dh_trademark;
//use App\Modules\DevicesDatabase\Models\dh_type;
//use App\Modules\DevicesDatabase\Models\dh_history;
use App\Modules\DevicesDatabase\Models\dh_instruction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Zip;
use Illuminate\Support\Facades\Response;

class InstructionsModel extends Controller {

//    const CONSTANT = 'значение константы';

    public function show(Request $request){

    }

    public function index(Request $request){

        $request->session()->put('model_id', $request->has('model_id') ? $request->get('model_id') : ($request->session()->has('model_id') ? $request->session()->get('model_id') : ''));
        $request->session()->put('searchInstructionsText', $request->has('searchInstructionsText') ? $request->get('searchInstructionsText') : ($request->session()->has('searchInstructionsText') ? $request->session()->get('searchInstructionsText') : ''));

        // Для первого вхождения model_id
        if($request->model_id !== NULL){
            $request->session()->put('model_id', $request->model_id);
        }

        $dh_models = new dh_model();
        $dh_instructions = null;
        if ($request->session()->get('model_id') != -1) {
            $dh_models = $dh_models->where('id', '=', $request->session()->get('model_id'))->get();
            $dh_instructions = $dh_models[0]->instructions()->where('model_id', '=', $request->session()->get('model_id'))->paginate(5);
        }

        if ($request->ajax())
            return view("DevicesDatabase::instructionsModel.index", compact('dh_instructions'));
        else
            return view("DevicesDatabase::instructionsModel.ajax", compact('dh_instructions'));
    }

    public function create(Request $request){

        if ($request->isMethod('get')) {
            return view('DevicesDatabase::instructionsModel.form');
        }else {

            $rules = [
                'model_id' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);

            $a = 0;

            // Путь к файлам
            $path = storage_path();
            $path = $path."/app/public/deviceDatabese/instructionsForModels/".$request->session()->get('model_id')."/";
            // Создание директории
            $this->createDir($path);
            // Обработка файлов
            $files = $request->file('file');
            $filesName = array();
            if(!empty($files))
                foreach ($files as $file){
                    $a = strripos( $file->getClientOriginalName(), ".");
                    $extension = substr($file->getClientOriginalName(), $a+1, strlen($file->getClientOriginalName())-1);
                    $tmpFileName = uniqid(strftime('%G-%m-%d'));
                    $NewfileName = $tmpFileName.".".$extension;
                    $filesName[] = $NewfileName;
                    $this->saveFilesToFolder($path, $file, $NewfileName);
                }

            if(empty($request->instructionText) && empty($files))
                return response()->json([
                    'fail' => false,
                    'redirect_url' => url('devicesDatabase/instructionsModel?model_id='.$request->session()->get('model_id'))
                ]);

            $dh_model = dh_model::find($request->session()->get('model_id'));

            $dh_instruction = new dh_instruction();
            $dh_instruction->text = $request->instructionText;
            $dh_instruction->file_path = json_encode($filesName);
            $dh_instruction->save();

            try {
                $dh_model->instructions()->save($dh_instruction);
            }catch (\Exception $e){
                info($e);
                return response()->json([
                    'fail' => true,
                    'errors' => $e
                ]);
            }


            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/instructionsModel?model_id='.$request->session()->get('model_id'))
            ]);
        }
    }

    private function saveFilesToFolder($path, $file, $NewfileName){
        $file->move($path, $NewfileName);
    }

    // Место хранения файлов
    private function createDir($path){
        // Создание директорий и поддиректорий
        if(!File::exists($path)){
            File::makeDirectory($path, 0775, true, true);
        }
    }

    public function delete($instruction_id, $id){
        // Путь к файлам
        $folderPath = storage_path();
        $folderPath = $folderPath."/app/public/deviceDatabese/instructionsForModels/$instruction_id/";
        // Текущий history
        $dh_instruction = dh_instruction::find($id);
        // Удаление промежуточных связей
        $dh_instruction->model()->detach($instruction_id);
        // Удаление файлов
        $paths = json_decode($dh_instruction->file_path);
        foreach ($paths as $path){
            File::delete($folderPath.$path);
        }
        dh_instruction::destroy($id);
        return redirect('devicesDatabase/instructionsModel?model_id='.$instruction_id);
    }


    public function addFile(Request $request, $device_id){
//        $a = 0;
    }

    // Возвращает архив с файлами
    public function getFiles(Request $request, $model_id){
        if ($request->isMethod('get')) {

            $s = $model_id;
            $s1 = $request->dh_instruction_id;
            $a = 1;
            $folderPath = storage_path();
            $folderPath = $folderPath."/app/public/deviceDatabese/instructionsForModels/$model_id/";
            $zipPath = $folderPath."/file.zip";

            $dh_instruction = new dh_instruction();
            $dh_instruction = $dh_instruction->where('id', '=', $request->dh_instruction_id)->get();
            $paths = json_decode($dh_instruction[0]->file_path);
            $zip = Zip::create($zipPath);
            foreach ($paths as $path){
                $zip->add("$folderPath/$path");
            }

            $zip->close();

            $headers = array(
                "Content-Type" => "application/zip",
            );

            return Response::download($zipPath, "file.zip", $headers)->deleteFileAfterSend(true);
        }else{
            return response()->json([
                'fail' => true,
                'redirect_url' => url('devicesDatabase/instructionsModel?model_id=' . $model_id)
            ]);
        }
    }

    public function update(Request $request, $id)
    {

//        if ($request->isMethod('get')){
//            $types = dh_type::all();
//            $trademarks = dh_trademark::all();
//            return view('DevicesDatabase::devicesHistory.form', ['dh_model' => dh_model::find($id)])->with('types', $types)->with('trademarks', $trademarks);
//        }else {
//            $rules = [
//                'model_name' => 'required',
//            ];
//            $validator = Validator::make($request->all(), $rules);
//            if ($validator->fails())
//                return response()->json([
//                    'fail' => true,
//                    'errors' => $validator->errors()
//                ]);
////            $dh_histories = $dh_histories
////                ->where('device_id', '=', $request->device_id)
//            $dh_history = dh_history::find($id);
//            $dh_history->model_name = $request->model_name;
////            $dh_history->type_id = $request->type_id;
////            $dh_history->trademark_id = $request->trademark_id;
//            $dh_history->save();
//            return response()->json([
//                'fail' => false,
//                'redirect_url' => url('devicesDatabase/deviceModels')
//            ]);
//        }
    }

}