<?php

namespace App\Modules\DevicesDatabase\Controllers;

//use App\Modules\Crud_1\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\View\View;


class Main extends Controller {

    public function index(Request $request){
        return view("DevicesDatabase::main");
    }

}