<?php

namespace App\Modules\DevicesDatabase\Controllers;

use App\Modules\DevicesDatabase\Models\dh_trademark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class Trademarks extends Controller {

    public function index(Request $request){
        $request->session()->put('searchTrademark', $request->has('searchTrademark') ? $request->get('searchTrademark') : ($request->session()->has('searchTrademark') ? $request->session()->get('searchTrademark') : ''));
        $request->session()->put('trademark_name', $request->has('trademark_name') ? $request->get('trademark_name') : ($request->session()->has('trademark_name') ? $request->session()->get('trademark_name') : ''));
        $request->session()->put('trademark_field', $request->has('trademark_field') ? $request->get('trademark_field') : ($request->session()->has('trademark_field') ? $request->session()->get('trademark_field') : 'trademark_name'));
        $request->session()->put('trademark_sort', $request->has('trademark_sort') ? $request->get('trademark_sort') : ($request->session()->has('trademark_sort') ? $request->session()->get('trademark_sort') : 'asc'));
        $dh_trademarks = new dh_trademark();
        if ($request->session()->get('trademark_name') != -1) {
            $dh_trademarks = $dh_trademarks
                ->where('trademark_name', 'like', '%' . $request->session()->get('searchTrademark') . '%')
                ->orderBy($request->session()->get('trademark_field'), $request->session()->get('trademark_sort'))
                ->paginate(14);
        }

        if ($request->ajax())
            return view("DevicesDatabase::trademark.index", compact('dh_trademarks'));
        else
            return view("DevicesDatabase::trademark.ajax", compact('dh_trademarks'));
    }

    public function create(Request $request){
        if ($request->isMethod('get'))
            return view('DevicesDatabase::trademark.form');
        else {
            $rules = [
                'trademark_name' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_trademark = new dh_trademark();
            $dh_trademark->trademark_name = $request->trademark_name;
            $dh_trademark->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/trademarks')
            ]);
        }
    }

    public function delete($id){
        dh_trademark::destroy($id);
        return redirect('devicesDatabase/trademarks');
    }

    public function update(Request $request, $id){

        if ($request->isMethod('get'))
            return view('DevicesDatabase::trademark.form', ['dh_trademark' => dh_trademark::find($id)]);
        else {
            $rules = [
                'trademark_name' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_trademark = dh_trademark::find($id);
            $dh_trademark->trademark_name = $request->trademark_name;
            $dh_trademark->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/trademarks')
            ]);
        }
    }

}