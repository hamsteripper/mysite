<?php

namespace App\Modules\DevicesDatabase\Controllers;

//use App\Modules\Crud_1\Models\Customer;
use App\Modules\DevicesDatabase\Models\dh_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class Types extends Controller {

    public function index(Request $request){
        $request->session()->put('searchType', $request->has('searchType') ? $request->get('searchType') : ($request->session()->has('searchType') ? $request->session()->get('searchType') : ''));
        $request->session()->put('type', $request->has('type') ? $request->get('type') : ($request->session()->has('type') ? $request->session()->get('type') : ''));
        $request->session()->put('type_field', $request->has('type_field') ? $request->get('type_field') : ($request->session()->has('type_field') ? $request->session()->get('type_field') : 'type'));
        $request->session()->put('type_sort', $request->has('type_sort') ? $request->get('type_sort') : ($request->session()->has('type_sort') ? $request->session()->get('type_sort') : 'asc'));

//        $x = $request->session()->get('field');
//        $y = $request->session()->get('sort');
//
//        $a = 1;

        $dh_types = new dh_type();
        if ($request->session()->get('type') != -1)
            $dh_types = $dh_types
                ->where('type', 'like', '%' . $request->session()->get('searchType') . '%')
                ->orderBy($request->session()->get('type_field'), $request->session()->get('type_sort'))
                ->paginate(10);

        if ($request->ajax())
            return view("DevicesDatabase::types.index", compact('dh_types'));
        else
            return view("DevicesDatabase::types.ajax", compact('dh_types'));
    }

    public function create(Request $request){
        if ($request->isMethod('get'))
            return view('DevicesDatabase::types.form');
        else {
            $rules = [
                'type' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_types = new dh_type();
            $dh_types->type = $request->type;
            $dh_types->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/types')
            ]);
        }
    }

    public function delete($id){
        dh_type::destroy($id);
        return redirect('devicesDatabase/types');
    }

    public function update(Request $request, $id){

        if ($request->isMethod('get'))
            return view('DevicesDatabase::types.form', ['dh_type' => dh_type::find($id)]);
        else {
            $rules = [
                'type' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $dh_type = dh_type::find($id);
            $dh_type->type = $request->type;
            $dh_type->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('devicesDatabase/types')
            ]);
        }
    }

}