<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dh_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type_id')->unsigned()->nullable(false);
            $table->bigInteger('trademark_id')->unsigned()->nullable(false);
            $table->string('model_name', 20)->nullable(false)->nullable(false);
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('dh_types');
            $table->foreign('trademark_id')->references('id')->on('dh_trademarks');

            $table->unique(['model_name','type_id', 'trademark_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dh_models');
    }
}
