<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dh_instructions', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->string('name', 30)->nullable(false);
            $table->string('text')->nullable(true);
            $table->longText('file_path')->nullable(true);
            $table->timestamps();

//            $table->foreign('device_id')->references('id')->on('dh_devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dh_instructions');
    }
}
