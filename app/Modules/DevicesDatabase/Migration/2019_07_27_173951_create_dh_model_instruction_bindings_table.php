<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhModelInstructionBindingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dh_model_instruction_bindings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('model_id')->unsigned()->nullable(false);
            $table->bigInteger('instruction_id')->unsigned()->nullable(false);
            $table->timestamps();

            $table->foreign('model_id')->references('id')->on('dh_models');
            $table->foreign('instruction_id')->references('id')->on('dh_instructions');

            $table->unique(['model_id', 'instruction_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dh_model_instruction_bindings');
    }
}
