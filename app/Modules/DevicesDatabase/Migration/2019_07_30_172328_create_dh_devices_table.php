<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dh_devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('SN')->nullable(false);
//            $table->bigInteger('type_id')->unsigned()->nullable(false);
            $table->bigInteger('model_id')->unsigned()->nullable(false);
            $table->bigInteger('counterparty_id')->unsigned()->nullable(false);
            $table->string('address')->nullable(false);
            $table->timestamps();

            $table->foreign('model_id')->references('id')->on('dh_models');
//            $table->foreign('type_id')->references('id')->on('dh_types');
            $table->foreign('counterparty_id')->references('id')->on('dh_counterparties');

//            $table->primary(['SN','model_id']);

            $table->unique(['SN', 'model_id']);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dh_devices');
    }
}
