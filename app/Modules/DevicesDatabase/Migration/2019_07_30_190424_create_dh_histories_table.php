<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dh_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('device_id')->unsigned()->nullable(false);
//            $table->string('device_sn')->nullable(false);
//            $table->bigInteger('model_id')->unsigned()->nullable(false);
            $table->string('comment')->nullable(true);
            $table->longText('file_path')->nullable(true);
            $table->date('date')->nullable(false);
            $table->timestamps();

            $table->foreign('device_id')->references('id')->on('dh_devices');
//            $table->foreign(['device_sn', 'model_id'])->references(['SN','model_id'])->on('dh_devices');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dh_histories');
    }
}
