<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;

class dh_counterparty extends Model
{
    protected $fillable = [
        'counterparty_id', 'type_id', 'model_id', 'SN',
    ];

    public function device(){
        return $this->hasMany('App\Modules\DevicesDatabase\Models\dh_device', 'id', 'counterparty_id');
    }
}
