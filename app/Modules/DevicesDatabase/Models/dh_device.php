<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;

class dh_device extends Model
{
//    protected $fillable = [
//        'SN', 'counterparty_id', 'type_id', 'model_id'
//    ];

    protected $guarded = [];

    public function model(){
        return $this->belongsTo('App\Modules\DevicesDatabase\Models\dh_model', 'model_id', 'id');
    }

    public function type(){
        return $this->belongsTo('App\Modules\DevicesDatabase\Models\dh_type', 'type_id', 'id');
    }

    public function counterparty(){
        return $this->belongsTo('App\Modules\DevicesDatabase\Models\dh_counterparty', 'counterparty_id', 'id');
    }

    public function history(){
        return $this->hasMany('App\Modules\DevicesDatabase\Models\dh_history', 'id', 'device_id');
    }
}
