<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;

class dh_history extends Model
{
    public function device(){
        return $this->belongsTo('App\Modules\DevicesDatabase\Models\dh_device', 'device_id', 'id');
    }
}
