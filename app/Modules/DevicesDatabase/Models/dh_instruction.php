<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;

class dh_instruction extends Model
{
//    protected $guarded = [];

    public function model(){
        return $this->belongsToMany('App\Modules\DevicesDatabase\Models\dh_model', 'dh_model_instruction_bindings', 'instruction_id', 'model_id');
    }
}
