<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;

use App\Modules\DevicesDatabase\Models\dh_trademark;
use App\Modules\DevicesDatabase\Models\dh_type;

class dh_model extends Model
{
    protected $fillable = [
        'type_id', 'trademark_id', 'model_name',
    ];

    public function types(){
        return $this->belongsTo('App\Modules\DevicesDatabase\Models\dh_type', 'type_id', 'id');
    }

    public function trademark(){
        return $this->belongsTo('App\Modules\DevicesDatabase\Models\dh_trademark', 'trademark_id', 'id');
    }

    public function instructions(){
        return $this->belongsToMany('App\Modules\DevicesDatabase\Models\dh_instruction', 'dh_model_instruction_bindings', 'model_id', 'instruction_id');
    }
}
