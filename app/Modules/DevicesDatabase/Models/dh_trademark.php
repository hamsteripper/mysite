<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\DevicesDatabase\Models\dh_model;

class dh_trademark extends Model
{
    protected $fillable = [
        'trademark_name',
    ];

    public function dh_model(){
        return $this->hasMany('App\Modules\DevicesDatabase\Models\dh_model', 'id', 'trademark_id');
    }
}
