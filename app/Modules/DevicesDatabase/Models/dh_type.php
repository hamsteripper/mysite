<?php

namespace App\Modules\DevicesDatabase\Models;

use Illuminate\Database\Eloquent\Model;

use App\Modules\DevicesDatabase\Models\dh_model;

class dh_type extends Model
{
    protected $fillable = [
        'type'
    ];

//    protected $timestamps = true;

    public function model(){
        return $this->hasMany('App\Modules\DevicesDatabase\Models\dh_model', 'id', 'type_id');
    }

}
