<?php

Route::group(
    [
        'prefix' => 'devicesDatabase',
        'namespace' => 'App\Modules\DevicesDatabase\Controllers',
        'middleware' => ['web','auth'],
    ],
    function () {

        Route::get('/', 'Main@index');


        Route::get('/types', 'Types@index');
        Route::match(['get', 'post'], 'types/create', 'Types@create');
        Route::match(['get', 'put'], 'types/update/{id}', 'Types@update');
        Route::delete('types/delete/{id}', 'Types@delete');

        Route::get('/trademarks', 'Trademarks@index');
        Route::match(['get', 'post'], 'trademarks/create', 'Trademarks@create');
        Route::match(['get', 'put'], 'trademarks/update/{id}', 'Trademarks@update');
        Route::delete('trademarks/delete/{id}', 'Trademarks@delete');

        Route::get('/deviceModels', 'DeviceModels@index');
        Route::match(['get', 'post'], 'deviceModels/create', 'DeviceModels@create');
        Route::match(['get', 'put'], 'deviceModels/update/{id}', 'DeviceModels@update');
        Route::delete('deviceModels/delete/{id}', 'DeviceModels@delete');

        Route::get('/deviceCounterparties', 'DeviceCounterparties@index');
        Route::match(['get', 'post'], 'deviceCounterparties/create', 'DeviceCounterparties@create');
        Route::match(['get', 'put'], 'deviceCounterparties/update/{id}', 'DeviceCounterparties@update');
        Route::delete('deviceCounterparties/delete/{id}', 'DeviceCounterparties@delete');

        Route::get('/devices', 'Devices@index');
        Route::match(['get', 'post'], 'devices/create', 'Devices@create');
        Route::match(['get', 'put'], 'devices/update/{id}', 'Devices@update');
        Route::delete('devices/delete/{id}', 'Devices@delete');

        Route::get('/devicesHistory', 'DevicesHistory@index')->name('devicesHistory.index');
        Route::match(['get', 'post'], 'devicesHistory/create', 'DevicesHistory@create')->name('devicesHistory.create');
        Route::match(['get', 'put'], 'devicesHistory/update/{id}', 'DevicesHistory@update');
        Route::delete('devicesHistory/delete/{device_id}/{id}', 'DevicesHistory@delete');
        Route::match(['get', 'post'], 'devicesHistory/addFile/{device_id}', 'DevicesHistory@addFile')->name('devicesHistory.addFile');
        Route::match(['get', 'post'], 'devicesHistory/getFiles/{device_id}', 'DevicesHistory@getFiles')->name('devicesHistory.getFiles');

        Route::get('/instructionsModel', 'InstructionsModel@index')->name('instructionsModel.index');
        Route::match(['get', 'post'], 'instructionsModel/create', 'InstructionsModel@create')->name('instructionsModel.create');
        Route::match(['get', 'put'], 'instructionsModel/update/{id}', 'InstructionsModel@update');
        Route::delete('instructionsModel/delete/{device_id}/{id}', 'InstructionsModel@delete');
        Route::match(['get', 'post'], 'instructionsModel/addFile/{device_id}', 'InstructionsModel@addFile')->name('instructionsModel.addFile');
        Route::match(['get', 'post'], 'instructionsModel/getFiles/{device_id}', 'InstructionsModel@getFiles')->name('instructionsModel.getFiles');


    }
);