<div class="container">
    <hr/>

    <div class="float-right">
        <a href="{{url('/home')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('devicesDatabase/deviceCounterparties/create')}}')" class="btn btn-primary">New</a>
    </div>
    {{--Search--}}
    <div class="row">
        <div class="col-sm-5 form-group">
            <div class="input-group">
                <input class="form-control" id="searchCounterparty"
                       value="{{ request()->session()->get('searchCounterparty') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('devicesDatabase/deviceCounterparties')}}?searchCounterparty='+this.value)"
                       placeholder="Search" name="searchCounterparty"
                       type="text"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-warning" onclick="ajaxLoad('{{url('devicesDatabase/deviceCounterparties')}}?searchCounterparty='+$('#searchCounterparty').val())">Search</button>
                </div>
            </div>
        </div>
    </div>
    {{--Table--}}
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('devicesDatabase/deviceCounterparties?counterparty_field=counterparty_name&counterparty_sort='.(request()->session()->get('counterparty_sort')=='asc'?'desc':'asc'))}}')">
                    Counterparty name
                </a>
                {{request()->session()->get('counterparty_field')=='counterparty_name'?(request()->session()->get('counterparty_sort')=='asc'?html_entity_decode('&#9652;'):html_entity_decode('&#9662;')):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($dh_counterparties as $dh_counterparty)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $dh_counterparty->counterparty_name }}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="javascript:ajaxLoad('{{url('devicesDatabase/deviceCounterparties/update/'.$dh_counterparty->id)}}')">Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete"
                        href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('devicesDatabase/deviceCounterparties/delete/'.$dh_counterparty->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{--Next page--}}
    <nav>
        <ul class="pagination justify-content-end">
            {{$dh_counterparties->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>