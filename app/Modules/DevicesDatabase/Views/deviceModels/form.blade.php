<div class="container">
    <div class="col-md-8 offset-md-2">

        <div class="card-img-top" >
            <div class="card-body bg-light text-info">

                <h1>{{isset($dh_model)?'Edit':'New'}} Model </h1>
                <hr/>
                @if(isset($dh_model))
                    {!! Form::model($dh_model,['method'=>'put','id'=>'frm']) !!}
                @else
                    {!! Form::open(['id'=>'frm']) !!}
                @endif
                {{--Name--}}
                <div class="form-group row required">
                    {!! Form::label("model_name","Model name",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                    <div class="col-md-8">
                        {!! Form::text("model_name",null,["class"=>"form-control".($errors->has('model_name')?" is-invalid":""),"autofocus",'placeholder'=>'Model name']) !!}
                        <span id="error-model_name" class="invalid-feedback"></span>
                    </div>
                </div>
                {{--Type--}}
                <div class="form-group row required">
                    {!! Form::label("Type","Type:",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                    <div class="col-md-8">
                        <select class="form-control" name="type_id">
                            @foreach($types as $type)
                                @if(isset($dh_model))
                                    @if($dh_model->type_id==$type->id)
                                        <option value="{{$type->id}}" selected>{{$type->type}}</option>
                                    @else
                                        <option value="{{$type->id}}">{{$type->type}}</option>
                                    @endif
                                @else
                                    <option value="{{$type->id}}">{{$type->type}}</option>
                                @endif
                            @endforeach
                            <span id="error-model_name" class="invalid-feedback"></span>
                        </select>
                    </div>
                </div>
                {{--Trademarks--}}
                <div class="form-group row required">
                    {!! Form::label("Trademarks","Trademarks:",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                    <div class="col-md-8">
                        <select class="form-control" name="trademark_id">
                            @foreach($trademarks as $trademark)
                                @if(isset($dh_model))
                                    @if($dh_model->type_id==$trademark->id)
                                        <option value="{{$trademark->id}}" selected>{{$trademark->trademark_name}}</option>
                                    @else
                                        <option value="{{$trademark->id}}">{{$trademark->trademark_name}}</option>
                                    @endif
                                @else
                                    <option value="{{$trademark->id}}">{{$trademark->trademark_name}}</option>
                                @endif
                            @endforeach
                            <span id="error-model_name" class="invalid-feedback"></span>
                        </select>
                    </div>
                </div>
                {{--Next page--}}
                <div class="form-group row">
                    <div class="col-md-3 col-lg-2"></div>
                    <div class="col-md-4">
                        <a href="javascript:ajaxLoad('{{url('devicesDatabase/deviceModels')}}')" class="btn btn-danger">Back</a>
                        {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>