<div class="container">

    <hr/>
    {{--New--}}
    <div class="float-right">
        <a href="{{url('/home')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('devicesDatabase/deviceModels/create')}}')" class="btn btn-primary">New</a>
    </div>
    {{--Search--}}
    <div class="row">

        <div class="col-sm-8 form-group">
            <div class="input-group">
                <input class="form-control" id="searchModels"
                       value="{{ request()->session()->get('searchModel') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('devicesDatabase/deviceModels')}}?searchModel='+this.value)"
                       placeholder="Search" name="searchModel"
                       type="text"/>
                <div class="input-group-btn">
                    <button type="submit"
                            class="btn btn-warning"
                            onclick="ajaxLoad('{{url('devicesDatabase/deviceModels')}}?searchModel='+$('#searchModel').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--Table--}}
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center" >
                <a href="#">No</a>
            </th>
            <th style="vertical-align: middle">
                <a href="#">Model name</a>
            </th>
            <th style="vertical-align: middle">
                <a href="#">Type name</a>
            </th>
            <th style="vertical-align: middle">
                <a href="#">Trademark name</a>
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    Instuctions
                </a>
            </th>

            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($dh_models as $dh_model)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $dh_model->model_name }}</td>
                <td style="vertical-align: middle">{{ $dh_model->types->type }}</td>
                <td style="vertical-align: middle">{{ $dh_model->trademark->trademark_name }}</td>
                <td style="vertical-align: middle" >
                    <a class="btn btn-primary btn-sm" title="Edit" href="{{route('instructionsModel.index', ['model_id' => $dh_model->id])}}">History</a>
                </td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="javascript:ajaxLoad('{{url('devicesDatabase/deviceModels/update/'.$dh_model->id)}}')">Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm"
                       title="Delete"
                       href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('devicesDatabase/deviceModels/delete/'.$dh_model->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$dh_models->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>