<div class="container">
    <div class="col-md-10 offset-md-2">

        <div class="card-img-top" >
            <div class="card-body bg-light text-info">

        <h1>{{isset($dh_devices)?'Edit':'New'}} device </h1>
        <hr/>
        @if(isset($dh_devices))
            {!! Form::model($dh_devices,['method'=>'put','id'=>'frm']) !!}
        @else
            {!! Form::open(['id'=>'frm']) !!}
        @endif



        {{--SN--}}
        <div class="form-group row required">
            {!! Form::label("SN","SN",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                {!! Form::text("SN",null,["class"=>"form-control".($errors->has('SN')?" is-invalid":""),"autofocus",'placeholder'=>'SN']) !!}
                <span id="error-SN" class="invalid-feedback"></span>
            </div>
        </div>
        {{--SN--}}
        <div class="form-group row required">
            {!! Form::label("Address","Address",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                {!! Form::text("address",null,["class"=>"form-control".($errors->has('address')?" is-invalid":""),"autofocus",'placeholder'=>'address']) !!}
                <span id="error-address" class="invalid-feedback"></span>
            </div>
        </div>
        {{--Counterparties--}}
        <div class="form-group row required">
            {!! Form::label("Counterparties","Counterparties:",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                <select class="form-control" name="counterparty_id">
                    @foreach($counterparties as $counterparties)
                        @if(isset($dh_devices))
                            @if($dh_devices->counterparty_id==$counterparties->id)
                                <option value="{{$counterparties->id}}" selected>{{$counterparties->counterparty_name}}</option>
                            @else
                                <option value="{{$counterparties->id}}">{{$counterparties->counterparty_name}}</option>
                            @endif
                        @else
                            <option value="{{$counterparties->id}}">{{$counterparties->counterparty_name}}</option>
                        @endif
                    @endforeach
                    <span id="error-counterparty_id" class="invalid-feedback"></span>
                </select>
            </div>
        </div>
        {{--Models--}}
        <div class="form-group row required">
            {!! Form::label("Model ","Models:",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
            <div class="col-md-8">
                <select class="form-control" name="model_id">
                    @foreach($models as $model)
                        @if(isset($dh_devices))
                            @if($dh_devices->type_id==$model->id)
                                <option value="{{$model->id}}" selected>{{$model->model_name}}</option>
                            @else
                                <option value="{{$model->id}}">{{$model->model_name}}</option>
                            @endif
                        @else
                            <option value="{{$model->id}}">{{$model->model_name}}</option>
                        @endif
                    @endforeach
                    <span id="error-model_id" class="invalid-feedback"></span>
                </select>
            </div>
        </div>
        {{--Save page--}}
        <div class="form-group row">
            <div class="col-md-3 col-lg-2"></div>
            <div class="col-md-4">
                <a href="javascript:ajaxLoad('{{url('devicesDatabase/devices')}}')" class="btn btn-danger">Back</a>
                {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
            </div>
        </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>