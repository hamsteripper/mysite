<div class="container">

    <hr/>
    {{--New--}}
    <div class="float-right">
        <a href="{{url('/home')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('devicesDatabase/devices/create')}}')"
           class="btn btn-primary">New </a>
    </div>
    {{--Search--}}
    <div class="row">

        <div class="col-sm-5 form-group">
            <div class="input-group">
                <input class="form-control" id="searchSN"
                       value="{{ request()->session()->get('searchSN') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('devicesDatabase/devices')}}?searchSN='+this.value)"
                       placeholder="Search" name="searchSN"
                       type="text"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-warning"
                            onclick="ajaxLoad('{{url('devicesDatabase/devices')}}?searchSN='+$('#searchSN').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--Table--}}
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('devicesDatabase/devices?SN_field=SN&SN_sort='.(request()->session()->get('SN_sort')=='asc'?'desc':'asc'))}}')">
                    SN
                </a>
                {{request()->session()->get('SN_field')=='SN'?(request()->session()->get('SN_sort')=='asc'?html_entity_decode('&#9652;'):html_entity_decode('&#9662;')):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    Model
                </a>
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    Type
                </a>
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    Address
                </a>
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    Counterparty
                </a>
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    Trademark
                </a>
            </th>
            <th style="vertical-align: middle">
                <a href="">
                    History
                </a>
            </th>

            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($dh_devices as $dh_device)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $dh_device->SN }}</td>
                <td style="vertical-align: middle">{{ $dh_device->model->model_name }}</td>
                <td style="vertical-align: middle">{{ $dh_device->model->types->type }}</td>
                <td style="vertical-align: middle">{{ $dh_device->address }}</td>
                <td style="vertical-align: middle">{{ $dh_device->counterparty->counterparty_name }}</td>
                <td style="vertical-align: middle">{{ $dh_device->model->trademark->trademark_name }}</td>
                <td style="vertical-align: middle" >
                    <a class="btn btn-primary btn-sm" title="Edit" href="{{route('devicesHistory.index', ['device_id' => $dh_device->id])}}">History</a>
                </td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="javascript:ajaxLoad('{{url('devicesDatabase/devices/update/'.$dh_device->id)}}')">Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    {{ csrf_field() }}
                    <a class="btn btn-danger btn-sm" title="Delete"
                       href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('devicesDatabase/devices/delete/'.$dh_device->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$dh_devices->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>