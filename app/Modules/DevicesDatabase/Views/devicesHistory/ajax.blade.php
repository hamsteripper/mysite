@extends('layouts.mainBlade')

@section('css2')
    <style>

        a, a:hover {
            color: white;
        }

        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection

@section('content2')
    <div id="content">
        @include("DevicesDatabase::devicesHistory.index")
    </div>
    {{--<div class="loading">--}}
        {{--<i class="fa fa-refresh fa-spin fa-2x fa-fw"></i><br/>--}}
        {{--<span>Loading</span>--}}
    {{--</div>--}}
@endsection

@section('js2')
    <script src="{{ asset('assets/ajax-crud.js') }}"></script>
    <script src="{{ asset('assets/2c7a93b259.js') }}"></script>

    {{--Препросмотр изображений--}}
    <script src="{{ asset('assets/lightbox-plus-jquery.js') }}"></script>
    <link href="{{ asset('assets/lightbox.min.css') }}" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

@endsection