
<div class="container">

    <link href="{{ asset('assets_css/component.css') }}" rel="stylesheet">

    <style>
        .img-thumbnail {
            height: 175px;
            border: 1px solid #000;
            margin: 10px 5px 0 0;
        }
    </style>

    <div class="col-md-8 offset-md-2 ">
        @if(isset($dh_history))
            {!! Form::model($dh_history,['method'=>'put','id'=>'frm']) !!}
        @else
            {!! Form::open(['id'=>'frm']) !!}
        @endif
        <div class="card h-100 text-info" >
            <div class="card-body" >
                <input type="hidden" name="device_id" value="{{request()->session()->get('device_id')}}"/>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Comment</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="historyComment" rows="3"></textarea>
                </div>

                <hr/>
                <div class="form-group" >
                    <input id="file" type="file" name="file[]" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" style="display: none" multiple="" />
                    <label for="file">
                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="7" viewBox="0 0 20 17" class="text-info">
                            <path  d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                        </svg>
                        <span class="text-info">Choose a file…</span>
                    </label>
                    <br>
                    <span id="outputMulti"></span>
                </div>

                <hr/>
                <div class="form-group">
                    <label for="date">Date</label>
                    <br>
                    <input type="date" id="datetime" name="date">
                </div>

                <hr/>
                {{--Next page--}}
                <div class="form-group row">
                    <div class="col-md-4">
                        <a href="javascript:ajaxLoad('{{url('devicesDatabase/devicesHistory?device_id='.request()->session()->get('device_id'))}}')" class="btn btn-danger">Back</a>
                        {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
                    </div>
                </div>

            </div>
        </div>

        {!! Form::close() !!}

    </div>

</div>

<script>

    function handleFileSelectMulti(evt) {
        var files = evt.target.files; // FileList object
        document.getElementById('outputMulti').innerHTML = "";
        for (var i = 0, f; f = files[i]; i++) {

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="img-thumbnail" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('outputMulti').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('file').addEventListener('change', handleFileSelectMulti, false);

</script>

{{--Текущая дата--}}
<script>
    document.getElementById("datetime").valueAsDate = new Date();
</script>

<script>

    'use strict';

    ;( function ( document, window, index )
    {
        var inputs = document.querySelectorAll( '.inputfile' );
        Array.prototype.forEach.call( inputs, function( input )
        {
            var label	 = input.nextElementSibling,
                labelVal = label.innerHTML;

            input.addEventListener( 'change', function( e )
            {
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    label.querySelector( 'span' ).innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });

            // Firefox bug fix
            input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
            input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
        });
    }( document, window, 0 ));

</script>
