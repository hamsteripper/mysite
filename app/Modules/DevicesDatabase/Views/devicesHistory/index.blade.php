<div class="container">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        .footer {
            padding: 15px 0px 10px 0px;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #fff;
            background-color: #222;
            border-color: #080808;
        }
    </style>

    <hr/>
    {{--New--}}
    <div class="float-right">
        <a href="{{url('devicesDatabase/devices')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('devicesDatabase/devicesHistory/create')}}')" class="btn btn-primary">New</a>
    </div>
    {{--Search--}}
    <div class="row">

        <div class="col-sm-8 form-group">
            <div class="input-group">
                <input class="form-control" id="searchDevicesHistory"
                       value="{{ request()->session()->get('searchDevicesHistory') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('devicesDatabase/devicesHistory')}}?searchDevicesHistory='+this.value)"
                       placeholder="Search" name="searchDevicesHistory"
                       type="text"/>
                <div class="input-group-btn">
                    <button type="submit"
                            class="btn btn-warning"
                            onclick="ajaxLoad('{{url('devicesDatabase/devicesHistory')}}?searchDevicesHistory='+$('#searchDevicesHistory').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--Table--}}


    @foreach($dh_histories as $dh_history)
        {{--Преобразование JSON в массив и подготовка путей--}}
        <?php
            $path = storage_path();
            $path = "/storage/deviceDatabese/devicesHistory/".request()->session()->get('device_id')."/";
            $array = json_decode($dh_history->file_path, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);
        ?>
        <br/>
        <div class="card-img-top" >
            <div class="card-body bg-light text-info">
                <h5 class="card-title">{{ $dh_history->date }}</h5>
                <hr/>
                <h5 class="card-title">{{ $dh_history->comment }}</h5>
                <hr/>
                <div class="row">
                    @foreach($array as $value)
                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                            <a class="thumbnail" href="{{$path.$value}}" data-lightbox="imgGLR" >
                                <img class="img-responsive" border="0" height="150" src="{{$path.$value}}" width="200" />
                            </a>
                        </div>
                    @endforeach
                </div>
                <hr/>
                <a class="btn btn-primary btn-sm" title="Get Files" href="{{route('devicesHistory.getFiles', ['device_id' => request()->session()->get('device_id'), 'dh_history_id' => $dh_history->id])}}">Get Files</a>
                {{--<a class="btn btn-primary btn-sm" title="Edit" href="javascript:ajaxLoad('{{url('devicesDatabase/devicesHistory/update/'.$dh_history->id)}}')">Edit</a>--}}
                <input type="hidden" name="_method" value="delete"/>
                <a class="btn btn-danger btn-sm"
                    title="Delete"
                    href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('devicesDatabase/devicesHistory/delete/'.request()->session()->get('device_id').'/'.$dh_history->id)}}','{{csrf_token()}}')">
                    Delete
                </a>
            </div>
        </div>
    @endforeach

    <nav>
        <ul class="pagination justify-content-end">
            {{$dh_histories->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>

</div>