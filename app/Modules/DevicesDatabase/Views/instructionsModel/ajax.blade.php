@extends('layouts.mainBlade')

{{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">--}}

{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>--}}

{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>--}}

@section('css2')
    <style>
        /*.loading {*/
            /*background: lightgrey;*/
            /*padding: 15px;*/
            /*position: fixed;*/
            /*border-radius: 4px;*/
            /*left: 50%;*/
            /*top: 50%;*/
            /*text-align: center;*/
            /*margin: -40px 0 0 -50px;*/
            /*z-index: 2000;*/
            /*display: none;*/
        /*}*/

        a, a:hover {
            color: white;
        }

        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection

@section('content2')
    <div id="content">
        @include("DevicesDatabase::instructionsModel.index")
    </div>
    <div class="loading">
        <i class="fa fa-refresh fa-spin fa-2x fa-fw"></i><br/>
        <span>Loading</span>
    </div>
@endsection

@section('js2')
    <script src="{{ asset('assets/ajax-crud.js') }}"></script>
    <script src="{{ asset('assets/2c7a93b259.js') }}"></script>

    {{--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>--}}

    {{--Препросмотр изображений--}}
    <script src="{{ asset('assets/lightbox-plus-jquery.js') }}"></script>
    <link href="{{ asset('assets/lightbox.min.css') }}" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>--}}

    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>--}}

    {{--Препросмотр изображений--}}

    {{--<div id="yandex_ad"></div>--}}
    {{--<script type="text/javascript">--}}
        {{--(function(w, d, n, s, t) {--}}
            {{--w[n] = w[n] || [];--}}
            {{--w[n].push(function() {--}}
                {{--Ya.Direct.insertInto(84098, "yandex_ad", {--}}
                    {{--ad_format: "direct",--}}
                    {{--type: "240x400",--}}
                    {{--border_type: "block",--}}
                    {{--border_radius: true,--}}
                    {{--site_bg_color: "FFFFFF",--}}
                    {{--header_bg_color: "FEEAC7",--}}
                    {{--bg_color: "FFF9F0",--}}
                    {{--border_color: "FBE5C0",--}}
                    {{--title_color: "0000CC",--}}
                    {{--url_color: "006600",--}}
                    {{--text_color: "000000",--}}
                    {{--hover_color: "0066FF",--}}
                    {{--favicon: true,--}}
                    {{--no_sitelinks: false--}}
                {{--});--}}
            {{--});--}}
            {{--t = d.getElementsByTagName("script")[0];--}}
            {{--s = d.createElement("script");--}}
            {{--s.src = "{{ asset('assets/context.js') }}";--}}
            {{--s.type = "text/javascript";--}}
            {{--s.async = true;--}}
            {{--t.parentNode.insertBefore(s, t);--}}
        {{--})(window, document, "yandex_context_callbacks");--}}
    {{--</script>--}}
    {{--<script src="https://use.fontawesome.com/2c7a93b259.js"></script>--}}
@endsection