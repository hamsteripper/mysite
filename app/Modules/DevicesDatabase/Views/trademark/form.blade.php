<div class="container">
    <div class="col-md-10 offset-md-2">

        <div class="card-img-top" >
            <div class="card-body bg-light text-info">
                <h1>{{isset($dh_trademark)?'Edit':'New'}} Trademarks </h1>
                <hr/>
                @if(isset($dh_trademark))
                    {!! Form::model($dh_trademark,['method'=>'put','id'=>'frm']) !!}
                @else
                    {!! Form::open(['id'=>'frm']) !!}
                @endif
                <div class="form-group row required">
                    {!! Form::label("trademark_name","Trademark name",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                    <div class="col-md-8">
                        {!! Form::text("trademark_name",null,["class"=>"form-control".($errors->has('trademark_name')?" is-invalid":""),"autofocus",'placeholder'=>'Trademark_name']) !!}
                        <span id="error-trademark_name" class="invalid-feedback"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3 col-lg-2"></div>
                    <div class="col-md-4">
                        <a href="javascript:ajaxLoad('{{url('devicesDatabase/trademarks')}}')" class="btn btn-danger">Back</a>
                        {!! Form::button("Save",["type" => "submit","class"=>"btn btn-primary"])!!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>