<div class="container">
    <hr/>
    <div class="float-right">
        <a href="{{url('/home')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('devicesDatabase/trademarks/create')}}')"
           class="btn btn-primary">New</a>
    </div>
        {{--<h1 style="font-size: 2.2rem">Customers List (Laravel CRUD, Search, Sort - AJAX)</h1>--}}
        {{--<hr/>--}}
    <div class="row">
        {{--<div class="col-sm-4 form-group">--}}
            {{--{!! Form::select('gender',['-1'=>'Select Gender','Male'=>'Male','Female'=>'Female'],request()->session()->get('gender'),['class'=>'form-control','onChange'=>'ajaxLoad("'.url("laravel-crud-search-sort-ajax").'?gender="+this.value)']) !!}--}}
        {{--</div>--}}
        <div class="col-sm-5 form-group">
            <div class="input-group">
                <input class="form-control" id="searchTrademark"
                       value="{{ request()->session()->get('searchTrademark') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('devicesDatabase/trademarks')}}?searchTrademark='+this.value)"
                       placeholder="Search" name="searchTrademark"
                       type="text"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-warning"
                            onclick="ajaxLoad('{{url('devicesDatabase/trademarks')}}?searchTrademark='+$('#searchTrademark').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('devicesDatabase/trademarks?trademark_field=trademark_name&trademark_sort='.(request()->session()->get('trademark_sort')=='asc'?'desc':'asc'))}}')">
                    Name
                </a>
                {{request()->session()->get('trademark_field')=='trademark_name'?(request()->session()->get('trademark_sort')=='asc'?html_entity_decode('&#9652;'):html_entity_decode('&#9662;')):''}}
            </th>

            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($dh_trademarks as $dh_trademark)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $dh_trademark->trademark_name }}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit"
                       href="javascript:ajaxLoad('{{url('devicesDatabase/trademarks/update/'.$dh_trademark->id)}}')">
                    Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete"
                       href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('devicesDatabase/trademarks/delete/'.$dh_trademark->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$dh_trademarks->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>