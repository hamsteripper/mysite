<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}

                <div class="container">
                    <div class="col-md-8 offset-md-2">

                        <div class="card-img-top" >
                            <div class="card-body bg-light text-info">
                                <h1>{{isset($dh_type)?'Edit':'New'}} Type</h1>
                                <hr/>
                                @if(isset($dh_type))
                                    {!! Form::model($dh_type,['method'=>'put','id'=>'frm']) !!}
                                @else
                                    {!! Form::open(['id'=>'frm']) !!}
                                @endif
                                <div class="form-group row required">
                                    {!! Form::label("type","Type",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::text("type",null,["class"=>"form-control".($errors->has('type')?" is-invalid":""),"autofocus",'placeholder'=>'Type']) !!}
                                        <span id="error-type" class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3 col-lg-2"></div>
                                    <div class="col-md-4">
                                        <a href="javascript:ajaxLoad('{{url('devicesDatabase/types')}}')" class="btn btn-danger">Back</a>
                                        {!! Form::button("Save",["type" => "submit","class"=>"btn
                                    btn-primary"])!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
