<div class="container">
    <hr/>

    <div class="float-right">
        <a href="{{url('/home')}}" class="btn btn-danger">Back</a>
        <a href="javascript:ajaxLoad('{{url('devicesDatabase/types/create')}}')" class="btn btn-primary">New</a>
    </div>

    {{--Search--}}
    <div class="row">
        <div class="col-sm-5 form-group">
            <div class="input-group">
                <input class="form-control" id="searchType"
                       value="{{ request()->session()->get('searchType') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('devicesDatabase/types')}}?searchType='+this.value)"
                       placeholder="Search" name="searchType"
                       type="text"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-warning" onclick="ajaxLoad('{{url('devicesDatabase/types')}}?searchType='+$('#searchType').val())">Search</button>
                </div>
            </div>
        </div>
    </div>

    {{--Table--}}
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('devicesDatabase/types?type_field=type&type_sort='.(request()->session()->get('type_sort')=='asc'?'desc':'asc'))}}')">
                    Type
                </a>
                {{request()->session()->get('type_field')=='type'?(request()->session()->get('type_sort')=='asc'?html_entity_decode('&#9652;'):html_entity_decode('&#9662;')):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($dh_types as $dh_type)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $dh_type->type }}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="javascript:ajaxLoad('{{url('devicesDatabase/types/update/'.$dh_type->id)}}')">Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete"
                        href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('devicesDatabase/types/delete/'.$dh_type->id)}}','{{csrf_token()}}')">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{--Next page--}}
    <nav>
        <ul class="pagination justify-content-end">
            {{$dh_types->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>