<?php

namespace App\Modules\FilesConverter\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\FilesConverter\Models\ConversionOptions;
use App\Modules\FilesConverter\Models\ConvertFile;
use App\Modules\FilesConverter\Models\DataBaseSaveLoadFileStatus;
use App\Modules\FilesConverter\Models\DownloadFileOrFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\FilesConverter\Models\FileUploader;

class FilesConverter extends Controller {

    // Стартовая страница
    public function index(){
        return view('FilesConverter::index');
    }

    // Выбор и скачивание файлов
    public function multipleFileSelection(Request $request){

        $destinationPath = storage_path().'/app/public/upload/'. Auth::id();
        $files = array();
        // Анализ пришедших типов файлов и сохранение поддерживаемых файлов на диск
        if($request->hasfile('filename')){
            foreach($request->file('filename') as $file){
                $uploader = new FileUploader();
                $uploader->upload($file, $destinationPath);
                $fileStatus = $uploader->get();
                array_push($files, $fileStatus);
            }
        }

        if(empty($files)){
            // TO FIX
        }

        // Получение параметров
        $convertOpt = new ConversionOptions();
        $convertOpt->setFiles($files);
        $files = $convertOpt->getFiles();

        // Сохранение данных в БД и удаление предыдущих данных от текущего пользователя
        $saveLoadModel = new DataBaseSaveLoadFileStatus();
        $saveLoadModel->saveFilesToDB($files, Auth::id());

        // Отправление данных пользователя в формате json
        return view('FilesConverter::multipleFileSelection',['files'=>json_encode($files)]);
    }

    // Конвертирование
    public function conversion(Request $request){

        $destinationPath = storage_path().'/app/public/upload/'. Auth::id();
        // Загрузка данных из БД
        $saveLoadModel = new DataBaseSaveLoadFileStatus();
        $files = $saveLoadModel->loadFilesFromDB(Auth::id());

        // Конвертация остального
        foreach ($files as $key => $file) {
            // Есть ли уникальное имя у файла ?
            if (array_key_exists("uniqueName", $file)) {
                $uniqueName = $file["uniqueName"];
                // Есть ли уникальное имя в запросе ?
                if ($request->get($uniqueName) != null) {
                    $finishType = $request->get($uniqueName);
                    // Конвертация
                    $convert = new ConvertFile();
                    $convert->setFilePath($file["path"]);
                    $convert->setStartFinishFileTypes($file["type"], $finishType);
                    $convert->convert();
                    // NULL EXCEPTION FIX
                    $files[$key]["newFilePath"] = $convert->getNewFilePath();
                    $pos = strripos($convert->getNewFilePath(), "/");
                    $files[$key]["newFileName"] = substr($convert->getNewFilePath(), $pos + 1);
                }
            }
        }

        $donloader = new DownloadFileOrFiles();

        // Fix сделать возвтрат к стартовой странице перед отправкой файла
        if (count($files) == 1) {
            return json_encode($donloader->getFile(Auth::user()->id, $files, $destinationPath ));
        } elseif (count($files) > 1) {
            return json_encode($donloader->getFiles(Auth::user()->id, $files, $destinationPath));
        }

    }
}
