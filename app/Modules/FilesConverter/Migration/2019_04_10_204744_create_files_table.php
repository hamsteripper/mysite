<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('filename');
            $table->string('path')->unique();
            $table->string('mimetype');
            $table->string('type');
            $table->bigInteger('user_id')->unsigned();
            $table->json('convertTo');
            $table->string('uniqueName')->unique();
//            $table->foreign('user_id')->references('id')->on('users');
            //создание поля для связывания с таблицей user


            $table->timestamps();
        });
//


//        Schema::table('priorities', function(Blueprint $table) {
//            //создание внешнего ключа для поля 'user_id', который связан с полем id таблицы 'users'
//            $table->foreign('user_id')->references('id')->on('users');
//        });

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
