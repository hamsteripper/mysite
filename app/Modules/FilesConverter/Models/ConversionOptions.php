<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 09.04.19
 * Time: 22:18
 */

namespace App\Modules\FilesConverter\Models;


class ConversionOptions
{
    private $files = array();

    public function setFiles(array $files){
        foreach ($files as $file){
            $this->files[] = clone $file;
        }
        $this->calculate();
    }

    // Установка опций конвертиции для файлов
    private function calculate(){
        foreach ($this->files as  $key=>$file) {
            $supportedTypes = SupportedMimeTypes::getConvertSupportedTypes();
            // Если есть для это файла возможности конвертации
            if(array_key_exists($file->getType(), $supportedTypes)){
                $arrayConvertTo = $supportedTypes[$file->getType()];
                $this->files[$key]->setConvertTo($arrayConvertTo);
            }
        }
    }

    public function getFiles(){
        return $this->files;
    }

}