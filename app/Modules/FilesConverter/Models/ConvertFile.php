<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 14.04.19
 * Time: 3:00
 */

namespace App\Modules\FilesConverter\Models;


use App\Modules\FilesConverter\Models\FileStatus;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ConvertFile
{

//    private $ok = false;
//    private $file = array();
    private $startFileType;
    private $finishFileType;
    private $filePath;
    private $newFilePath;
    private $newName;

    private $fileIsExist = false;
    private $startFileTypeIsExist = false;
    private $finishFileTypeIsExist = false;
    /**
     * ConvertFile constructor.
     * @param file $file
     * @param string $startType
     * @param array of options $param
     * @param string $finishFormat
     */

    public function setStartFinishFileTypes(string $startFileType, string $finishFileType){
        // Получить все возможные варианты конвертации
        $supForm = SupportedMimeTypes::getConvertSupportedTypes();
        // Если текущий тип присутствует в одном из всех возможных вариантов конвертации
        if(array_key_exists($startFileType, $supForm)){
            // Так же если тип к которому надо привести есть
            // во всех возможных финальных форматах, для данного формата файла
            if(in_array($finishFileType, $supForm[$startFileType])){
                $this->startFileTypeIsExist = true;
                $this->finishFileTypeIsExist = true;
                $this->startFileType = $startFileType;
                $this->finishFileType = $finishFileType;
            }else{
                // FIX
            }
        }
    }

    public function setFilePath(string $filePath){
        if(is_file($filePath)){
            $this->fileIsExist = true;
            $this->filePath = $filePath;
        }else{
            // FIX
        }
    }

    public function createOrClearOutputDir(){
        $pos = strripos($this->filePath, ".");
        $strWithoutExt = substr($this->filePath, 0, $pos);
        $pos = strripos($this->filePath, "/");
        $strWithoutFile = substr($this->filePath, 0, $pos);
        $pos = strripos($strWithoutExt, "/");
        $strFileName = substr($strWithoutExt, $pos+1);

        if(!File::exists($strWithoutFile."/output")){ // Если выходной директории нет, то создать.
            // Создание директории
            File::makeDirectory($strWithoutFile."/output", 0775);
        }else{
            if (file_exists($strWithoutFile."/output")) { // Если выходная директория есть, то очистить.
                foreach (glob($strWithoutFile."/output/*") as $file) {
                    // Удаление файла
                    unlink($file);
                }
            }
        }
    }

    /**
     * @param array $file
     * @param string $startType
     * @param array $param
     * @param string $finishFormat
     */
    public function convert(){

        $pos = strripos($this->filePath, ".");
        $strWithoutExt = substr($this->filePath, 0, $pos);
        $pos = strripos($this->filePath, "/");
        $strWithoutFile = substr($this->filePath, 0, $pos);
        $pos = strripos($strWithoutExt, "/");
        $strFileName = substr($strWithoutExt, $pos+1);

        // Если все параметры верны
        if($this->fileIsExist && $this->startFileTypeIsExist && $this->finishFileTypeIsExist){
            if( // Конвертация
                $this->startFileType == "flac" &&
                $this->finishFileType == "mpga"
            ){
                $newFile =  $strWithoutFile."/output/".$strFileName.".mp3";
                $this->newName = $strFileName.".mp3";
                $command = "ffmpeg -y -i ". "\"" .$this->filePath. " \"" ." -ab 320k -map_metadata 0 -id3v2_version 3 ". "\"" .$newFile. "\"";
                exec($command, $output);
                $this->newFilePath = $newFile;
            }elseif(
                $this->startFileType == "wav" &&
                $this->finishFileType == "mpga"
            ){
                $newFile =  $strWithoutFile."/output/".$strFileName.".mp3";
                $this->newName = $strFileName.".mp3";
                $command = "ffmpeg -y -i ". "\"" .$this->filePath. " \"" ." -codec:a libmp3lame -qscale:a 2 ". "\"" .$newFile. "\"";
                exec($command, $output);
                $this->newFilePath = $newFile;
            }elseif(
                ($this->startFileType == "doc" || $this->startFileType == "docx") &&
                ($this->finishFileType == "pdf" || $this->finishFileType == "docx" || $this->finishFileType == "txt" || $this->finishFileType == "doc")
            ){
                $newFile =  $strWithoutFile."/output/".$strFileName.".$this->finishFileType";
                $this->newName = $strFileName.".".$this->finishFileType;
                $newFileDir = $strWithoutFile."/output/";
                $command = "export HOME=/tmp && libreoffice --headless --convert-to     " . $this->finishFileType . " \"" .$this->filePath. "\"" . " --outdir \"$newFileDir\"";
                exec($command, $output);
                $this->newFilePath = $newFile;
            }elseif(
                ($this->startFileType == "xls" || $this->startFileType == "xlsx") &&
                ($this->finishFileType == "pdf" || $this->finishFileType == "xlsx" || $this->finishFileType == "xls")
            ){
                $newFile =  $strWithoutFile."/output/".$strFileName.".$this->finishFileType";
                $this->newName = $strFileName.".".$this->finishFileType;
                $newFileDir = $strWithoutFile."/output/";
                $command = "export HOME=/tmp && libreoffice --headless --convert-to " . $this->finishFileType . " \"" .$this->filePath. "\"" . " --outdir \"$newFileDir\"";
                exec($command, $output);
                $this->newFilePath = $newFile;
            }elseif(
                ($this->startFileType == "bmp") &&
                ($this->finishFileType == "pdf" || $this->finishFileType == "png" || $this->finishFileType == "jpg")
            ){
                $newFile =  $strWithoutFile."/output/".$strFileName.".$this->finishFileType";
                $this->newName = $strFileName.".".$this->finishFileType;
                $newFileDir = $strWithoutFile."/output/";
                $command = "export HOME=/tmp && libreoffice --headless --convert-to " . $this->finishFileType . " \"" .$this->filePath. "\"" . " --outdir \"$newFileDir\"";
                exec($command, $output);
                $this->newFilePath = $newFile;
            }elseif(
                ($this->startFileType == "jpeg" || $this->startFileType == "png" || $this->startFileType == "bmp" || $this->startFileType == "pdf") &&
                ($this->finishFileType == "pdf")
            ){
                $newFile =  $strWithoutFile."/output/".$strFileName.".$this->finishFileType";
                $this->newName = $strFileName.".".$this->finishFileType;
                $newFileDir = $strWithoutFile."/output/";
                $command = "convert " . " \"" .$this->filePath. "\"" .  " \"".$newFile. "\"";
                exec($command, $output);
                $this->newFilePath = $newFile;
            }
        }
    }

    public function getNewFilePath(){
        $file = str_replace('\"','',$this->newFilePath);
        if(is_file($file)){
            return $file;
        }else{
            return "";
        }
    }

    public function getNewName(){
        return $this->newName;
    }

    public function getNewFileName(){
        $file = str_replace('\"','',$this->newFilePath);
        if(is_file($file)){
            return $file;
        }else{
            return "";
        }
    }

}
