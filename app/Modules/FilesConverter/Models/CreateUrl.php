<?php


namespace App\Modules\FilesConverter\Models;


use Illuminate\Support\Facades\Storage;

class CreateUrl {
    // Создание url ссылки
    public static function createUrl(string $userId, string $fileName):string {
        $str = "upload/".$userId."/output/".$fileName;
        $url = Storage::url($str);
        $url = env('APP_URL').$url;
        return $url;
    }
}
