<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 11.04.19
 * Time: 20:30
 */

namespace App\Modules\FilesConverter\Models;


use App\Modules\FilesConverter\Models\FileStatus;
use App\Modules\FilesConverter\Models\Files;

class DataBaseSaveLoadFileStatus
{

    /**
     * @param array $files
     * @param int $userId
     */
    // Удаление записей из БД и диска
    private function deleteOldFiles(array $files, int $userId){

        $filesRecord = array();

        foreach ($files as  $file) {
            if ($file->getFilePath() != "") {
                array_push($filesRecord, [
                    "path" => $file->getFilePath(),
                ]);
            }
        }

        // Все записи у которых path не такой как в новом files и id от этого пользователя
        $filesDB = Files::whereNotIn('path', $filesRecord)->where('user_id', '=', $userId)->get();

        foreach ($filesDB as $file){
            if(is_file($file->path)){
                unlink($file->path);
            }else{
                // FIX
            }
        }
        // Все записи у которых path не такой как в новом files удалить из БД
        Files::where('user_id', $userId)->delete();
    }

    public function saveFilesToDB(array $files, int $userId): bool{

        if(empty($files)){
            return false;
        }
        // Удаление изи базы и диска старых файлов
        $this->deleteOldFiles($files, $userId);

        $filesPaths = array();

        foreach ($files as  $file) {
            if ($file->getFilePath() != "") {
                array_push($filesPaths, [
                    "path" => $file->getFilePath(),
                ]);
            }
        }

        // Массив строк
        $filesRecord = array();

        // Формирование массива строк
        foreach ($files as  $file){
            if($file->getFilePath() != "") {
                array_push($filesRecord, [
                    "filename" => $file->getFileName(),
                    "path" => $file->getFilePath(),
                    "mimetype" => $file->getMimeType(),
                    "type" => $file->getType(),
                    "convertTo" => json_encode($file->getConvertTo()),
                    // Организовать проверку на уникальность имени
                    "uniqueName" => $file->getUniqueName(),
                    "user_id" => $userId
                ]);
            }
        }

        // Внесение данных в бд
        Files::insert($filesRecord);

        return true;
    }

    public function loadFilesFromDB(int $userId): array {
        // Все записи у которых path не такой как в новом files
        $result = Files::where('user_id', $userId)->get();
        return $result->toArray();
    }

}
