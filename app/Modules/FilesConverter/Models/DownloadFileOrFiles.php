<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 16.04.19
 * Time: 22:17
 */

namespace App\Modules\FilesConverter\Models;


use App\Modules\FilesConverter\Models\CreateUrl;
use Illuminate\Support\Facades\Response;
//use App\Modules\FilesConverter\Models\CreateUrl;

use Zip;



class DownloadFileOrFiles
{
    public function __construct(){}

    private function deleteZipFile($folderPath){
        if(file_exists($folderPath)){
            unlink($folderPath);
        }
    }

    private function clearOutputDir($newFilePath){
        if($newFilePath == ""){
            return;
        }
        $pos = strripos($newFilePath, "/");
        $strWithoutFile = substr($newFilePath, 0, $pos);
        $outputDir =  $strWithoutFile."/*.*";
        $filelist = glob($outputDir);
        foreach ($filelist as $files){
            if($files != $newFilePath){
                unlink($files);
            }
        }
    }

    public function getFiles(string $userId, array $files, string $destinationPath){
        $folderPath = $destinationPath . "/output/file.zip";
        $this->deleteZipFile($folderPath);
        $zip = Zip::create($folderPath);
        foreach ($files as $file){
            $zip->add($file["newFilePath"]);
        }
        $zip->close();
        $url = CreateUrl::createUrl($userId, "file.zip");
        $this->clearOutputDir($folderPath);
        return ["status"=>"success", "url"=> $url, "fileName"=>"file.zip", "Content-Type"=>"application/zip"];
    }
    public function getFile(string $userId, $files, $DESTINATION_PATH){
        $newFilePath = $files[0]["newFilePath"];
        $newFileName = $files[0]["newFileName"];
        $mimeType = $files[0]["mimetype"];
        $url = CreateUrl::createUrl($userId, $newFileName);
        $this->clearOutputDir($newFilePath);
        return ["status"=>"success", "url"=> $url, "fileName"=>$newFileName, "Content-Type"=>$mimeType];
    }
}
