<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 16.04.20
 * Time: 1:11
 */

namespace App\Modules\FilesConverter\Models;

use App\Modules\FilesConverter\Models\FileStatus;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class FileMerge
{
//    private $ok = false;
//    private $file = array();
    private $filePathOne;
    private $filePathTwo;

    private $startFileTypeOne;
    private $startFileTypeTwo;

    private $finishFileType;

    private $strWithoutFile;

    private $newFilePath;
    private $newName;

    private $fileIsExist = false;
    private $startFileTypeIsExist = false;
    private $finishFileTypeIsExist = false;
    /**
     * ConvertFile constructor.
     * @param file $file
     * @param string $startType
     * @param array of options $param
     * @param string $finishFormat
     */

    public function setStartFinishFileTypes(string $startFileTypeOne, string $finishFileType){
        // Получить все возможные варианты конвертации
        $supForm = SupportedMimeTypes::getConvertSupportedTypes();
        if(array_key_exists($startFileTypeOne, $supForm)){
            if(in_array($finishFileType, $supForm[$startFileTypeOne])){
                    $this->startFileTypeIsExist = true;
                    $this->finishFileTypeIsExist = true;

                    $this->startFileTypeOne = $startFileTypeOne;

                    $this->finishFileType = $finishFileType;
            }else{
                // FIX
            }
        }
    }


    public function setFilePathOne(string $filePath){
        if(is_file($filePath)){
            $this->fileIsExist = true;
            $this->filePathOne = $filePath;

        }else{
            // FIX
        }
    }

    public function setFilePathTwo(string $filePath){
        if(is_file($filePath)){
            $this->fileIsExist = true;
            $this->filePathTwo = $filePath;
        }else{
            // FIX
        }
    }

    public function setOutputPath(string $strWithoutFile){

        $pos = strripos($this->filePathOne, "/");
        $strWithoutFile = substr($this->filePathOne, 0, $pos);

        $this->strWithoutFile = $strWithoutFile;
        if(!File::exists($strWithoutFile."/output")){
            File::makeDirectory($strWithoutFile."/output", 0775);
        }
    }



    /**
     * @param array $file
     * @param string $startType
     * @param array $param
     * @param string $finishFormat
     */
    public function convert(){

//        $pos = strripos($this->filePath, ".");
//        $strWithoutExt = substr($this->filePath, 0, $pos);
//        $pos = strripos($this->filePath, "/");
//        $strWithoutFile = substr($this->filePath, 0, $pos);
//        $pos = strripos($this->strWithoutExt, "/");
//        $strFileName = substr($strWithoutExt, $pos+1);


        // Если все параметры верны
        if($this->fileIsExist && $this->startFileTypeIsExist && $this->finishFileTypeIsExist){
            if(
                ($this->startFileTypeOne == "jpeg" || $this->startFileTypeOne == "png" || $this->startFileTypeOne == "bmp" || $this->startFileTypeOne == "pdf") &&
                ($this->finishFileType == "pdf")
            ){
//                $newFile =  $this->strWithoutFile."/output/".$strFileName.".$this->finishFileType";
//                $this->newName = $strFileName.".".$this->finishFileType;
                $newFileDir = $this->strWithoutFile."/output/";
                $newFilePath = $newFileDir.uniqid().".".$this->finishFileType;

                $filePathTwo = "";
                if($this->filePathTwo != ""){
                    $filePathTwo = " \"" .$this->filePathTwo. "\"";
                }

                $command = "convert " . " \"" .$this->filePathOne. "\"" . $filePathTwo .  " \"".$newFilePath. "\"";
                exec($command, $output);
                $this->newFilePath = $newFilePath;
            }
        }
    }

    public function getNewFilePath(){
        $file = str_replace('\"','',$this->newFilePath);
        if(is_file($file)){
            return $file;
        }else{
            return "";
        }
    }

    public function getNewName(){
        return $this->newName;
    }

    public function getNewFileName(){
        $file = str_replace('\"','',$this->newFilePath);
        if(is_file($file)){
            return $file;
        }else{
            return "";
        }
    }


}