<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 09.04.19
 * Time: 22:21
 */

namespace App\Modules\FilesConverter\Models;

use JsonSerializable;


class FileStatus implements JsonSerializable
{

    protected $filename;
    protected $filepath;
    protected $mimetype;
    protected $type;
    protected $convertTo = array();
    protected $uniqueName;
//    private $supportedFile = false;

    public function set($filename, $filepath, $mimetype, $type){
        $this->filename = $filename;
        $this->filepath = $filepath;
        $this->mimetype = $mimetype;
        $this->type = $type;
        $this->uniqueName = uniqid();
    }
    public function setFilepath($filepath){
        return $this->filepath = $filepath;
    }
    public function setConvertTo(array $convertTo){
        return $this->convertTo = $convertTo;
    }


    public function getFileName(){
        return $this->filename;
    }
    public function getFilePath(){
        return $this->filepath;
    }
    public function getMimeType(){
        return $this->mimetype;
    }
    public function getType(){
        return $this->type;
    }
    public function getConvertTo(){
        return $this->convertTo;
    }
    public function getUniqueName(){
        return $this->uniqueName;
    }

    public function jsonSerialize()
    {
        return
            [
                'filename'   => $this->getFileName(),
                //'filepath'   => $this->getFilePath(),
                'mimetype'   => $this->getMimeType(),
                'type'   => $this->getType(),
                'convertTo'   => $this->getConvertTo(),
                'uniqueName' => $this->getUniqueName()
            ];
    }

}