<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 07.04.19
 * Time: 20:23
 */

namespace App\Modules\FilesConverter\Models;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use App\Modules\FilesConverter\Models\FileStatus;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;

class FileUploader{

    private $fileStatus;

    // Финальные директории
    private function outputDir($destinationPath){

        // Создание директории для сохранения начальных файлов
//        if(!File::exists($destinationPath)){
//            // Создание директории
//            File::makeDirectory($destinationPath, 0775);
//        }
        // Создание директории для сохранения начальных файлов
        if(!File::exists($destinationPath)){
            // Создание директории
            File::makeDirectory($destinationPath, 0775, true);
        }
        // Создание директории для сохранения конечных файлов
        if(!File::exists($destinationPath."/output")){ // Если выходной директории нет, то создать.
            // Создание директории
            File::makeDirectory($destinationPath."/output", 0775);
        }
    }

    // Скачивание файлов
    public function upload(UploadedFile $file, string  $destinationPath): bool{
        //Правила валидации
        $rules = array(
            'filename' => SupportedMimeTypes::getRequired()
        );
        // Валидация
        try {
            $validator = Validator::make(array('filename' => $file), $rules);
            // Если успех: сохраняем файл, увеличиваем счётчик загруженных файлов, запоминаем путь к файлу
            if($validator->passes()){
                // Пусть сохранения
//                $destinationPath = $destinationPath storage_path()."/app/public/upload"."/$userID";
                // Настройка выходной директории
                $this->outputDir($destinationPath);
                // Оригинальное имя файла
                $filename = $file->getClientOriginalName();
                // Сохраняем файл на диск
                $upload_success = $file->move($destinationPath, $filename);
                // Пусть к файлу
                $path = $upload_success->getPathname();
                // MimeType сохранённого файла
                $mimeType = $upload_success->getMimeType();
                // MimeTypeShort
                $types = new MimeTypeExtensionGuesser();
                $type = $types->guess($mimeType);
                // Создаём контейнер
                $this->fileStatus = new FileStatus();
                // Записываем в контейнер
                $this->fileStatus->set($filename, $path, $mimeType, $type);
                // Если валидация не пройдена, то данный тип не поддерживается
            }else{
                // Оригинальное имя файла
                $filename = $file->getClientOriginalName();
                // Создаём контейнер
                $this->fileStatus = new FileStatus();
                // Записываем в контейнер
                $this->fileStatus->set($filename, "", "", "");
            }
            return true;
        }catch (ValidationException $e){
            return false;
        }
    }

    public function get(): FileStatus {
        return $this->fileStatus;
    }

}
