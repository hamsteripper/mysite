<?php

namespace App\Modules\FilesConverter\Models;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $fillable = [
        'filename', 'path', 'mimetype', 'type', 'user_id', 'convertTo', 'uniqueName'
    ];

    protected $casts = [
        'convertTo' => 'array',
    ];
}
