<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 09.04.19
 * Time: 20:29
 */

namespace App\Modules\FilesConverter\Models;

class SupportedMimeTypes{

    private static $SUPPORTED_FOMATS = ["doc", "docx", "xls", "xlsx", "jpeg", "pdf", "png", "flac", "wav"];

    private static $CONVERT_SUPPORTED_TYPES = [
        "doc"   =>  ["pdf", "docx", "txt"],
        "docx"  =>  ["pdf", "doc", "txt"],

        "xlsx"  =>  ["pdf", "xls"],
        "xls"   =>  ["pdf", "xlsx"],


        "jpeg"   =>  ["pdf"],
        "png"   =>  ["pdf"],
        "pdf"   =>  ["pdf"],
//        "bmp"   =>  ["jpg", "png", "pdf"],


        "flac" => ["mpga"],
        "wav" => ["mpga"]
    ];

    // Файлы, которые можно добавлять в один файл
    private static $ALL_IN_ONE_ARE_SUPPORTED = [
        "pdf" => ["pdf", "jpeg", "png"]
    ];

    private static $STR = 'required|mimes:';

    public static function getRequired(){
        return $str = self::$STR . implode(",", self::$SUPPORTED_FOMATS);
    }

    public static function getTypes(){
        return self::$SUPPORTED_FOMATS;
    }

    public static function getConvertSupportedTypes(){
        return self::$CONVERT_SUPPORTED_TYPES;
    }

    public static function getAllInOneSupportedTypes(){
        return self::$ALL_IN_ONE_ARE_SUPPORTED;
    }

}