<?php

// Загрузка начальной станицы
Route::group(
    [
        'namespace' => 'App\Modules\FilesConverter\Controllers',
        'as' => 'FilesConverter.',
        'middleware' => ['web','auth']
        ],
    function(){
        Route::get('/FilesConverter', ['uses' => 'FilesConverter@index']);
    }
);

// Пересылаем файл
Route::group(
    [
        'namespace' => 'App\Modules\FilesConverter\Controllers',
        'as' => 'FilesConverter.',
        'middleware' => ['web','auth']
    ],
    function(){
        Route::post('/MultipleFileSelection', ['uses' => 'FilesConverter@multipleFileSelection']);
    }
);

//  Получаем выбранные конвертации от пользователя
Route::group(
    [
        'namespace' => 'App\Modules\FilesConverter\Controllers',
        'as' => 'FilesConverter.',
        'middleware' => ['web','auth']
    ],
    function(){
        Route::post('/Сonversion', ['uses' => 'FilesConverter@conversion']);
    }
);

