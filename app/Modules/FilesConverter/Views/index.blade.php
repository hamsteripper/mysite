@extends('layouts.mainBlade')

@section('css2')
    <link href="assets_css/FilesConverter.css"  rel="stylesheet">
@endsection

@section('content2')
    <br><br><br>
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-8">
                {{--Форма выбора файла или файлов--}}
                <form action="{{url('MultipleFileSelection')}}" method="post" enctype="multipart/form-data" id="avatar">
                    {{--Обязательный токен--}}
                    {{ csrf_field() }}
                    <div class="row justify-content-center align-items-center">
                        <div class="col-md-10">
                            {{--Хранение и отображение файла или файлов--}}
                            <div class="form-group">
                                <div class="custom-file" id="customFile">
                                    <input style="display: none;" id="selectField" type="file" name="filename[]" class="custom-file-input inputfile inputfile-2" data-multiple-caption="{count} files selected" style="display: none" multiple="" />
                                    <label for="selectField">
                                        <label id="photo" name="photo" class="custom-file-control" for="selectField">Choose file</label>
                                    </label>
                                    <br>
                                    <span id="outputMulti"></span>
                                </div>
                            </div>
                            {{--Строка для кнопки отправки формы--}}
                            <div class="row justify-content-center align-items-center">
                                <button type="submit" class="btn btn-primary submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js2')
    <script type="text/javascript" src="assets/FilesConverter_DisplayTheSelectedFiles.js"></script>
@endsection
