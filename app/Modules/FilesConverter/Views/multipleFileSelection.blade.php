@extends('layouts.mainBlade')

@section('css2')
    <link href="assets_css/FilesConverter.css"  rel="stylesheet">
@endsection

@section('content2')
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-8">
                <form id="send_files" name="send_files" action="" method="post" enctype="multipart/form-data"  id="avatar">
                    {{ csrf_field() }}
                    <div class="row justify-content-center align-items-center">
                        <div class="col-md-10">
                            {{--Добавляются списки--}}
                            <div class="form-group" id="myGroup"></div>
                            <div class="row justify-content-center align-items-center">
                                <button id="submit" type="button" class="btn btn-primary submit" onclick="onSubmit()">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js2')
    <script src="{{ asset('assets/FilesConverter_CreateSelectForm.js') }}"></script>
    <script>
        var phpJson =  <?php echo $files;?>;
        main_CreateSelectForm(phpJson);
    </script>
    <script src="{{ asset('assets/senderExpansion.js') }}"></script>
@endsection
