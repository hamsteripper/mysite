// Получение и обработка параметров от php
function main_CreateSelectForm(json){
    var group = document.getElementById('myGroup');
    group.appendChild(document.createElement("hr"));
    for (var key in json) {
        if (json.hasOwnProperty(key)) {
            group.appendChild(createLableForm(json[key], key));
            group.appendChild(filesConverter_CreateSelectForm(json[key], key));
            group.appendChild(document.createElement("hr"));
        }
    }
}
// Создать и вернуть список
function filesConverter_CreateSelectForm(file) {
    var select = document.createElement("select");
    select.name = file.uniqueName;
    if(file.type == ""){ // Если тип для конвертирования не найден, то добавить элемент 0 и выйти
        select.className = "custom-select my-1 mr-sm-2 bg-danger";
        var el = document.createElement("option");
        el.id = 0;
        el.textContent = "Current File Type Not Found";
        el.value = "CurrentFileTypeNotFound";
        select.appendChild(el);
        return select;
    }else{ // Иначе добавить возможность выбора
        select.className = "custom-select my-1 mr-sm-2 bg-success";
        var el = document.createElement("option");
        el.id = 0;
        el.textContent = "Choose New File Format";
        el.value = "ChooseNewFileFormat";
        select.appendChild(el);
    }
    // Добавить список всех возможных форматов
    for(var key in file.convertTo) {
        var format = file.convertTo[key];
        var el = document.createElement("option");
        el.id = key;
        el.textContent = format;
        el.value = format;
        select.appendChild(el);
    }
    return select;
}

function createLableForm(file, lableId) {
    var label = document.createElement("label");
    label.id = lableId;
    label.className = "my-1 mr-2";
    label.textContent = file.filename;
    return label;
}
