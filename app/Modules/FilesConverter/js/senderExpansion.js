function onSubmit(token) {
    sendAjaxForm('send_files', "/Сonversion");
}

function redirect(){
    location.href = '/FilesConverter';
}

// Отправка формы на сервер
function sendAjaxForm(ajax_form, url) {

    var data = new FormData();

    var form_data = $('#'+ajax_form).serializeArray();
    $.each(form_data, function (key, input) {
        data.append(input.name, input.value);
    });

    $.ajax({
        url: url,
        method: "post",
        processData: false,
        contentType: false,
        data: data,
        success: function (data) {
            var dt = JSON.parse(data);
            if(dt["status"] === "success"){
                var a = document.createElement("a");
                a.style.display = 'none';
                a.href = dt["url"];
                a.setAttribute("target","_blank");
                document.body.appendChild(a);
                a.click();
                setTimeout(redirect, 7000);
            }else if(dt["status"] === "error"){
                alert(dt["status"]);
            }else{
                alert("!?");
            }
        },
        error: function (e) {
            alert("Ошибка отправки");
        }
    });

}
