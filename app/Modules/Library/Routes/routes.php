<?php


Route::group(
    [
        'prefix' => 'Library',
        'namespace' => 'App\Modules\Library\Controllers',
        'middleware' => ['web','auth'],
    ],
    function () {
        Route::get('/index', 'LibraryMainController@index');
//        Route::match(['get', 'post'], 'create', 'Crud3Controller@create');
//        Route::match(['get', 'put'], 'update/{id}', 'Crud3Controller@update');
//        Route::delete('delete/{id}', 'Crud3Controller@delete');
    }
);