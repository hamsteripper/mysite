<?php

namespace App\Modules; /** * Сервис провайдер для подключения модулей */

use App\Modules\FilesConverter\Controllers\FileConverter;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ModulesServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function boot(){


//        $modules = array(
//            'Test',
//            'DashboardController',
//            'FilesConverter'
//        );
        //получаем список модулей, которые надо подгрузить
        $modules = config("module.modules");
        if($modules) {
            foreach ($modules as $module){
                //Подключаем роуты для модуля
                if(file_exists(__DIR__.'/'.$module.'/Routes/routes.php')){
                    $this->loadRoutesFrom(__DIR__.'/'.$module.'/Routes/routes.php');
                }
                //Загружаем Views
                //view('Test::admin')
                if(is_dir(__DIR__.'/'.$module.'/Views')) {
                    $this->loadViewsFrom(__DIR__.'/'.$module.'/Views', $module);
                }
                //Подгружаем миграции
                if(is_dir(__DIR__.'/'.$module.'/Migration')) {
                    $this->loadMigrationsFrom(__DIR__.'/'.$module.'/Migration');
                }
                //Подгружаем переводы
                //trans('Test::messages.welcome')
                if(is_dir(__DIR__.'/'.$module.'/Lang')) {
                    $this->loadTranslationsFrom(__DIR__.'/'.$module.'/Lang', $module);
                }

                if(is_dir(__DIR__.'/'.$module.'/js')) {
                    $this->publishes([
                        __DIR__.'/'.$module.'/js' => public_path('assets'),
                    ], 'public');
                    // NEED =>   php artisan vendor:publish
                }

                if(is_dir(__DIR__.'/'.$module.'/resource')) {
                    $this->publishes([
                        __DIR__.'/'.$module.'/resource' => public_path('resource'),
                    ], 'public');
                    // NEED =>   php artisan vendor:publish
                }

                if(is_dir(__DIR__.'/'.$module.'/css')) {
                    $this->publishes([
                        __DIR__.'/'.$module.'/css' => public_path('assets_css'),
                    ], 'public');
//                    // NEED =>   php artisan vendor:publish
                }
            }
        }
    }

    public function register(){
//        $this->app->bind('App\Modules\FilesConverter\Controllers\FileConverter', function ($app) {
//            return new FileConverter();
//        });
    }
}

?>