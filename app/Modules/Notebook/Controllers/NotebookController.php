<?php
namespace App\Modules\Notebook\Controllers;

use App\Modules\Notebook\Models\Note;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

class NotebookController extends Controller
{
    public function index(Request $request){
        $tree = $this->createTree();
        return view('Notebook::index', ["data" => $tree]);
    }

    private function createTree(){
        $tree = [];
        $note = new Note();
        $result = $note->get()->toArray();
        foreach ($result as $r){
            // Удаление первого символа
            $id = $r["id"];
            $path = $r["path"];
            $document_name = $r["document_name"];
            $path = explode("/", $path);
            $this->addTreeElement($tree, $path, $document_name, $id);
        }
        return json_encode($tree);
    }

    // Заполнение в глубину
    private function addTreeElement(array &$tree, $path, $document_name, $id){
        if(count($path)  == 1) {
            // Присвоение нового файла массиву переданому по ссылке
            $file = ["name"=>$document_name, "type"=>"file", "id"=>$id];
            $tree[] =& $file;
            return;
        }elseif(count($path)  > 1){
            $name = $path[0];
            array_shift($path);
            $folder = null;
            // Смотрим есть ли такая папка
            for($i = 0; $i < count($tree); $i++){
                if($tree[$i]["type"] == "folder" && $tree[$i]["name"]  == $name){
                    $folder =& $tree[$i];
                    break;
                }
            }
            // Если папки нет, то создать новую, иначе создать новую и передать дальше
            if($folder == null){
                $folder = ["name"=>$name, "type"=>"folder", "contents"=>[]];
                // !!!!!
                $tree[] =& $folder;
                $this->addTreeElement($folder["contents"], $path, $document_name, $id);
                return;
            }else{
                $this->addTreeElement($folder["contents"], $path, $document_name, $id);
                return;
            }
        }
    }

    public function addNewFileToDB(Request $request){
        $note = new Note();
        $note->user_id = Auth::id();
        $note->path = $request->path;
        if($request->document_name == null){
            $note->document_name = "";
        }else{
            $note->document_name = $request->document_name;
        }
        $id = null;
        try {
            $note->save();
            $id = $note->id;
        }catch (\Exception $e){
            Log::error($e);
            return response()->json(['error']);
        }
        return response()->json(["id" => $id]);
    }

    public function saveFile(Request $request)
    {
        Log::debug($request->text);

        try {
            Note::where('id', $request->id)->update(['text' => $request->text]);
        }catch (\Exception $e){
            Log::error($e);
            return response()->json(['error']);
        }
        return response()->json(['save']);
    }

    public function loadFile(Request $request){
        try {
            $v = Note::where('id', $request->id)->get()->toArray()[0];
        }catch (\Exception $e){
            Log::error($e);
            return response()->json(['error']);
        }
        return response()->json($v);
    }

    public function deleteFile(Request $request){
        try {
            $v = Note::where('id', $request->id)->delete();
        }catch (\Exception $e){
            Log::error($e);
            return response()->json(['error']);
        }
        return response()->json($v);
    }

    public function renameFile(Request $request){
        if($request->document_name == null){
            return response()->json(['raname']);
        }else{
            try {
                Note::where('id', $request->id)->update(['document_name' => $request->document_name]);
            }catch (\Exception $e){
                Log::error($e);
                return response()->json(['error']);
            }
        }
        return response()->json(['raname']);
    }

}
