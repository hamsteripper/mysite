<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note_pictures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('note_id')->unsigned()->nullable(false);
            $table->string('name');
            $table->binary('file')->nullable(false);
            $table->timestamps();

            $table->foreign('note_id')->references('id')->on('notes')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note_pictures');
    }
}
