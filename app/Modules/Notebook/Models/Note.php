<?php

namespace App\Modules\Notebook\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
//    protected $table = 'notes';
    protected $fillable = ['id', 'text'];
}
