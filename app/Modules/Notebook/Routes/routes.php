<?php

Route::group(
    [
        'prefix' => 'notebook',
        'namespace' => 'App\Modules\Notebook\Controllers',
        'middleware' => ['web','auth']
    ],
    function(){
        Route::get('/', ['uses' => 'NotebookController@index'])->name('NotebookControllerIndex');
        Route::post('/save', ['uses' => 'NotebookController@save'])->name('NotebookControllerSave');
        Route::post('/addNewFileToDB', ['uses' => 'NotebookController@addNewFileToDB'])->name('NotebookControllerAddNewFileToDB');
        Route::post('/saveFile', ['uses' => 'NotebookController@saveFile'])->name('NotebookControllerSaveFile');
        Route::post('/loadFile', ['uses' => 'NotebookController@loadFile'])->name('NotebookControllerLoadFile');
        Route::post('/deleteFile', ['uses' => 'NotebookController@deleteFile'])->name('NotebookControllerDeleteFile');
        Route::post('/renameFile', ['uses' => 'NotebookController@renameFile'])->name('NotebookControllerRenameFile');
    }
);
