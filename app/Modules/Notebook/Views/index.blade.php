@extends('layouts.mainBlade')

@section('css2')

    <link href="assets_css/notebook.css"  rel="stylesheet">
    <link href="assets_css/toast.css"  rel="stylesheet">
    <script>
        var tree = <?php echo $data; ?>
    </script>
@endsection

@section('content2')
    <div class="container background_inherit_struct col-12" >
        <div class="row">
            <!-- делаем заголовок -->
            <div class="col-12" class="head">
                <h1 id="h1_name">Текущий файл</h1>
            </div>
        </div>
        <div class="row background_inherit_struct col-12">
            {{--Список--}}
            <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 background_inherit_struct">
                <!--Контейнер для вывода дерева каталогов-->
                <div class="tree_container" id="tree_container">
                    <li class="mainFolder"><span>/</span>
                        <ul id="1_folder"></ul>
                    </li>
                </div>
                <div class="background_inherit_struct">
                    <button id="saveFile" type="button" class="btn saveFile" >Save File</button>
                </div>
            </div>

            <!-- делаем само окно редактора, где пишем весь текст -->
            <div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
                <div id="editor" contenteditable="true" class="class-contenteditable" ></div>
            </div>
        </div>
    </div>
    <!--Меню корневой папки-->
    <nav id="folder-mainMenu" class="context-menu ">
        <ul class="context-menu__items">
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="New" id="newFile_mainMenu" ><i class="fa fa-edit"></i>New file</a>
            </li>
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="New" id="newFolder_mainMenu" ><i class="fa fa-edit"></i>New folder</a>
            </li>
        </ul>
    </nav>
    <!--Меню папки-->
    <nav id="folder-menu" class="context-menu">
        <ul class="context-menu__items">
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="New" id="newFile" ><i class="fa fa-edit"></i>New file</a>
            </li>
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="New" id="newFolder" ><i class="fa fa-edit"></i>New folder</a>
            </li>
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete" id="deleteFolder" ><i class="fa fa-times"></i> Delete folder</a>
            </li>
        </ul>
    </nav>
    <!--Меню файла-->
    <nav id="file-menu" class="context-menu">
        <ul class="context-menu__items">
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Rename" id="renameFile"><i class="fa fa-edit"></i>Rename file</a>
            </li>
            <li class="context-menu__item">
                <a href="#" class="context-menu__link" data-action="Delete" id="deleteFile"><i class="fa fa-times"></i>Delete file</a>
            </li>
        </ul>
    </nav>
    {{--Окно ввода имени папки--}}
    <nav id="newFolder-menu" class="context-menu">
        <input type="text" id="newFolderNameInput" class="form-control" placeholder="Введите имя новой папки"  aria-describedby="basic-addon1">
    </nav>
    {{--Окно ввода имени файла--}}
    <nav id="newFile-menu" class="context-menu">
        <input  type="text" id="newFileNameInput" class="form-control" placeholder="Введите имя нового файла" aria-describedby="basic-addon1">
    </nav>
    {{--Окно подтверждения удаления файла--}}
    <nav id="deleteFile-menu" class="context-menu" style="text-align: center;">
        <button class="btn deleteFileYes" id="deleteFileButtonYes">Yes</button>
        <button class="btn deleteFileNo" id="deleteFileButtonNo">NO</button>
    </nav>
    {{--Окно подтверждения удаления папки--}}
    <nav id="deleteFolder-menu" class="context-menu" style="text-align: center;">

        <div><span>Удалить папку и все вложенные файлы ?</span></div>
        <div>
            <button class="btn deleteFileYes" id="deleteFileButtonYes">Yes</button>
            <button class="btn deleteFileNo" id="deleteFileButtonNo">NO</button>
        </div>
    </nav>
    {{--Окно редактирования имени файла--}}
    <nav id="editFile-menu" class="context-menu">
        <input type="text" id="editFileNameInput" class="form-control" placeholder="Введите новое имя файла" aria-describedby="basic-addon1">
    </nav>
    

@endsection

@section('js2')
    <script type="text/javascript" src="assets/toast.js"></script>
    <script type="text/javascript" src="assets/notebook.js"></script>
    <script>


    </script>
@endsection
