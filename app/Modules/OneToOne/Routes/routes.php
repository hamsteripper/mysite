<?php


Route::group(
    [
        'prefix' => 'oneToOne',
//        'namespace' => 'App\Modules\OneToOne\Controllers',
        'middleware' => ['web','auth'],
    ],
    function () {
//        Route::get('/user', 'App\Modules\OneToOne\Controllers\UserController@index')->name('users.index');

        Route::resource('/user', 'App\Modules\OneToOne\Controllers\UserController');
        Route::resource('/animal', 'App\Modules\OneToOne\Controllers\AnimalController');

    }
);



