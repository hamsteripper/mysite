@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{route('user.index')}}" class="btn btn-primary">Назад</a>
        <hr>
        <ul>
            <li>Name: {{$user->name}}</li>
            <li>Email: {{$user->email}}</li>
            <li>Animal: {{$user->animal->name_animal}}</li>
        </ul>
    </div>
@endsection