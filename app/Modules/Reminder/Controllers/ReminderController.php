<?php

namespace App\Modules\Reminder\Controllers;

use App\Http\Controllers\Controller;

//use Illuminate\Http\Response;

use App\Modules\Reminder\Models\DB\Reminder;
use App\Modules\TelegramBot\Models\DB\TgUsers;
use App\Modules\TelegramBot\Models\Response;
use DateTime;

class ReminderController extends Controller
{

    public static function index(){
        $reminder = Reminder::all()->toArray();

        foreach ($reminder as $r){
            self::step1($r);
        }
    }

    public static function step1($r){
        if($r["text"] != null && $r["date_reminder"] != null){ // Если это единичное оповещение
            $first = DateTime::createFromFormat('dY-m-d H:i:s', $r["date_reminder"]);
            $date = new DateTime();
            $second = $date->format('Y-m-d H:i:s');
            $second = DateTime::createFromFormat('dY-m-d H:i:s', $second);
            if($second >= $first){
                self::step2($r, 'one');
                Reminder::where('id', $r["id"])->delete();
            }
        }elseif ($r["text"] != null && $r["repeat"] != null & $r["time"] != null){ // Если это постоянное оповещение
            // Текущая дата
            $date = new DateTime();
            // Текущая дата (день-месяц-год)
            $currentDate = $date->format('Y-m-d');
            // Дата последнего обновления
            $updateDT = new DateTime($r["updated_at"]);
            // Дата последнего обновления (день-месяц-год)
            $date_update = $updateDT->format('Y-m-d');
            // Текущий день, как число
            $day = date("w", strtotime('now'));
            // Подходящий ли день, для напоминания
            if($r["repeat"] == '8' || $day == $r["repeat"]){
                // Если это новая запись или сегодня обновления не было, значит выполнить
                if($r["updated_at"] == $r["created_at"] || $date_update != $currentDate){
                    $date = new DateTime();
                    $second = $date->format('H:i:s');
                    if($second >= $r["time"] ){
                        self::step2($r, 'many');
                        $date = new DateTime();
                        $second = $date->format('Y-m-d H:i:s');
                        Reminder::where('id', $r["id"])->update(['updated_at' => $second]);
                    }
                }
            }
        }
    }

    public static function step2($r, $s){
        $user_id = $r["user_id"];
        $tgUser = TgUsers::where('user_id', $user_id)->first();
        if($s == "one") {
            Response::send(["chat_id" => $tgUser["telegram_user_id"], "text" => "Одиночное напоминание:"]);
            Response::send(["chat_id"=>$tgUser["telegram_user_id"], "text"=>$r["text"]]);
        }elseif($s = "many") {
            Response::send(["chat_id" => $tgUser["telegram_user_id"], "text" => "Постоянное напоминание:"]);
            Response::send(["chat_id"=>$tgUser["telegram_user_id"], "text"=>$r["text"]]);
        }
    }

    public static function list($user_id){
        $reminder = Reminder::where('user_id', $user_id)->get();
        switch ($reminder){
            case null:
                return null;
                break;
            default:
                $reminder = $reminder->toArray();
                return $reminder;
                break;
        }
    }

    public static function delete($id){
        try{
            Reminder::where('id', $id)->delete();
        }catch(\Exception $e){
            Log::error($e);
            // В случе ошибки возвращаем сообщение об ошибке
            return ["status"=>"error"];
        }
        return ["status"=>"success"];
    }
}

