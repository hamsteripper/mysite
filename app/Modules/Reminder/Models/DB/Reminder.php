<?php

namespace App\Modules\Reminder\Models\DB;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    public $timestamps = true;

    protected $fillable = ['created_at'];

//    const CREATED_AT = 'create_time';
//    const UPDATED_AT = 'update_time';
    //
}
