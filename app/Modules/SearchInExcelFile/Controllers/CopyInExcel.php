<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 15.10.18
 * Time: 21:53
 */

namespace App\Modules\SearchInExcelFile\Controllers;


class CopyInExcel{

    private $FileName = null;
    private $uploadDir = null;
    private $arrayPagesNames = null;
    private $excel = null;

    function __construct(){
        include_once(app_path()."/myLibs/files/saveFile.php");
        include_once(app_path()."/myLibs/ExcelModules/copyAndGenInExcel.php");
        $this->uploadDir = app_path()."/TMP/";
    }

    public function show(){
        $this->save();
        $this->excel = new \copyAndGenInExcel();
        $this->excel->load($this->uploadDir.$this->FileName);
        $parameters = $this->createParametersOfCopy(1);
        $this->excel->copyInNewArray($parameters);
//        $parameters = $this->createParametersOfCopy(2);
//        $this->excel->copyInNewArray($parameters);
//        $parameters = $this->createParametersOfCopy(3);
//        $this->excel->copyInNewArray($parameters);
//        $parameters = $this->createParametersOfCopy(4);
//        $this->excel->copyInNewArray($parameters);
//        $parameters = $this->createParametersOfCopy(5);
//        $this->excel->copyInNewArray($parameters);
//        $parameters = $this->createParametersOfCopy(6);
//        $this->excel->copyInNewArray($parameters);
        $this->excel->loadObject($this->uploadDir.$this->FileName);
    }

    private function save(){
        if (count($_POST)>0 and array_key_exists( "userfile", $_FILES)) {
            // Имя файла
            $this->FileName = $_FILES["userfile"]["name"];
            // Сохранение файла
            if (saveFile($this->uploadDir, "userfile")){
                // Сверяем тип файла с нужными нам
                $hand = new \copyAndGenInExcel();
                $file = $this->uploadDir.$_FILES["userfile"]["name"];
                if ($hand->isExcel($file)){
                    $hand->load($file);
                    // Список имён страниц
                    $this->arrayPagesNames = $hand->getPages($file);
                }else{
                    deleteFile($this->uploadDir.$_FILES["userfile"]["name"]);
                    header("Location: ".$_SERVER["HTTP_REFERER"]."&wrongFile=wrongFile");
                    exit;
                }
            }else{
                echo "File not write";
                return;
            }
        }elseif(count($_POST)>0 and array_key_exists( "FileName", $_POST)){
            $this->FileName = $_POST["FileName"];
        }
    }

    private function createParametersOfCopy($i):array{
        $result = array();
        switch($i){
            case 1:
                $result["START"] = "START";
                $result["startPos"] = array("A", 22);
                $result["col"] = array("Наименование"=>"copy","Кол."=>"copy","Ед.изм"=>"copy","Цена1"=>"copy","Стоимость1"=>"copy");
                $result["STOP"] = "Ребилд";//, "ГдеТонер"
                break;
            case 2:
                $result["START"] = "START";
                $result["col"] = array("Наименование"=>"copy","Кол."=>"copy","Ед.изм"=>"copy","Цена2"=>"copy","Стоимость2"=>"copy");
                $result["STOP"] = "БПС"; // , "Айти Корпорейт"
                break;
            case 3:
                $result["START"] = "START";
                $result["col"] = array("Наименование"=>"copy","Кол."=>"copy","Ед.изм"=>"copy","Цена3"=>"copy","Стоимость3"=>"copy");
                $result["STOP"] = "Парнас"; // , "ИП Васюков"
                break;
            case 4:
                $result["START"] = "START";
                $result["col"] = array("Наименование"=>"copy","Кол."=>"copy","Ед.изм"=>"copy","Цена1"=>"copy","Стоимость1"=>"copy");
                $result["STOP"] = "ГдеТонер";//,
                break;
            case 5:
                $result["START"] = "START";
                $result["col"] = array("Наименование"=>"copy","Кол."=>"copy","Ед.изм"=>"copy","Цена2"=>"copy","Стоимость2"=>"copy");
                $result["STOP"] = "Айти Корпорейт"; // ,
                break;
            case 6:
                $result["START"] = "START";
                $result["col"] = array("Наименование"=>"copy","Кол."=>"copy","Ед.изм"=>"copy","Цена3"=>"copy","Стоимость3"=>"copy");
                $result["STOP"] = "ИП Васюков"; // ,
                break;
            default:
                break;
        }
        return $result;
    }

}