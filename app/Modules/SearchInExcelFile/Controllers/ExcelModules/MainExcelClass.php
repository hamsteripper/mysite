<?php
namespace App\Modules\SearchInExcelFile\Controllers\ExcelModules;


use PHPExcel_Settings;
use PHPExcel_IOFactory;

class MainExcelClass{

    protected $fileName = null;
    protected $fileObject = null;
    protected $arrayPagesNames = array();
    private $result = array();

    private $supportedFormats = array(
        "OpenExcel" => "application/vnd.oasis.opendocument.spreadsheet",
        "MSExcel" => "application/vnd.ms-excel",
        "MSExcel2007" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );

    protected $excelIndex = array(
        0 => "",
        1 => "A", 2 => "B", 3 => "C", 4 => "D", 5 => "E",
        6 => "F", 7 => "G", 8 => "H", 9 => "I", 10 => "J",
        11 => "K", 12 => "L", 13 => "M", 14 => "N", 15 => "O",
        16 => "P", 17 => "Q", 18 => "R", 19 => "S", 20 => "T",
        21 => "U", 22 => "V", 23 => "W", 24 => "X", 25 => "Y",
        26 => "Z"
    );

    // Проверка на соответствие формату excel
    public function isExcel($fileName){
        $bool = false;
        foreach ($this->supportedFormats as $value){
            if (mime_content_type($fileName) == $value){
                $bool = true;
                break;
            }
        }
        if(!$bool){
            return false;
        }
        return true;
    }
    // Создать объединённый массив из выбраных страниц
    public function createArrayFromPages($arraySelectedPagesNames){
        // Чтение выбранных таблиц в файл
        return $this->result = $this->createArrayFromFile($this->fileObject, $this->arrayPagesNames, $arraySelectedPagesNames);
    }
    // Создание двухмерного массива в соответствии с выбранными страницами
    Protected function createArrayFromFile($fileObject, $arrayPagesNames, $arraySelectedPagesNames): array{
        $result = array();
        for($i = 0; $i < iterator_count($fileObject->getWorksheetIterator()); $i++){
            if(!in_array($i, $arraySelectedPagesNames)){
                continue;
            }
            $fileObject->setActiveSheetIndex($i);
            $aSheet = $fileObject->getActiveSheet();
            $result[$arrayPagesNames[$i]] = $aSheet->toArray(null,true,true,false);
        }
        return $result;
    }
    // Чтение файла
    public function load($fileName){
        $bool = false;
        foreach ($this->supportedFormats as $value){
            if (mime_content_type($fileName) == $value){
                $bool = true;
                break;
            }
        }
        if(!$bool){
            return "Error Current File Not Excell Format";
        }

        PHPExcel_Settings::setLocale("ru");
        $this->fileName = $fileName;
        $this->fileObject = PHPExcel_IOFactory::load($fileName);
        $this->arrayPagesNames = $this->fileObject->getSheetNames();
        return "Ok";
    }
    // Сохранить результат
    public function saveResult($filePath){
        // create php excel object
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        // read data to active sheet
        $doc->getActiveSheet()->fromArray($this->result, NULL, "A1");
        $objWriter = new PHPExcel_Writer_Excel2007($doc);
        $objWriter->save($filePath);
    }
    // Сохранить результат
    public function loadObject($filePath){
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="filename.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->fileObject, 'Excel2007');
        $objWriter->save('php://output');
    }
    // Отправить результат клиенту
    public function downloadToClient($filePath){
        PHPExcel_Settings::setLocale("ru");
        $objPHPExcel = PHPExcel_IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"result.xlsx\"");
        header("Cache-Control: max-age=0");
        $objWriter->save('php://output');
        exit;
    }
    // Получить список страниц в файле
    public function getPages(): array {
        return $this->arrayPagesNames;
    }
    // Возврат результата
    public function getResult(){
        return $this->result;
    }

}