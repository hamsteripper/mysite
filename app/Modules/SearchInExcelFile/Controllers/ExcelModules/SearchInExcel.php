<?php

namespace App\Modules\SearchInExcelFile\Controllers\ExcelModules;

//include_once(app_path()."/myLibs/ExcelModules/mainExcelClass.php");
use App\Modules\SearchInExcelFile\Controllers\ExcelModules\MainExcelClass;
use mysqli;
use PDO;
use SpreadSheet;

class SearchInExcel extends MainExcelClass {

    private $arraySelectedPagesNames = null;
    private $whatFountArray = null;
    private $pagesArray = array();
    private $result = array();

    private $dsn = "";
    private $db_serverName = "";
    private $db_base = "";
    private $db_user = "";
    private $db_pass = "";
    private $db_tick = "";
    private $db_prefix = "";

    public function __construct()
    {
        include_once (public_path()."/dhtmlxSpreadsheet_v21/php/config.php");
        include_once (public_path()."/dhtmlxSpreadsheet_v21/php/api.php");

        $this->dsn = $dsn;
        $this->db_serverName = $db_serverName;
        $this->db_base = $db_base;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_tick = $db_tick;
        $this->db_prefix = $db_prefix;
    }



    private function search(){
        // Чтение выбранных таблиц в файл
        $this->pagesArray = parent::createArrayFromPages($this->arraySelectedPagesNames);
        // Обнуление массива
        $this->result = array();
        // Максимальная строка
        $maxCount = 0;
        // Поочерёдно берём критерий поиска из массива критериев
        $j = 1;
        foreach($this->whatFountArray as $whatFountValue){
            // Флаг добавления индекса
            $addIndex = true;
            // Значение найдено
            $found = false;
            foreach($this->pagesArray as $key=>$currentPageArray ){
                foreach($currentPageArray as $pagesArrayLine ) {
                    for ($i = 1; $i < count($pagesArrayLine); $i++) {
                        if (stripos(strval($pagesArrayLine[$i]), $whatFountValue) == true) {
                            $found = true;
                            if($addIndex){
                                $addIndex = false;
                                array_push($this->result, array($j++));
                            }
                            array_push($this->result, array_merge(array("","$whatFountValue",$key,""),$pagesArrayLine));
                            // Строка максимального размера
                            if(count($pagesArrayLine) >= $maxCount){
                                $maxCount = count($pagesArrayLine);
                            }
                        }
                    }
                }
            }

            if(!$found){
                array_push($this->result, array($j++));
                array_push($this->result, array("","$whatFountValue","","","Not Found"));
            }
        }

        $maxCount += 3;

        foreach($this->result as $key=>$arr){
            if(count($arr) < $maxCount) {
                for ($i = count($arr); $i <= $maxCount; $i++) {
                    array_push($this->result[$key], "");
                }
            }
        }
    }

    private function clearDatabase($sheetId){
        // Create connection
        $conn = new mysqli($this->db_serverName, $this->db_user, $this->db_pass, $this->db_base);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // sql to delete a record
        $sql = "DELETE FROM dhx_data WHERE sheetid=$sheetId";
        if ($conn->query($sql) === TRUE) {
            //echo "Record deleted successfully";
        } else {
            //echo "Error deleting record: " . $conn->error;
        }

        $sql = "DELETE FROM dhx_header WHERE sheetid=$sheetId";
        if ($conn->query($sql) === TRUE) {
            //echo "Record deleted successfully";
        } else {
           // echo "Error deleting record: " . $conn->error;
        }

        $sql = "DELETE FROM dhx_sheet WHERE sheetid=$sheetId";
        if ($conn->query($sql) === TRUE) {
            //echo "Record deleted successfully";
        } else {
            //echo "Error deleting record: " . $conn->error;
        }

        $sql = "DELETE FROM dhx_triggers WHERE sheetid=$sheetId";
        if ($conn->query($sql) === TRUE) {
            //echo "Record deleted successfully";
        } else {
            //echo "Error deleting record: " . $conn->error;
        }
        $conn->close();

    }

    public function writeToDatabase($sheetId){

        $this->clearDatabase($sheetId);

        $res = new PDO($this->dsn, $this->db_user, $this->db_pass);
        $sh = new SpreadSheet($res, strval($sheetId), array("prefix"=>$this->db_prefix, "tick"=>$this->db_tick));

        for($i = 0; $i < count($this->result); $i++){
            $x = 0;
            for($j = 0; $j < count($this->result[$i]); $j++){
//                echo intdiv($j, 26);
                $column = $this->excelIndex[intdiv($j, 26)].$this->excelIndex[($j) % 26 + 1];
                $line = strval($i+1);
                $value = $this->result[$i][$j];
                $sh->setValue($column.$line, $this->result[$i][$j]);
                $x++;
            }
        }
//        exit;

    }

    public function load($fileName){
        parent::load($fileName);
    }

    public function searchArray($whatFountArray, $arraySelectedPagesNames) : array{
        $this->whatFountArray = $whatFountArray;
        $this->arraySelectedPagesNames = $arraySelectedPagesNames;
        $this->search();
        return $this->result;
    }
    public function searchString($whatFountString, $arraySelectedPagesNames) : array {
        $this->whatFountArray = explode("\n", $whatFountString);
        foreach ($this->whatFountArray as $key=>$als){
            $this->whatFountArray[$key] = trim($this->whatFountArray[$key]);
        }
        $this->arraySelectedPagesNames = $arraySelectedPagesNames;
        $this->search();
        return $this->result;
    }

}