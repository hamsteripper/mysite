<?php

namespace App\Modules\SearchInExcelFile\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\SearchInExcelFile\Controllers\ExcelModules\SearchInExcel;

class SearchInFile extends Controller
{

//    private  $excelArray = array();
    private  $FileName = "";
    private  $textarea = "";
    private  $whatFountArray = array();


    private $start;

    function __construct() {
        include_once(app_path()."/Modules/SearchInExcelFile/Controllers/files/saveFile.php");
//        include_once(app_path()."/myLibs/ExcelModules/searchInExcel.php");
    }

    function __destruct()
    {
//        echo 'Время выполнения скрипта: '.(microtime(true) - $this->start).' сек.';
    }

    public function getFile(Request $request)
    {
        if (count($_POST)>0 and array_key_exists( "userfile", $_FILES)) {
            // Имя файла
            $this->FileName = $_FILES["userfile"]["name"];
            // Директория для зпшрузки файла
            $this->uploadDir = app_path()."/";
            // Сохранение файла
            if (saveFile($this->uploadDir, "userfile")){
                // Сверяем тип файла с нужными нам
                $hand = new SearchInExcel();
                $file = $this->uploadDir.$_FILES["userfile"]["name"];
                if ($hand->isExcel($file)){
                    $hand->load($file);
                    $arrayPagesNames = $hand->getPages($file);
                    // Список имён страниц
                }else{
                    deleteFile($this->uploadDir.$_FILES["userfile"]["name"]);
                    header("Location: ".$_SERVER["HTTP_REFERER"]."&wrongFile=wrongFile");
                    exit;
                }
            }else{
                echo "File not write";
                return;
            }
        }elseif(count($_POST)>0 and array_key_exists( "FileName", $_POST)){
            $this->FileName = $_POST["FileName"];
        }

        $arrayOut = [
            "FileName"=>$this->FileName,
            "textarea"=>$this->textarea,
            "hidden"=>"hidden",
            "arrayPagesNames"=>$arrayPagesNames,
            "arrayPagesNamesChecked"=>array()
        ];

        return view("SearchInExcelFile::searchInExcel.searchInExcel", $arrayOut);
    }

    private function multiexplode ($delimiters,$string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    public function searchAndShow(Request $request){
        if((count($_POST)>0) and (array_key_exists( "textarea", $_POST)) and ($_POST["textarea"]!=null)){

            // Строка критериев
            $this->textarea = $_POST["textarea"];
            // Имя файла
            $this->FileName = $_POST["FileName"];
            // Обработчик excel файла
            $hand = new SearchInExcel();
            $hand->load(app_path()."/".$this->FileName);
            // Список имён страниц
            $arrayPagesNames = $hand->getPages(app_path()."/".$this->FileName);
            // Список имён страниц (массив)
            $arrayPagesNamesChecked = array();
            // Массив критериев поиска (если разделитель enter)
            $this->whatFountArray = $this->multiexplode(array(",","|","\n"), $this->textarea);
            // Очистка от спецсимволов и пробелов
            foreach ($this->whatFountArray as $key=>$als){
                $this->whatFountArray[$key] = trim($this->whatFountArray[$key]);
            }

            foreach ($arrayPagesNames as $key=>$value1) {
                // Запоминаем, какие чекбоксы были выделены
                if (array_key_exists("checkbox" . $key, $_POST)) {
                    array_push($arrayPagesNamesChecked, $key);
                } else {
                    continue;
                }
            }
            // Найденные строки в массиве
            $hand->searchArray($this->whatFountArray, $arrayPagesNamesChecked);
            $result = $hand->getResult();
            $hand->writeToDatabase(10000);

            $arrayOut = [
                "FileName"=>$this->FileName,
                "textarea"=>$this->textarea,
                "hidden"=>"submit",
                "stack"=>$result,
                "arrayPagesNames"=>$arrayPagesNames,
                "arrayPagesNamesChecked"=>$arrayPagesNamesChecked
            ];

            return view("SearchInExcelFile::searchInExcel.searchInExcel",$arrayOut);
        }else{
            echo "Pages not select";
        }
    }

    public function downloadFile(Request $request){
        $hand = new \searchInExcel();
        $hand->downloadToClient(app_path()."exp.xslx");
    }
}