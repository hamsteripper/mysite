<?php

namespace App\Modules\SearchInExcelFile\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SelectExcelFile extends Controller
{
    public function show(){
        // Обработчик исключения, на случай, если данный передаваемый файл не excel
        // или отсутствуют необходимые параметры перехода
        if(count($_GET)>0 and isset($_GET['wrongFile'])){
            echo "<script>alert(\"Переданный тип файла не excel\");</script>";
//            var_dump($_SERVER);
//            unset($_GET['wrongFile']);
            //'FilesConverter::multipleFileSelection'
            return view("SearchInExcelFile::selectExcelFile");
//            header("Location: ".$_SERVER["HTTP_REFERER"]."&wrongFile=wrongFile");
        }

        if(count($_GET)>0 and isset($_GET['function'])) {
            return view("SearchInExcelFile::selectExcelFile");
        }else{
            echo "<script>alert(\"Неверные параметры\");</script>";
        }
    }
}
