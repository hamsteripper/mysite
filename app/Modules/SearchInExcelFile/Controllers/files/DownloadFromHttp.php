<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 08.01.19
 * Time: 18:14
 */

namespace App\myLibs\files;

use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class DownloadFromHttp
{

    static public function downloadFile($pathIn, $pathOut, $GuzzleHttpOptions){

        $i = 0;

        while(true){
            try {
                $i = strripos($pathIn, ".");
                $expansion = strtolower(substr($pathIn, $i));
                $tmpFileName = uniqid(strftime('%G-%m-%d'));
                $tmpFilePath  = $pathOut."/".$tmpFileName.$expansion;
                $resource = fopen($tmpFilePath, 'w');
                $stream   = stream_for($resource);

                $client   = new Client();
                $GuzzleHttpOptions[RequestOptions::SINK] = $stream;
                $response = $client->request('GET', $pathIn, $GuzzleHttpOptions);

                $stream->close();
                break;
                // Добавить в исключения
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                Log::error("BOTERROR = ".$e);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                Log::error("BOTERROR = ".$e);
            } catch (\Telegram\Bot\Exceptions\TelegramSDKException $e) {
                Log::error("BOTERROR = ".$e);
            }


            if($i == 5){
                return "";
            }
            sleep(5);
            $i++;
        }

        if ($response->getStatusCode() === 200) {
            return [$tmpFilePath, $tmpFileName];
        }else{
            return "";
        }
    }
}