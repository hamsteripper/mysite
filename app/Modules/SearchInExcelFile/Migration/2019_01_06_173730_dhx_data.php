<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DhxData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dhx_data', function (Blueprint $table) {
            $table->string('sheetid')->nullable(false);
            $table->integer('columnid')->nullable(false);
            $table->integer('rowid')->nullable(false);
            $table->longText('data')->nullable(true);
            $table->string('style')->nullable(true);
            $table->longText('parsed')->nullable(true);
            $table->longText('calc')->nullable(true);

            $table->primary(['sheetid', 'columnid', 'rowid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dhx_data');
    }
}
