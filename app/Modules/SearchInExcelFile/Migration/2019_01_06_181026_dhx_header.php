<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DhxHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dhx_header', function (Blueprint $table) {
            $table->string('sheetid')->nullable(false);
            $table->integer('columnid')->nullable(false);
            $table->string('label')->nullable(true);
            $table->integer('width')->nullable(true);

            $table->primary(['sheetid', 'columnid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dhx_header');
    }
}
