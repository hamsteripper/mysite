<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DhxSheet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dhx_sheet', function (Blueprint $table) {
            $table->string('sheetid')->nullable(false)->autoIncrement();
            $table->integer('userid')->nullable(true);
            $table->string('name')->nullable(true);
            $table->string('key')->nullable(true);
            $table->string('cfg')->nullable(true);

            $table->primary('sheetid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dhx_sheet');
    }
}
