<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DhxUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dhx_user', function (Blueprint $table) {
            $table->integer('userid')->nullable(false)->autoIncrement();
            $table->string('apikey')->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('secret')->nullable(true);
            $table->string('pass')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dhx_user');
    }
}
