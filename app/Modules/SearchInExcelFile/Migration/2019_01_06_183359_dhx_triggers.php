<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DhxTriggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dhx_triggers', function (Blueprint $table) {
            $table->integer('id')->nullable(false)->autoIncrement();
            $table->string('sheetid')->nullable(true);
            $table->string('trigger')->nullable(true);
            $table->string('source')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dhx_user');
    }
}
