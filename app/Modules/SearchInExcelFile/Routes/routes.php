<?php


Route::group(
    [
        'prefix' => 'searchInExcelFile',
        'namespace' => 'App\Modules\SearchInExcelFile\Controllers',
        'middleware' => ['web','auth'],
    ],
    function () {
        // Общее отображение для выбора файла
        Route::get("/selectExcelFile","SelectExcelFile@show");
        // Всё что относится к поиску в excel
        Route::post("/searchInFileGetFile","SearchInFile@getFile");
        Route::post("/searchInFilesearchAndShow","SearchInFile@searchAndShow");
        Route::post("/searchInFilesearchAndDownloadFile","SearchInFile@downloadFile");
    }
);



