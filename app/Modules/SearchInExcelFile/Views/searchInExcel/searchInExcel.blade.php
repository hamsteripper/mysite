@extends('layouts.mainBlade')

@section('content2')

    {{--<!DOCTYPE html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
{{--<head>--}}
    {{--<meta charset="utf-8">--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    {{--<!-- Fonts -->--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">--}}
    {{--<!-- Главное меню-->--}}
    {{--<link href="{{ asset('css/mainMenu.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <script src="{{ asset('dhtmlxSpreadsheet_v21/spreadsheet.php?skin=dhx_web&sheet=10000&parent=gridbox&math=true') }}"></script>

    {{--<title>RBT</title>--}}

    {{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>--}}
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--}}

    {{--<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>--}}

    <style type="text/css">
        /*<![CDATA[*/
        table {
            width: 100%;
            table-layout: fixed;
            border-collapse: collapse;
            border-spacing: 0;
        }
        .fixed {
            /*width: 70px;*/
            /*height: 20px;*/
            overflow: hidden;
        }

        .topics tr {
            line-height: 14px;
        }
    </style>

    <script>
        $.fn.editable.defaults.mode = 'popup';
        $(document).ready(function() {
            $('.people-editable').editable({
                emptytext: ''
            });
        });
    </script>

    <form action="/searchInExcelFile/searchInFilesearchAndShow" method="post">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <table >
                    <tr>
                        <?php $x = 0; ?>
                        @if(empty($arrayPagesNames))
                        @else

                            @foreach($arrayPagesNames as $value)
                                @if (in_array(strval($loop->index), $arrayPagesNamesChecked))
                                    <td class="fixed">
                                        <a><input type="checkbox" name="{{ "checkbox".strval($loop->index) }}" value="NO" checked>{{$value}}</a>
                                    </td>
                                @else
                                    <td class="fixed">
                                        <a><input type="checkbox" name="{{ "checkbox".strval($loop->index) }}" value="NO"  >{{$value}}</a>
                                    </td>
                                @endif
                                <?php $x++; ?>
                                <?php
                                if($x % 4 == 0){
                                    echo "</tr>";
                                    echo "<tr>";
                                }
                                ?>
                            @endforeach

                        @endif
                    </tr>
                </table>
            </div>
        </div>

        <br/>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="FileName" value="{{$FileName}}">
        <div class="container ">
            <div class="row justify-content-center align-items-center">
                Что искать ?: <input class="form-control input-lg" name="textarea" value="{{$textarea}}">
                <input type="submit" value="Search in excel" /><br />
            </div>
        </div>
        <br/>
    </form>

    <form action="/searchInExcelFile/searchInFilesearchAndDownloadFile" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="FileName" value="{{$FileName}}">
        <input type="hidden" name="textarea" value="{{$textarea}}">
        @if(empty($arrayPagesNames))
        @else
            @foreach($arrayPagesNames as $value)
                @if (in_array("checkbox".strval($loop->index), $arrayPagesNamesChecked))
                    <input type="hidden" name="{{ "checkbox".strval($loop->index) }}">
                @else
                @endif
            @endforeach
        @endif
    </form>

    <div class="container-fluid">
        {{--<div class="row align-self-center">--}}
        <div id="gridbox" style="width: 100%; height: 65vh; background-color:white;"></div>
        {{--</div>--}}
    </div>

    {{--<script type="text/javascript">--}}

        {{--$.ajaxSetup({--}}
            {{--headers: {--}}
                {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
            {{--}--}}
        {{--});--}}

    {{--</script>--}}

@endsection