@extends('layouts.mainBlade')

@section('content2')


    <style>

        .card{
            border: 0px;
            background: inherit;
            /*background-color: rgba(10,50,50,0.4);*/
            color: white;
        }

        .custom-file-label{
            /*border: 0px;*/
            background-color: rgba(10,50,50,0.5);
            color: white;
        }

        .custom-file{

            /*border: 0px;*/
            background-color: rgba(10,50,50,0.5);
            color: white;

            /*position: relative;*/
            /*display: inline-block;*/
            /*max-width: 100%;*/
            /*height: 2.5rem;*/
            /*cursor: pointer;*/
        }

        .btn{
            /*border: 0px;*/
            background-color: rgba(10,50,50,0.5);
            color: white;
        }

    </style>


    {{--Шаблон перехода--}}
    <?php
        if(count($_GET)>0 and isset($_GET['function'])){
            if($_GET['function'] == "searchInExcel"){
                $path = "/searchInExcelFile/searchInFileGetFile";
            }elseif ($_GET['function'] == "copyInExcel"){
                $path = "/searchInExcelFile/copyInExcel";
            }
        }
    ?>

    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="container">
                        <br/><br/><br/><br/><br/><br/><br/>
                        <div class="col-md-4 offset-md-4">
                            <form id="slideForm" enctype="multipart/form-data" action="{{$path}}" method="post">
                                <div class="form-group">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="mt-5 ml-5">
                                        <label class="custom-file">
                                            <input type="file" name="userfile" id="file" class="custom-file-input" onchange="$(this).next().after().text($(this).val().split('\\').slice(-1)[0])" required>
                                            <span style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" class="custom-file-control"></span>
                                        </label>
                                        <input class="form-control" type="submit" value="Отправить файл" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
