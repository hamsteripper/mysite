<?php

namespace App\Modules\TelegramBot\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\TelegramBot\Models\Setting;
use Illuminate\Http\Request;
use App\Modules\TelegramBot\Models\Response;

use App\Modules\TelegramBot\Models\DB\TgUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BindTelegramToAccount_ extends Controller
{

    public function __construct()
    {

    }

    public function index(){
        return view('TelegramBot::bindTelegramToAccountIndex_');
    }

    public function getDateFromBD($telegram_username){



        $arr = TgUsers::where('telegram_username', $telegram_username)->get()->toArray();

        if(count($arr) != 0 && $arr[0]['telegram_username'] == $telegram_username){
            return $arr[0]['telegram_user_id'];
        }else{
            return "notFound";
        }

    }

    public function sendCode(Request $request){

        $telegramUsername = $request->get('telegramUsername');
        // TODO
        // Если NULL
        if($telegramUsername == null){
            return view('TelegramBot::bindTelegramToAccountIndex_');
        }

        $result = $this->getDateFromBD($telegramUsername);
        if($result == "notFound"){
            // TODO
            dump("notFound");
        }else{
            // Символы, которые будут использоваться в пароле.
            $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
            // Количество символов в пароле.
            $max=5;
            // Определяем количество символов в $chars
            $size=StrLen($chars)-1;
            // Определяем пустую переменную, в которую и будем записывать символы.
            $password=null;
            // Создаём пароль.
            while($max--)
                $password.=$chars[rand(0,$size)];
                // Выводим созданный пароль.
            Response::send(["chat_id"=>$result, "text"=>$password]);
            Session::put("BindTelegramToAccount_Username", $telegramUsername);
            Session::put("BindTelegramToAccount_Id", $result);
            Session::put("BindTelegramToAccount_Password", $password);

            return view('TelegramBot::bindTelegramToAccountConfirmation');
        }
    }

    public function confirmCode(Request $request){
        $telegramPassword = $request->get('telegramPassword');
        // TODO
        // Если NULL
        if($telegramPassword == null){
            return view('TelegramBot::bindTelegramToAccountConfirmation');
        }

        $password = Session::get("BindTelegramToAccount_Password");
        $TelegramUserId = Session::get("BindTelegramToAccount_Id");
        if($telegramPassword == $password){
            $record = array();
            $record['user_id'] = Auth::user()->id;
            try{
                TgUsers::where('telegram_user_id', $TelegramUserId)->update($record);
                Response::send(["chat_id"=>$TelegramUserId, "text"=>"Accepted"]);
            }catch(\Exception $e){
                Log::error($e);
                // В случе ошибки возвращаем сообщение об ошибке
                Log::error("Error add new user id DB", $e);
            }
            return view('/home');
        }else{
            return view('TelegramBot::bindTelegramToAccountConfirmation');
        }


    }

}
?>