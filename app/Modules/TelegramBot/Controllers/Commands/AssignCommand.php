<?php

namespace App\Modules\TelegramBot\Controllers\Commands;

//use App\Modules\TelegramBot\Models\TgMainConvertHandler;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;


use Telegram;

use App\Modules\TelegramBot\Models\TgMainAssignHandler;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
/**
 * Class HelpCommand.
 */
class AssignCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'assign';

    /**
     * @var array Command Aliases
     */
//    protected $aliases = ['listcommands'];

    /**
     * @var string Command Description
     */
    protected $description = 'Assign Bid';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $updates = Telegram::getWebhookUpdates()->toarray();
        $message = $updates['message'];


        // Проверка на групповой чат
        if($message['chat']['id'] >= 0){
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"This command is not processed in the group chat."]);
            return;
        }

        Log::debug('AssignCommand');

        $mainAssign = new TgMainAssignHandler();
        $mainAssign->create($message, "/assign");


    }
}
