<?php

namespace App\Modules\TelegramBot\Controllers\Commands;

//use App\Modules\TelegramBot\Models\TgMainConvertHandler;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;


use Telegram;

use App\Modules\TelegramBot\Models\TgMainBidHandler;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
/**
 * Class HelpCommand.
 */
class BidCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'addbid';

    /**
     * @var array Command Aliases
     */
//    protected $aliases = ['listcommands'];

    /**
     * @var string Command Description
     */
    protected $description = 'Add bid';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $updates = Telegram::getWebhookUpdates()->toarray();
        $message = $updates['message'];


        // Проверка на групповой чат
        if($message['chat']['id'] >= 0){
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"This command is not processed in the group chat."]);
            return;
        }

        Log::debug('BidCommand');

        $mainBid = new TgMainBidHandler();
        $mainBid->main($message, "/addbid");




    }
}
