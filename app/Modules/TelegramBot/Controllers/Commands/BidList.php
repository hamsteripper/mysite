<?php

namespace App\Modules\TelegramBot\Controllers\Commands;

//use App\Modules\TelegramBot\Models\TgMainConvertHandler;
use App\Modules\TelegramBot\Models\TgMainBidListHandler;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;


use Telegram;

use App\Modules\TelegramBot\Models\TgMainAssignHandler;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
/**
 * Class HelpCommand.
 */
class BidList extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'bl';

    /**
     * @var array Command Aliases
     */
//    protected $aliases = ['listcommands'];

    /**
     * @var string Command Description
     */
    protected $description = 'Bid List';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $updates = Telegram::getWebhookUpdates()->toarray();
        $message = $updates['message'];


        // Проверка на групповой чат
        if($message['chat']['id'] >= 0){
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"This command is not processed in the group chat."]);
            return;
        }

        Log::debug('blCommand');

        $bl = new TgMainBidListHandler();
        //
        $pieces = explode(" ", $message["text"]);
        if(count($pieces) == 1){
            $bl->showAll($message);
        }elseif(count($pieces) == 2){
            $bl->showInDetail($message, $pieces[1]);
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"Неверные параметры команды, должно быть \"/bl\" для просмотра списка заявок или \"/bl номер заявки\" для просмотра определённой заявки"]);
        }

//        $mainAssign = new TgMainAssignHandler();
//        $mainAssign->create($message, "/bl");


    }
}
