<?php

namespace App\Modules\TelegramBot\Controllers\Commands;

use App\Modules\TelegramBot\Models\TgMainConvertHandler;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;


use Telegram;

use App\Modules\TelegramBot\Models\TgMainConvertHandle;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
/**
 * Class HelpCommand.
 */
class ConvertCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'convert';

    /**
     * @var array Command Aliases
     */
//    protected $aliases = ['listcommands'];

    /**
     * @var string Command Description
     */
    protected $description = 'Convert Files';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $updates = Telegram::getWebhookUpdates()->toarray();
        $message = $updates['message'];


        // Проверка на групповой чат
        if($message['chat']['id'] >= 0){
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"This command is not processed in the group chat."]);
            return;
        }

        Log::debug('ConvertCommand');

        $mainConv = new TgMainConvertHandler();
        $mainConv->main($message, "/convert");


    }
}
