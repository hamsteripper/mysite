<?php

namespace App\Modules\TelegramBot\Controllers\Commands;

//use App\Modules\TelegramBot\Models\TgMainConvertHandler;
use App\Modules\TelegramBot\Models\TgMainBidListHandler;
use App\Modules\TelegramBot\Models\TgMainReminderHandler;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;


use Telegram;

use App\Modules\TelegramBot\Models\TgMainAssignHandler;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;

/**
 * Class HelpCommand.
 */
class ReminderCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'remind';

    /**
     * @var array Command Aliases
     */
//    protected $aliases = ['listcommands'];

    /**
     * @var string Command Description
     */
    protected $description = 'Reminder';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $updates = \Telegram::getWebhookUpdates()->toarray();
        $message = $updates['message'];


        // Проверка на групповой чат
        if($message['chat']['id'] >= 0){
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"This command is not processed in the group chat."]);
            return;
        }

        $n = new TgMainReminderHandler();
        $pieces = explode(" ", $message["text"]);
        if(count($pieces) == 3 && $pieces[1] == "del" && is_numeric($pieces[2]) ){
            $n->delete($message, $pieces[2]);
        }elseif(count($pieces) == 2 && $pieces[1] == "list"){
            $n->list($message);
        }elseif(count($pieces) == 1){
            $n->create($message);
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"Неверные параметры команды, должно быть
             \"/remind\" для создания новой напоминалки
             или \"/remind list\" для просмотра списка активных напоминалок
             или \"/remind del №\" для завершения напоминалки под выбранным номером"]);
        }

//        $n = new TgMainReminderHandler();
//        $n->create($message);

        /*
        Log::debug('blCommand');

        $bl = new TgMainBidListHandler();
        //
        $pieces = explode(" ", $message["text"]);
        if(count($pieces) == 1){
            $bl->showAll($message);
        }elseif(count($pieces) == 2){
            $bl->showInDetail($message, $pieces[1]);
        }else{
            Response::send(["chat_id"=>$message['chat']['id'], "text"=>"Неверные параметры команды, должно быть \"/bl\" для просмотра списка заявок или \"/bl номер заявки\" для просмотра определённой заявки"]);
        }
        */


    }
}
