<?php

namespace App\Modules\TelegramBot\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\TelegramBot\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('Access_Level_1');
        return view('TelegramBot::setting', Setting::getSettings());
    }


    public function store(Request $request){


        Setting::where('key', '!=', NULL)->delete();
        foreach ($request->except('_token') as $key => $value){
                $setting = new Setting();
                $setting->key = $key;
                $setting->value = $request->$key;
                $setting->save();
        }
        return redirect()->route('setting.index');
    }

        public function setWebhook(Request $request){
        $result = $this->sendTelegramData('setWebhook', [
            'query' => ['url' => $request->url . '/' . \Telegram::getAccessToken() . '?XDEBUG_SESSION_START=PHPSTORM']
        ]);
        return redirect()->route('setting.index')->with('status', $result);
    }

    public function sendTelegramData ($route = '', $params = [], $method = 'POST'){
        //?XDEBUG_SESSION_START=PHPSTORM
//        $params = ['query' => [
//            'XDEBUG_SESSION_START' => 'PHPSTORM',
//        ]];
        //https://api.telegram.org/bot730373847:AAEA23YGS_7VO1sqAPn1oE_G3B56waNDJTc/setWebhook?url=%2F730373847%3AAAEA23YGS_7VO1sqAPn1oE_G3B56waNDJTc
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.telegram.org/bot' . \Telegram::getAccessToken() .'/']);
        $result = $client->request($method, $route, $params);
        return (string)$result->getBody();

    }

    public function getWebhookInfo(Request $request){
        $result = $this->sendTelegramData('getWebhookInfo');
        return redirect()->route('setting.index')->with('status', $result);
    }

}
