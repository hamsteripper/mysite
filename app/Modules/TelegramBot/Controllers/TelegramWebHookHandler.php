<?php

namespace App\Modules\TelegramBot\Controllers;

use App\Modules\TelegramBot\Models\TgMainReminderHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Log;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use App\Modules\TelegramBot\Models\TgMainConvertHandler;
use App\Modules\TelegramBot\Models\TgMainBidHandler;
use App\Modules\TelegramBot\Models\TgMainAssignHandler;

use App\Modules\TelegramBot\Models\DB\TgUsers;


class TelegramWebHookHandler extends Controller {

    //Добавление пользователя в базу данных
    private function telegramUsers($updates){

        Log::debug($updates);
        $telegram_username = "";
        if(array_key_exists('username',$updates['from'] )) {
            $telegram_username = $updates['from']['username'];
        }

        $telegram_user_id = $updates['from']['id'];
        $is_bot = $updates['from']['is_bot'];

        $arr = TgUsers::where('telegram_user_id', $telegram_user_id)->get()->toArray();

        Log::debug(json_encode($arr));
        Log::debug($telegram_username);
        Log::debug($telegram_user_id);

        if(count($arr) == 0){

        }elseif(count($arr) == 0 && $telegram_username != "" && $is_bot == false){
            $record = array();
            $record['telegram_user_id'] = $telegram_user_id;
            $record['telegram_username'] = $telegram_username;
            try{
                TgUsers::insert($record);
            }catch(\Exception $e){
                Log::error($e);
                // В случе ошибки возвращаем сообщение об ошибке
                Log::error("Error add new user id DB", $e);
            }
        }elseif($arr['0']['telegram_user_id'] == $telegram_user_id && $arr['0']['telegram_username'] != $telegram_username && $is_bot == false){
            $record = array();
            $record['telegram_username'] = $telegram_username;
            try{
                TgUsers::where('telegram_user_id', $telegram_user_id)->update($record);
            }catch(\Exception $e){
                Log::error($e);
                // В случе ошибки возвращаем сообщение об ошибке
                Log::error("Error add new user id DB", $e);
            }
        }

    }


    public function webhook(){
        // Получение сообщения
        $updates = Telegram::getWebhookUpdates()->toarray();
        Log::debug($updates);
        // Проверка на существование ключа message или callback_query
        // Если он ест, то продолжаем обработку
        // Иначе прерывание
        if(array_key_exists('message', $updates) ) {
            Log::debug('TelegramWebHookHandler::message is set');
            $updates = $updates['message'];
            // Обработка сообщения
            $this->messageUpdate($updates);
        }elseif(array_key_exists('callback_query', $updates)){
            Log::debug('callback');
            $updates = $updates['callback_query'];
            if(array_key_exists('message', $updates)){
                Log::debug('TelegramWebHookHandler::message is set');
//                $update = [];
                $update = $updates['message'];

                // Если существует параметр дата
                if(array_key_exists("data", $updates)){
                    $update["data"] = $updates['data'];
                }

                if($update['chat']['type'] == "private"){
                    $update['from']['id'] = $update['chat']['id'];
                }

                Log::debug('callback updates');
                Log::debug($update);
                $this->messageUpdate($update);
            }

        }else{
            Log::debug('message or callback_query not set');
            return;
        }

        //Обработчик комманд
        Telegram::commandsHandler(true);

    }

    private function messageUpdate($updates){

//        $updates = $updates["message"];

        // Добавление пользователя
        $this->telegramUsers($updates);

        Log::debug('TelegramWebHookHandler');
//        Log::debug('TelegramWebHookHandler'.isset($updates['callback_query']));
        $result = TgWhatCommandIsSet::whatCommandIsSet($updates['from']['id']);

        Log::debug($updates);

        // Определение типа сообщения
        if(isset($updates['entities'])){
            // Если это комманда, то пропускаем, дальше идёт обработчик комманд
            Log::debug('TelegramWebHookHandler::command');
        } elseif(isset($updates['text'])){
            if($result == "/convert"){
                Log::debug("TelegramWebHookHandler::Text::/convert");
                $ch = new TgMainConvertHandler();
                $ch->main($updates,"text");
            }elseif ($result == "/addbid"){
                Log::debug("TelegramWebHookHandler::Text::/addBid");
                $ch = new TgMainBidHandler();
                $ch->main($updates,"text");
            }elseif ($result == "/assign"){
                Log::debug("TelegramWebHookHandler::Text::/assign");
                $ch = new TgMainAssignHandler();
                $ch->main($updates,"text");
            }elseif ($result == "/remind"){
                Log::debug("TelegramWebHookHandler::Text::/remind");
                $ch = new TgMainReminderHandler();
                $ch->main($updates,"text");
            }
        }elseif(isset($updates['document'])){
            Log::debug('TelegramWebHookHandler::document');
            $result = TgWhatCommandIsSet::whatCommandIsSet($updates['from']['id']);
            if($result == "/convert"){
                Log::debug("TelegramWebHookHandler::Document::/convert");
                $ch = new TgMainConvertHandler();
                $ch->main($updates,"document");
            }elseif($result == "/addbid"){
                Log::debug("TelegramWebHookHandler::Document::/addbid");
                $bid = new TgMainBidHandler();
                $bid->main($updates,"document");
            }
        }elseif(isset($updates['photo'])){
            Log::debug('TelegramWebHookHandler::photo');
        }elseif(isset($updates['audio'])){
            Log::debug('TelegramWebHookHandler::audio');
            $result = TgWhatCommandIsSet::whatCommandIsSet($updates['from']['id']);
            if($result == "/convert"){
                Log::debug("TelegramWebHookHandler::Audio::/convert");
                $ch = new TgMainConvertHandler();
                $ch->main($updates,"audio");
            }elseif($result == "/addbid"){
                Log::debug("TelegramWebHookHandler::Audio::/addbid");
                $bid = new TgMainBidHandler();
                $bid->main($updates,"audio");
            }
        }elseif(isset($updates['location'])){
            Log::debug('TelegramWebHookHandler::location');
        }elseif(isset($updates['sticker'])) {
            Log::debug('TelegramWebHookHandler::sticker');
        }

        return;
    }

    private function callbackUpdate(){

    }

}
