<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTgUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tg_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('telegram_user_id')->nullable(false)->unique();
            $table->string('telegram_username')->nullable(true)->unique();
            $table->bigInteger('user_id')->unsigned()->nullable(true)->unique();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

            $table->unique(['telegram_user_id','telegram_username','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tg_users');
    }
}


