<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTgCommandsStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tg_commands_states', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->bigInteger('chat_id')->nullable(true);
            $table->bigInteger('from_id')->nullable(false);
            $table->string('command')->nullable(false);
            $table->string('status')->nullable(true);
            $table->string('comment')->nullable(true);
            $table->longText('parameters')->nullable(true);
            $table->timestamps();

            $table->foreign('from_id')->references('telegram_user_id')->on('tg_users');
//            $table->foreign('manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tg_commands_states');
    }
}
