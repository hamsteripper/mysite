<?php


namespace App\Modules\TelegramBot\Models;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use Telegram;

use Telegram\Bot\Api;

use Illuminate\Support\Facades\Log;

class Response {

    public static function send(array $arr){

        if (array_key_exists("text", $arr) && array_key_exists("reply_markup", $arr)){
            self::sendMessageKeyboard($arr);
        }elseif(array_key_exists("text", $arr)){
            self::sendMessage($arr);
        }

    }

    private static function sendMessageKeyboard(array $arr){
        Log::error(\Telegram::getAccessToken());
        $telegram = new Api(\Telegram::getAccessToken());
        $response = $telegram->sendMessage([
            'chat_id' => $arr["chat_id"],
            'text' => $arr["text"],
            'reply_markup' => $arr["reply_markup"]
        ]);
    }

    private static function sendMessage(array $arr){

        Log::error(\Telegram::getAccessToken());
        $telegram = new Api(\Telegram::getAccessToken());
        $response = $telegram->sendMessage([
            'chat_id' => $arr["chat_id"],
            'text' => $arr["text"]
        ]);

        return $response->getMessageId();

    }

}