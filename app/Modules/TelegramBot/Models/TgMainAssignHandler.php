<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:04
 */

namespace App\Modules\TelegramBot\Models;

use App\Modules\BidsDatabase\Models\DB\Bid;
use App\Modules\TelegramBot\Models\DB\TgUsers;
use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use App\User;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgCommandsState;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use PHPUnit\Util\Json;
use App\Modules\FilesConverter\Models\ConvertFile;
use App\Modules\TelegramBot\Models\TgMainBidHandler;

class TgMainAssignHandler
{

    public function create(array $message, string $type){
        Log::debug("TgMainAssignHandler::groupChat");
        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainAssignHandler::groupChat");
            return "";
        }
        // Чтение текущей команды из БД
        $result = TgWhatCommandIsSet::whatCommandIsSet($message['from']['id']);
        if($result == ""){
            // Create new
            $status = $this->createNew($message);
            /* TODO exception status*/
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ожидается имя исполнителя"]);
        }elseif($result == "/assign"){
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Текущая команда активна"]);
            return "";
        }else{
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Команда $result уже запущена"]);
            return "";
        }
    }

    private function createNew(array $message):array {
        Log::debug("TgMainAssignHandler::create::/assign");
        // Создаём массив параметров для новой комманды
        $record = array();
        $record['from_id'] = $message['from']['id'];
        $record['command'] = "/assign";
        // Генерация номера заявки
        $parameters = array();
        $parameters["comment"] = "";
        $record['parameters'] = json_encode($parameters);
        // Статус команды
        $record["status"] = "waiting_for_username";
        // Попытка записать данные в БД
        try{
            TgCommandsState::insert($record);
        }catch(\Exception $e){
            Log::error($e);
            // В случе ошибки возвращаем сообщение об ошибке
            return ["status"=>"Error create new command"];
        }
        // В случае успеха возвращаем сообщение о необходимых параметрах
        return ["status"=>"ok"];
    }

    public function main(array $message, string $type):array {
        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainAssignHandler::groupChat");
            return ["exception"=>"groupChat"];
        }
        // Вывод логов
        Log::debug("TgMainAssignHandler::message");
        Log::debug($message);
        // Определение типа сообщения
        switch ($type){
            case "text":
                // Анализируем состояние команды в базе
                $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
                if (count($record)) {
                    Log::debug("TgMainAssignHandler::message" . json_encode($record));
                    switch ($record["status"]) {
                        case "waiting_for_username":
                            $result = $this->waiting_for_username($message);
                            if($result["status"] == "success") {
                                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ожидается текст заявки"]);
                            }elseif($result["status"] == "Пользователь не обнаружен"){
                                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Пользователь не обнаружен"]);
                            }else{
                                /* TODO */
                            }
                            break;
                        case "waiting_for_comment":
                        case "waiting_for_nothing":
                            $result = $this->addComment($message);
                            if($result["status"] == "success") {
                                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ожидание завершения или ещё текста"]);
                            }elseif($result["status"] == "error"){
                                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ошибка добавления текста"]);
                            }else{
                                /* TODO */
                            }
                            break;
                        default:
                            break;
                    }
                } else {
                    Log::debug("TgMainAssignHandler::message::error");
                    return ["error" => "Error command"];
                }
                break;
            default:
                break;
        }
        return ["status"=>"success"];
    }

    // Если статус ожидания имени пользователя
    private function waiting_for_username(array $message){
        $tgUserRecord = TgUsers::where('telegram_username', $message["text"])->get()->toArray();
        if($tgUserRecord != null && $tgUserRecord[0]["user_id"] != null){
            $result = $this->addTgUser($message["from"]["id"], $message["text"]);
            if($result["status"] == "error"){
                Log::debug("TgMainAssignHandler::tgUserRecord::Ошибка обновления команды" );
                return ["status"=>"Ошибка обновления команды"];
            }
        }else{
            Log::debug("TgMainAssignHandler::tgUserRecord::Пользователь не обнаружен" );
            return ["status"=>"Пользователь не обнаружен"];
        }
        return ["status"=>"success"];
    }

    // Добавление пользователя в команду
    private function addTgUser(string $tgUserId, string $tgUserName){
        $record = TgWhatCommandIsSet::getTgCommandsState($tgUserId)[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        $parameters["telegram_username"] = $tgUserName;
//        $parameters["telegram_user_id"] = $tgUserId;
        $record["parameters"] = json_encode($parameters);
        $record["status"] = "waiting_for_comment";

        try{
            TgCommandsState::where('from_id', $tgUserId)->update($record);
        }catch (\Exception $e){
            Log::debug($e);
            return ["status"=>"error"];
        }
        return ["status"=>"success"];
    }

    // Добавляем текст в команду
    private function addComment(array $message){
        Log::debug("TgMainAssignHandler:addComment");

        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        $parameters["comment"] = $parameters["comment"] . $message["text"] . "\n\r" ;
        $record["parameters"] = json_encode($parameters);
        $record["status"] = "waiting_for_nothing";

        try{
            TgCommandsState::where('from_id', $message['from']['id'])->update($record);
        }catch (\Exception $e){
            Log::debug($e);
            return ["status"=>"error"];
        }

        return ["status"=>"success"];
    }

    // Подтверждение
    public function accept($message){
        Log::debug("TgMainAssignHandler::accept");
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        Log::debug("TgMainAssignHandler::accept::".json_encode($record));

        switch($record["status"]){
            case "waiting_for_username":
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ожидается имя исполнителя"]);
                break;
            case "waiting_for_comment":
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ожидается текст заявки"]);
                break;
            case "waiting_for_nothing":
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ожидается..."]);
                $tgUserEngineer = TgUsers::where("telegram_username", $parameters["telegram_username"])->get()[0]->toArray();
                $engineerId = User::where("id", $tgUserEngineer["user_id"])->get()[0]->toArray();

                Log::debug("TgMainAssignHandler::accept::".json_encode($engineerId));

                $tgUserManager = TgUsers::where("telegram_user_id", $record["from_id"])->get()[0]->toArray();
                $managerId = User::where("id", $tgUserManager["user_id"])->get()[0]->toArray();

                Log::debug("TgMainAssignHandler::accept::".json_encode($managerId));

                $b = new Bid;
                $b->engineer_id = $engineerId["id"];
                $b->manager_id = $managerId["id"];
                $b->files = "";
                $b->data = $parameters["comment"];
                $b->comment = "";
                $b->status = "assigned";
                $b->date = date("Ymd");

                try{
                    $b->save();
                    TgWhatCommandIsSet::deleteTgCommandsState($message["from"]["id"]);
                    Response::send(["chat_id"=>$message['from']['id'], "text"=>"Готово"]);
                    Response::send(["chat_id"=>$tgUserEngineer["telegram_user_id"], "text"=>"Новая заявка"]);
                }catch(\Exception $e){
                    Log::error($e);
                    // В случе ошибки возвращаем сообщение об ошибке
                    return ["status"=>"Error create new command"];
                }
                break;
        }

        return ["status"=>"ok"];

    }

    // Проверка на групповой чат
    private function chatIsGroup(array $message){
        if($message['chat']['id'] >= 0){
            return false;
        }else{
            return true;
        }
    }
    //  Удаление текущей команды
    public function clear(array $message):bool{
        TgWhatCommandIsSet::deleteTgCommandsState($message["from"]["id"]);
        return true;
    }
}