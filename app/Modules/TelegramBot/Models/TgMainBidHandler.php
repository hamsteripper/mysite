<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:04
 */

namespace App\Modules\TelegramBot\Models;


//use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use Illuminate\Support\Facades\Log;
//use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgCommandsState;
//use App\Modules\FilesConverter\Models\SupportedMimeTypes;
//use PhpParser\Node\Expr\Array_;
use function Psy\debug;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
//use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
//use Illuminate\Support\Carbon;
//use PHPUnit\Util\Json;
//use App\Modules\FilesConverter\Models\ConvertFile;

use App\Modules\BidsDatabase\Models\AddCurrentBidsToDatabase;

class TgMainBidHandler
{

    public function main(array $message, string $type):string {

        Log::debug($message);

        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainBidHandler::groupChat");
            return "";
        }

        // Чтение текущей команды из БД
        $result = TgWhatCommandIsSet::whatCommandIsSet($message['from']['id']);
        // Анализ результата
        if($result == "" && $type == "/addbid"){
            // Если в БД ничего нет, и пришла комманда конвертации
            // То, создаём новую команду и записываем её в бд
            $result = self::createNewCommand($message);
            if(array_key_exists("error", $result)){
                Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                return"";
            }else{
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Добавление новой заявки\r\nОжидание документов, фотографий или комментариев"]);
            }
        }elseif($result == "/addbid"){
            // Команда найдена
            Log::debug("TgMainBidHandler::/addbid");
//
            switch ($type){
//                case "/ok":
//                    Log::debug("TgMainBidHandler:Type:/addbid /ok");
//                    break;
                case "/addbid":
//                    // Данная команда уже активированна
//                    // Отправляем комментарий текущей команды
                    Log::debug("TgMainBidHandler:Type:/addbid");
                    break;
                case "document":
//                    // Пришёл документ
//                    // Проверяем mimeType, в случае успеха загружаем
//                    // И записываем его в бд
                    Log::debug("TgMainBidHandler:Document:/addbid");
                    $result = self::uploadFile($message, $type);
//                    // Отправка ошибки
                    if(array_key_exists("error", $result)){
                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                        return"";
                    }
                    break;
                case "text":
//                    // Пришёл текст
//                    // Проверяем и записываем в БД
                    Log::debug("TgMainBidHandler:Text:/addbid");
                    $result = self::addComment($message);
                    // Отправка ошибки
                    if(array_key_exists("error", $result)){
                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                        return "";
                    }
                    break;
                case "audio":
//                    // Пришло аудио
//                    // Проверяем и записываем в БД
                    Log::debug("TgMainBidHandler:Audio:/addbid");
//                    $result = self::uploadFile($message, $type);
//                    // Отправка ошибки
//                    if(array_key_exists("error", $result)){
//                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
//                        return"";
//                    }
                    break;
                case "photo":
//                    // Пришло аудио
//                    // Проверяем и записываем в БД
                    Log::debug("TgMainBidHandler:Photo:/addbid");
//                    $result = self::uploadFile($message, $type);
//                    // Отправка ошибки
//                    if(array_key_exists("error", $result)){
//                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
//                        return"";
//                    }
                    break;
//
            }
//
//            // Проверка статуса готовности
//            $result = self::checkStatusReady($message);
//            if($result){
//                // Конвертация файла
//                $result = self::convertFile($message);
//                if(array_key_exists("error", $result)){
//                    Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
//                    return "";
//                }
//
////                Log::debug("afafasf".$message);
//                Log::debug("xxxxxxxxxxxxx");
//                Log::debug($message);
//                Log::debug($type);
//                // Создание url
//                $url = self::createUrl($message);
//                // Отправка финального ответа
//                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Final file:"]);
//                Response::send(["chat_id"=>$message['from']['id'], "text"=>$url]);
//                // Удаление ненужных данных из папок и базы данных
//                self::clear($message);
//
//            }else{
//                return "";
//            }

        }else{
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Command $result is active"]);
        }

        return "";
    }

    public function accept($message){
        Log::debug("TgMainBidHandler:accept");
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();

        Log::debug(json_encode($record));

            $array = json_decode($record["parameters"], true)["file_paths"];
            $comment = json_decode($record["parameters"], true)["comment"];
            $id = $record["from_id"];

            try{
                $addBidsToDB = new AddCurrentBidsToDatabase();
                $addBidsToDB->add($array, $comment, $id);
            }catch (Exceprion $e){
                Log::debug("accept-false");
            }

    }

    function __autoload($class)
    {
        include("App\Modules\BidsDatabase\Model\AddCurrentBidsToDatabase" . '.php');

        // Проверяем необходимость подключения указанного класса
        if (!class_exists("App\Modules\BidsDatabase\Model\AddCurrentBidsToDatabase", false)) {
            trigger_error("Не удалось загрузить класс: \"App\Modules\BidsDatabase\Model\AddCurrentBidsToDatabase\"", E_USER_WARNING);
        }
    }

    // Удаление ненужных данных из папок и базы данных
    // Fix: exception
    public function clear(array $message):bool{
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        $start_file_Path = $parameters["file_paths"];
        foreach($start_file_Path as $file_Path){
            Log::debug($file_Path);
            File::delete($file_Path);
        }
        TgWhatCommandIsSet::deleteTgCommandsState($message["from"]["id"]);
        return true;
    }

    // Установка финального расширения
    private function addComment(array $message){
        Log::debug("TgMainBidHandler:addComment");
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        $parameters["comment"] = $parameters["comment"] . $message["text"] . "\n\r" ;

        $record["parameters"] = json_encode($parameters);
        try{
            TgCommandsState::where('from_id', $message['from']['id'])->update($record);
        }catch (\Exception $e){
            Log::debug($e);
            return ["error"=>"setFinalExtension::update::error"];
        }

        return ["success"=>""];

    }

    // Загрузка файла
    private function uploadFile(array $message, $type):array {
        Log::debug("TgMainConvertHandler:UploadFile");
        // Данные для БД
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        //Устанавливаем путь для сохранения файлов
        $savePath = storage_path()."/app/public/TG/".$message["from"]["id"]."/";
        // 1) Получение mime_type
        $parameters['mime_type'] = $message[$type]['mime_type'];
        // 2) Получение type
        $parameters['type'] = self::getType($message[$type]['mime_type']);
        $message["my_type"] = self::getType($message[$type]['mime_type']);

        // Создание директорий и поддиректорий
        if(!File::exists($savePath)){
            File::makeDirectory($savePath, 0775, true, true);
        }

        // Создание загрузчика
        $tgUF = new TgUploadFiles();
        // Положить сообщение
        $tgUF->setMesage($message, $type);
        // Положить путь к выходному файлу
        $tgUF->setSavePath($savePath);

        // Скачивание
        $result = $tgUF->uploadFile();

        $start_file_Path = $result["start_file_Path"];
        $start_file_name = $result["start_file_name"];

        $array = [$result["start_file_Path"]];

        Log::debug("uploadFile = " . json_encode($array));

        // Параметры для записи в БД
        $record['from_id'] = $message['from']['id'];
        $parameters["file_paths"] = array_merge($parameters["file_paths"], $array);
        $record["parameters"] = json_encode($parameters);
        Log::debug("uploadFile = " . json_encode($parameters));
        try{
            TgCommandsState::where('from_id', $message['from']['id'])->update($record);
        }catch (\Exception $e){
            Log::debug($e);
            return ["error"=>"uploadFile::update::error"];
        }

        return ["success"=>""];
    }
    // Получение типа файла
    private static function getType(string $mime_type):string{

        // Исколючение
        if($mime_type == 'audio/flac'){
            return 'flac';
        }

        $mm = new MimeTypeExtensionGuesser();
        $result = $mm->guess($mime_type);

        if($result != ""){
            return $result;
        }else{
            return "error";
        }
    }

    // Проверка на групповой чат
    private function chatIsGroup(array $message){
        if($message['chat']['id'] >= 0){
            return false;
        }else{
            return true;
        }
    }

    private function createNewCommand(array $message):array {
        Log::debug("TgMainBidHandler::createNewCommand::/addbid");

        // Создаём массив параметров для новой комманды
        $record = array();
        $record['from_id'] = $message['from']['id'];
        $record['command'] = "/addbid";

        // Вычлинение номера заявки
        $pieces = explode(" ", $message["text"]);
        if(count($pieces) <= 1){
            return ["error"=>"Warrning :: Вторым параметром должен быть номер заявки"];
        }else{
            unset($pieces[0]);
            $pieces = implode(" ", $pieces);
        }

        Log::debug($pieces);

        $parameters = array();
        // Добавление номер заявки
        $parameters["bidNumber"] = $pieces;
        $parameters["file_paths"] = array();
        $parameters["comment"] = "";

        $record['parameters'] = json_encode($parameters);
        // Попытка записать данные в БД
        try{
            TgCommandsState::insert($record);
        }catch(\Exception $e){
            Log::error($e);
            // В случе ошибки возвращаем сообщение об ошибке
            return ["error"=>"Error create new command"];
        }
        // В случае успеха возвращаем сообщение о необходимых параметрах
        return ["success"=>""];
    }
}