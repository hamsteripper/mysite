<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:04
 */

namespace App\Modules\TelegramBot\Models;

use App\Modules\BidsDatabase\Models\DB\Bid;
use App\Modules\TelegramBot\Models\DB\TgUsers;
use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use App\User;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgCommandsState;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use PHPUnit\Util\Json;
use App\Modules\FilesConverter\Models\ConvertFile;
use App\Modules\TelegramBot\Models\TgMainBidHandler;

class TgMainBidListHandler
{

    public function showAll(array $message){
        Log::debug("TgMainBidListHandler::showAll");

        $tgUsers = TgUsers::where("telegram_user_id", $message['from']['id'])->get()[0]->toArray();
        $bids = Bid::where("engineer_id", $tgUsers["user_id"])->get()->toArray();

        $l = "";
        foreach ($bids as $key=>$value){
            $l .= "/bl ".$value["id"] . "\r\n";
        }

        Log::debug("TgMainBidListHandler::showAll".json_encode($bids));
        Response::send(["chat_id"=>$message['chat']['id'], "text"=>"$l"]);

    }

    public function showInDetail(array $message, $bidId){
        Log::debug("TgMainBidListHandler::showInDetail ".$bidId);
        $tgUsers = TgUsers::where("telegram_user_id", $message['from']['id'])->get()[0]->toArray();
        $bids = Bid::where("engineer_id", $tgUsers["user_id"])->where("id", $bidId)->get()[0]->toArray();
        $response = "Текст заявки: \r\n";
        $response .= $bids['data']."\r\n";
        $response .= "Статус: \r\n";
        switch ($bids['status']){
            case 'assigned':
                $response .= $bids['status'];
                break;
        }
        Response::send(["chat_id"=>$message['chat']['id'], "text"=>$response]);

    }

    // Проверка на групповой чат
    private function chatIsGroup(array $message){
        if($message['chat']['id'] >= 0){
            return false;
        }else{
            return true;
        }
    }

}