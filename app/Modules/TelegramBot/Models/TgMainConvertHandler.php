<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:04
 */

namespace App\Modules\TelegramBot\Models;


use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgCommandsState;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use PHPUnit\Util\Json;
use App\Modules\FilesConverter\Models\ConvertFile;

class TgMainConvertHandler
{

    public function main(array $message, string $type):string {

        Log::debug($message);

        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainConvertHandler::groupChat");
            return "";
        }

        // Чтение текущей команды из БД
        $result = TgWhatCommandIsSet::whatCommandIsSet($message['from']['id']);
        // Анализ результата
        if($result == "" && $type == "/convert"){
            // Если в БД ничего нет, и пришла комманда конвертации
            // То, создаём новую команду и записываем её в бд
            $result = self::createNewCommand($message);
            if(array_key_exists("error", $result)){
                Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                return"";
            }else{
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"File or finish extension expecting..."]);
            }
        }elseif($result == "/convert"){
            // Команда найдена
            Log::debug("TgMainConvertHandler::/convert");

            switch ($type){
                case "/convert":
                    // Данная команда уже активированна
                    // Отправляем комментарий текущей команды
                    Log::debug("TgMainConvertHandler:Type:/convert");
                    return "";
                case "document":
                    // Пришёл документ
                    // Проверяем mimeType, в случае успеха загружаем
                    // И записываем его в бд
                    Log::debug("TgMainConvertHandler:Document:/convert");
                    $result = self::uploadFile($message, $type);
                    // Отправка ошибки
                    if(array_key_exists("error", $result)){
                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                        return"";
                    }
                    break;
                case "text":
                    // Пришёл текст
                    // Проверяем и записываем в БД
                    Log::debug("TgMainConvertHandler:Text:/convert");
                    $result = self::setFinalExtension($message);
                    // Отправка ошибки
                    if(array_key_exists("error", $result)){
                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                        return "";
                    }
                    break;
                case "audio":
                    // Пришло аудио
                    // Проверяем и записываем в БД
                    Log::debug("TgMainConvertHandler:Audio:/convert");
                    $result = self::uploadFile($message, $type);
                    // Отправка ошибки
                    if(array_key_exists("error", $result)){
                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                        return"";
                    }
                    break;
                case "photo":
                    // Пришло аудио
                    // Проверяем и записываем в БД
                    Log::debug("TgMainConvertHandler:Photo:/convert");
                    $result = self::uploadFile($message, $type);
                    // Отправка ошибки
                    if(array_key_exists("error", $result)){
                        Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                        return"";
                    }
                    break;

            }

            // Проверка статуса готовности конвертации
            $result = self::checkStatusReady($message);
            if($result){
                // Конвертация файла
                $result = self::convertFile($message);
                if(array_key_exists("error", $result)){
                    Response::send(["chat_id"=>$message['from']['id'], "text"=>$result['error']]);
                    return "";
                }
                // Создание url
                $url = self::createUrl($message);
                // Отправка финального ответа
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Final file:"]);
                Response::send(["chat_id"=>$message['from']['id'], "text"=>$url]);
                // Удаление ненужных данных из папок и базы данных
                self::clear($message);
            }else{
                return "";
            }

        }else{
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Command $result is active"]);
        }

        return "";
    }

    // Удаление ненужных данных из папок и базы данных
    // Fix: exception
    private function clear(array $message):bool{
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);

        $start_file_Path = $parameters["start_file_Path"];
        // Удаление оригинального файла
        File::delete($start_file_Path);
        TgWhatCommandIsSet::deleteTgCommandsState($message["from"]["id"]);

        return true;
    }

    // Создание url ссылки
    private function createUrl(array $message):string {

        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);

        $url = Storage::url("TG/".$message["from"]["id"]."/output/".$parameters["newFileName"]);
        $url = "https://hamsteripper.cloud".$url;

        return $url;

    }

    // Конвертация файла
    private function convertFile(array $message):array {

        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);

        try{
            // Конвертация
            $convert = new ConvertFile();
            $convert->setFilePath($parameters["start_file_Path"]);
            $convert->setStartFinishFileTypes($parameters["type"], $parameters["finish_extension"]);
            $convert->convert();
        }catch (\Exception $e){
            Log::error($e);
            return ["error"=>"convertFile::convert::error"];
        }
        // NULL EXCEPTION FIX
        $parameters["newFileName"] = $convert->getNewName();
        $parameters["newFilePath"] = $convert->getNewFileName();
        $record["parameters"] = json_encode($parameters);

        try{
            TgCommandsState::where('from_id', $message['from']['id'])->update($record);
        }catch (\Exception $e){
            Log::error($e);
            return ["error"=>"convertFile::update::error"];
        }
        return ["success"=>""];

    }


    // Проверка статуса готовности
    private function checkStatusReady(array $message):bool{
        Log::debug("TgMainConvertHandler:checkStatusReady");
        //
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);

        $finish_extension = "";
        $supported_types = "";

        if(array_key_exists("finish_extension", $parameters)){
            $finish_extension = $parameters["finish_extension"];
        }else{
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Finish Extension Waiting..."]);
        }

        if(array_key_exists("supported_types", $parameters)){
            $supported_types = $parameters["supported_types"];
        }else{
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"File Waiting..."]);
        }

        if(array_key_exists("finish_extension", $parameters) && array_key_exists("supported_types", $parameters)){
            if(in_array($finish_extension, $supported_types)){
                return true;
            }else{
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Unsupported file format or finish extension..."]);
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Need:"]);
                Response::send(["chat_id"=>$message['from']['id'], "text"=>json_encode(SupportedMimeTypes::getConvertSupportedTypes())]);
                return false;
            }
        }else{
            return false;
        }

    }

    // Установка финального расширения
    private function setFinalExtension(array $message){
        Log::debug("TgMainConvertHandler:setFinalExtension");
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        $parameters["finish_extension"] = $message["text"];
        $record["parameters"] = json_encode($parameters);
        try{
            TgCommandsState::where('from_id', $message['from']['id'])->update($record);
        }catch (\Exception $e){
            Log::debug($e);
            return ["error"=>"setFinalExtension::update::error"];
        }

        return ["success"=>""];

    }

    // Загрузка файла
    private function uploadFile(array $message, $type):array {
        Log::debug("TgMainConvertHandler:UploadFile");
        // Данные для БД
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        $parameters = json_decode($record["parameters"],true);
        //Устанавливаем путь для сохранения файлов
        $savePath = storage_path()."/app/public/TG/".$message["from"]["id"]."/";
        // 1) Получение mime_type
        $parameters['mime_type'] = $message[$type]['mime_type'];
        // 2) Получение type
        $parameters['type'] = self::getType($message[$type]['mime_type']);
        $message["my_type"] = self::getType($message[$type]['mime_type']);
//        return ["success"=>""];
        // 3) Определение поддерживается ли конвертация данного типа файла
        // Fix, response to user
        $supportedTypes = SupportedMimeTypes::getConvertSupportedTypes();
        if(array_key_exists($parameters['type'], $supportedTypes)){
            // Если файл поддерживается, то записываем в параметры
            $parameters['supported_types'] = $supportedTypes[$parameters['type']];
        }else{
            Log::debug("Mime Type Is Not Supported");
            return ["error"=>"Mime Type Is Not Supported"];
        }
        // Создание директорий и поддиректорий
        if(!File::exists($savePath)){
            File::makeDirectory($savePath, 0775, true, true);
        }

        // Создание загрузчика
        $tgUF = new TgUploadFiles();
        // Положить сообщение
        $tgUF->setMesage($message, $type);
        // Положить путь к выходному файлу
        $tgUF->setSavePath($savePath);

        // Скачивание
        $result = $tgUF->uploadFile();
        // Параметры для записи в БД
        $record['from_id'] = $message['from']['id'];
        $parameters = array_merge($parameters, $result);
        $record["parameters"] = json_encode($parameters);
        try{
            TgCommandsState::where('from_id', $message['from']['id'])->update($record);
        }catch (\Exception $e){
            Log::debug($e);
            return ["error"=>"uploadFile::update::error"];
        }

        return ["success"=>""];
    }
    // Получение типа файла
    private static function getType(string $mime_type):string{

        // Исколючение
        if($mime_type == 'audio/flac'){
            return 'flac';
        }

        $mm = new MimeTypeExtensionGuesser();
        $result = $mm->guess($mime_type);

        if($result != ""){
            return $result;
        }else{
            return "error";
        }
    }

    // Проверка на групповой чат
    private function chatIsGroup(array $message){
        if($message['chat']['id'] >= 0){
            return false;
        }else{
            return true;
        }
    }

    private function createNewCommand(array $message):array {
        Log::debug("TgMainConvertHandler::createNewCommand::/convert");

        // Создаём массив параметров для новой комманды
        $record = array();
        $record['from_id'] = $message['from']['id'];
        $record['command'] = "/convert";
//        $record['status'] = "file_or_extension_waiting";
//        $record['comment'] = "File Or Finish Extension Waiting...";
        $parameters = array();
        $record['parameters'] = json_encode($parameters);
        // Попытка записать данные в БД
        try{
            TgCommandsState::insert($record);
        }catch(\Exception $e){
            Log::error($e);
            // В случе ошибки возвращаем сообщение об ошибке
            return ["error"=>"Error create new command"];
        }
        // В случае успеха возвращаем сообщение о необходимых параметрах
        return ["success"=>""];
    }
}