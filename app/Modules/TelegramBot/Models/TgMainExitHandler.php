<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 24.06.19
 * Time: 0:06
 */

namespace App\Modules\TelegramBot\Models;


use App\Modules\TelegramBot\Models\TgMainAssignHandler;
use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use App\Modules\TelegramBot\Models\TgMainBidHandler;
//use App\Modules\TelegramBot\Models\TgMainAssignHandler;


class TgMainExitHandler{

    public function main(array $message):string {
        Response::send(["chat_id"=>$message['chat']['id'], "text"=>"All commands is drop"]);

        // Чтение текущей команды из БД
        $records = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])->toArray();

        // Если комманд нет
        if(empty($records)){
            return "";
        };

        // Обработка команды пользователя
        switch ($records[0]["command"]){
            case "/convert":
                $this->convertHandler($records[0]);
                break;
            case "/addbid":
                $this->bidHandler($message);
                break;
            case "/assign":
                $this->assignHandler($message);
                break;
            case "/remind":
                $this->remindHandler($message);
                break;
            default:
                break;
        }

        return "";
    }

    public function convertHandler(array $record):string{

        $parameters = json_decode($record["parameters"],true);

        Log::debug("xxxxx111 = " . json_encode($parameters));
        if(array_key_exists( "start_file_Path", $parameters) && File::exists($parameters["start_file_Path"])){
            Log::debug("asfasfanjkabfbkjs");
            File::delete($parameters["start_file_Path"]);
        }

        TgWhatCommandIsSet::deleteTgCommandsState($record["from_id"]);

        return "";
    }

    public function bidHandler(array $message):string{
        $bid = new TgMainBidHandler();
        $bid->clear($message);
        return "";
    }

    public function assignHandler(array $message):string{
        $assign = new TgMainAssignHandler();
        $assign->clear($message);
        return "";
    }

    public function remindHandler(array $message):string{
        $assign = new TgMainReminderHandler();
        $assign->clear($message);
        return "";
    }

}
