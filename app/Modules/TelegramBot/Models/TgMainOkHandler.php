<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:04
 */

namespace App\Modules\TelegramBot\Models;


use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use Illuminate\Support\Facades\Log;
use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgCommandsState;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use PHPUnit\Util\Json;
use App\Modules\FilesConverter\Models\ConvertFile;
use App\Modules\TelegramBot\Models\TgMainBidHandler;

class TgMainOkHandler
{

    public function main(array $message, string $type):string {

        Log::debug($message);

        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainOkHandler::groupChat");
            return "";
        }

        // Чтение текущей команды из БД
        $result = TgWhatCommandIsSet::whatCommandIsSet($message['from']['id']);

        if($result == "/addbid" && $type == "/ok"){
            Log::debug('TgMainOkHandler::addbid');
            $mainBid = new TgMainBidHandler();
            $mainBid->accept($message);

        }elseif ($result == "/assign" && $type == "/ok"){
            Log::debug('TgMainOkHandler::assign');
            $mainAssign = new TgMainAssignHandler();
            $mainAssign->accept($message);
        }

        return "";
    }

    // Проверка на групповой чат
    private function chatIsGroup(array $message){
        if($message['chat']['id'] >= 0){
            return false;
        }else{
            return true;
        }
    }

}