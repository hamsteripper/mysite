<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:04
 */

namespace App\Modules\TelegramBot\Models;

use App\Modules\BidsDatabase\Models\DB\Bid;
use App\Modules\Reminder\Controllers\ReminderController;
use App\Modules\TelegramBot\Models\DB\TgUsers;
use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\TgWhatCommandIsSet;
use App\Modules\Reminder\Models\DB\Reminder;
use App\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Log;
//use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgCommandsState;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use PHPUnit\Util\Json;
use App\Modules\FilesConverter\Models\ConvertFile;
use App\Modules\TelegramBot\Models\TgMainBidHandler;

use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

use Telegram;

use Telegram\Bot\Api;


class TgMainReminderHandler
{

    public function create(array $message){
        Log::debug("TgMainReminderHandler::create");
        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainReminderHandler::groupChat");
            return "";
        }
        // Чтение текущей команды из БД
        $result = TgWhatCommandIsSet::whatCommandIsSet($message['from']['id']);
        if($result == ""){
            $this->addToDatabase($message,"/remind", "", "waiting_for_comment");
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Введите текст напоминания:"]);
        }else{
            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Команда $result уже запущена"]);
            return "";
        }

    }

    private function addToDatabase($message, $command, $comment="", $status, $parameters=[]){
        $record = array();
        $record['from_id'] = $message['from']['id'];
        $record['command'] = $command;
        $record['comment'] = $comment;
        // Генерация номера заявки
        $record['parameters'] = json_encode($parameters);
        // Статус команды
        $record["status"] = $status;
        $record['created_at'] = new DateTime();
        // Попытка записать данные в БД
        try{
            TgCommandsState::insert($record);
        }catch(\Exception $e){
            Log::error($e);
            // В случе ошибки возвращаем сообщение об ошибке
            return ["status"=>"Error create new command"];
        }
        return ["status"=>"ok"];
    }

    private function updateDatabase($message, $status, $comment="", $parameters=null):array {
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
//        $record = array();
        if($comment != "")
            $record['comment'] = $comment;
        // Генерация номера заявки
        if($parameters != null) {
            $param = json_decode($record['parameters'], true);
            Log::debug($param);
            $record['parameters'] = json_encode(array_merge($param, $parameters));
        }
        // Статус команды
        $record["status"] = $status;
        // Попытка записать данные в БД
        try{
            TgCommandsState::where('from_id', '=', $message['from']['id'])->update($record);
        }catch(\Exception $e){
            Log::error($e);
            // В случе ошибки возвращаем сообщение об ошибке
            return ["status"=>"Error create new command"];
        }
        return ["status"=>"ok"];
    }

    public function main(array $message, string $type):array {
        // Проверка на групповой чат
        if(self::chatIsGroup($message)){
            Log::debug("TgMainReminderHandler::groupChat");
            return ["exception"=>"groupChat"];
        }

        // Прокерка
        $record = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        Log::debug("TgMainReminderHandler::main" . json_encode($record));
        if(!count($record)) {
            return ["status"=>"exception"];
        }

        // Если тип это текст
        switch ($type) {
            case "text":
                    $this->textHandler($message, $record);
                break;
            default:
                break;
        }

        return ["status"=>"success"];

    }

    private function textHandler($message, $record){
        // Анализируем состояние команды в базе
        Log::debug("TgMainReminderHandler::text");
        switch ($record["status"]) {
            case "waiting_for_comment":
                Log::debug("TgMainReminderHandler::status::waiting_for_comment");
                $keyboard = $this->keyboardOptions("options");
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Выбор варианта напоминания", "reply_markup" => $keyboard]);
                $this->updateDatabase($message, "waiting_for_option", $message["text"], null);
                break;
            case "waiting_for_option":
                Log::debug("TgMainReminderHandler::status::waiting_for_option");
                if(array_key_exists('data', $message)){
                    switch ($message['data']){
                        case 'remind_at_intervals':
                            $keyboard = $this->keyboardOptions("week");
                            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Введите регулярность повторения", "reply_markup" => $keyboard]);
                            $this->updateDatabase($message, "repeat", "", null);
                            break;
                        case 'remind_once':
                            Response::send(["chat_id"=>$message['from']['id'], "text"=>"Введите дату напоминания (формат 'dd.mm.yy'):"]);
                            $this->updateDatabase($message, "date_expected", "", null);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 'repeat':
                Log::debug("TgMainReminderHandler::status::repeat");
                if(array_key_exists('data', $message)){
                    Log::debug("TgMainReminderHandler::status::repeat::ok");
                    if( (intval($message['data']) >= 0) && (intval($message['data']) <= 8)){
                            Log::debug("TgMainReminderHandler::status::repeat");
                            Log::debug("TgMainReminderHandler::status::".json_encode($message));
                            $this->updateDatabase($message, "time_expected", "", ["repeat" => $message['data']]);
                            Response::send(["chat_id" => $message['from']['id'], "text" => "Введите время напоминания (формат 'hh.mm.ss'):"]);
                    }
                }
                break;
            case "date_expected":
                $s = $this->is_Date($message["text"]);
                if($s){
                    $this->updateDatabase($message, "time_expected", "", ["date" => $message["text"]]);
                    Response::send(["chat_id"=>$message['from']['id'], "text"=>"Введите время напоминания (формат 'hh.mm.ss'):"]);
                }else{
                    Response::send(["chat_id"=>$message['from']['id'], "text"=>"Введите дату напоминания (формат 'dd.mm.yy'):"]);
                }
                break;
            case "time_expected":
                Log::debug("TgMainReminderHandler::status::time_expected");
                $s = $this->is_Time($message["text"]);
                if($s){
                    $this->updateDatabase($message, "time_expected", "", ["time" => $message["text"]]);
                    $this->accept($message);
                    Response::send(["chat_id"=>$message['from']['id'], "text"=>"Принято"]);
                }else{
                    Response::send(["chat_id"=>$message['from']['id'], "text"=>"Введите время напоминания (формат 'hh.mm.ss'):"]);
                }
                break;
            default:
                break;
        }
    }

    private function accept($message){
        $r = TgWhatCommandIsSet::getTgCommandsState($message["from"]["id"])[0]->toArray();
        Log::debug("TgMainReminderHandler::accept");
        Log::debug("TgMainReminderHandler::accept::".json_encode($r));
        $user_id = TgUsers::where('telegram_user_id', $message["from"]["id"])->get()[0]->toArray()["user_id"];
        $record = array();
        $record["user_id"] = $user_id;
        $record["text"] = $r["comment"];
        $param = json_decode($r['parameters'], true);
        if(array_key_exists("repeat", $param)){
            $record["repeat"] = $param["repeat"];
//            $date = date("Y-m-d H:i:s");
//            $record["date_reminder"] = $date;
            $record["time"] = $param["time"];
        }else{
//            $record["repeat"] = "no";
            $date = new DateTime($param["date"]. " " .$param["time"]);
            $record["date_reminder"] = $date->format('Y-m-d H:i');
        }

        $record['updated_at'] = new DateTime();
        $record['created_at'] = new DateTime();
        Reminder::insert($record);
        TgWhatCommandIsSet::deleteTgCommandsState($message["from"]["id"]);
    }

    // Дата ли это ?
    private function is_Date($str){
        return is_numeric(strtotime($str));
    }

    private function is_Time($str){
        return is_numeric(strtotime($str));
    }

    private function keyboardOptions(string $str){
        $keyboard = null;
        /*Выбор варианта напоминания*/
        if($str == "options"){
            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => 'Напоминать с периодичностью', 'callback_data' => 'remind_at_intervals']),
                    Keyboard::inlineButton(['text' => 'Напомнить один раз', 'callback_data' => 'remind_once'])
                );
        }elseif ($str == "calendar"){
            $month = date('m');
            $year = date('Y');
            $calendar = $this->get_calendar($month, $year);
            $keyboard = Keyboard::make([
                'keyboard' => $calendar,
                'resize_keyboard' => true,
                'one_time_keyboard' => true
            ]);
        }elseif ($str == "week"){
            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => 'Понедельник', 'callback_data' => '1']),
                    Keyboard::inlineButton(['text' => 'Вторник', 'callback_data' => '2']),
                    Keyboard::inlineButton(['text' => 'Среда', 'callback_data' => '3']),
                    Keyboard::inlineButton(['text' => 'Четверг', 'callback_data' => '4'])
                )
                ->row(
                    Keyboard::inlineButton(['text' => 'Пятница', 'callback_data' => '5']),
                    Keyboard::inlineButton(['text' => 'Суббота', 'callback_data' => '6']),
                    Keyboard::inlineButton(['text' => 'Воскресенье', 'callback_data' => '0'])
                )->row(
                    Keyboard::inlineButton(['text' => 'Каждый день', 'callback_data' => '8'])
                )
                ->setOneTimeKeyboard(true);
        }

        return $keyboard;
    }

    // Календарь текущего месяца
    function get_calendar(int $month, int $year): array {
        $prevMonthCallback = 'calendar-month-';
        if ($month === 1) {
            $prevMonthCallback .= '12-'.($year-1);
        } else {
            $prevMonthCallback .= ($month-1).'-'.$year;
        }

        $nextMonthCallback = 'calendar-month-';
        if ($month === 12) {
            $nextMonthCallback .= '1-'.($year+1);
        } else {
            $nextMonthCallback .= ($month+1).'-'.$year;
        }

        $start = new \DateTime(sprintf('%d-%d-01', $year, $month));

        $calendarMap = [
            [
                ['text' => '<', 'callback_data' => $prevMonthCallback],
                ['text' => $start->format('F Y'), 'callback_data' => 'calendar-months_list-'.$year],
                ['text' => '>', 'callback_data' => $nextMonthCallback],
            ],
            [
                ['text' => 'Mon', 'callback_data' => 'null_callback'],
                ['text' => 'Tue', 'callback_data' => 'null_callback'],
                ['text' => 'Wed', 'callback_data' => 'null_callback'],
                ['text' => 'Thu', 'callback_data' => 'null_callback'],
                ['text' => 'Fri', 'callback_data' => 'null_callback'],
                ['text' => 'Sat', 'callback_data' => 'null_callback'],
                ['text' => 'Sun', 'callback_data' => 'null_callback'],
            ],
        ];


        $end = clone $start;
        $end->modify('last day of this month');
        $iterEnd = clone $start;
        $iterEnd->modify('first day of next month');
        $row = 2;
        foreach (new DatePeriod($start, new DateInterval("P1D"), $iterEnd) as $date) {
            /** @var \DateTime $date */

            if (!isset($calendarMap[$row])) {
                $calendarMap[$row] = array_combine([1, 2, 3, 4, 5, 6, 7], [[], [], [], [], [], [], []]);
            }

            $dayIterator = (int)$date->format('N');
            if ($dayIterator != 1 && $start->format('d') === $date->format('d')) {
                for ($i = 1; $i < $dayIterator; $i++){
                    $calendarMap[$row][$i] = ['text' => ' ', 'callback_data' => 'null_callback'];
                }
            }

            $calendarMap[$row][$dayIterator] = ['text' => $date->format('d'), 'callback_data' => sprintf('calendar-day-%d-%d-%d', $date->format('d'), $month, $year)];

            if ($dayIterator < 7 && $end->format('d') === $date->format('d')) {
                for ($i = $dayIterator+1; $i <= 7; $i++){
                    $calendarMap[$row][$i] = ['text' => ' ', 'callback_data' => 'null_callback'];
                }
                $calendarMap[$row] = array_values($calendarMap[$row]);
                break;
            }

            if ($dayIterator === 7) {
                $calendarMap[$row] = array_values($calendarMap[$row]);
                $row++;
            }
        }

        return $calendarMap;
    }

    // Получить список напоминаний
    public function list($message){

        $user_id = TgUsers::where('telegram_user_id', $message["from"]["id"])->get()[0]->toArray()["user_id"];
        $remind = ReminderController::list($user_id);

        switch (count($remind)) {
            case 0:
                Response::send(["chat_id" => $message['from']['id'], "text" => "Нет напоминаний"]);
                break;
            case 1:
                $str = $this->getList($remind[0]);

                Response::send(["chat_id" => $message['from']['id'], "text" => $str]);

                break;
            default:
                foreach ($remind as $item) {
                    $str = $this->getList($item);
                    Response::send(["chat_id" => $message['from']['id'], "text" => $str]);
                }
                break;
        }
    }

    private function getList($remind){
        $str  = "ID = " . $remind["id"] . "\n\r";
        $str .= "TEXT = " . $remind["text"] . "\n\r";
        if($remind["date_reminder"] != null){
            $str .= "DATE = " . $remind["date_reminder"] . "\n\r";
        }else{
            switch($remind["repeat"] ){
                case 1:
                    $str .= "DATE = " . "Каждый понедельник" . "\n\r";
                    break;
                case 2:
                    $str .= "DATE = " . "Каждый вторник" . "\n\r";
                    break;
                case 3:
                    $str .= "DATE = " . "Каждый среду" . "\n\r";
                    break;
                case 4:
                    $str .= "DATE = " . "Каждый четверг" . "\n\r";
                    break;
                case 5:
                    $str .= "DATE = " . "Каждую пятницу" . "\n\r";
                    break;
                case 6:
                    $str .= "DATE = " . "Каждую субботу" . "\n\r";
                    break;
                case 0:
                    $str .= "DATE = " . "Каждое воскресенье" . "\n\r";
                    break;
                case 8:
                    $str .= "DATE = " . "Каждый день" . "\n\r";
                    break;
            }
            $str .= "TIME = " . $remind["time"];
        }

        return $str;
    }

    // Удалить напоминание
    public function delete($message, $id){

//        $user_id = TgUsers::where('telegram_user_id', $message["from"]["id"])->get()[0]->toArray()["user_id"];
//        return ;
        switch (ReminderController::delete($id)["status"]){
            case "success":
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Удалено"]);
                break;
            case "error":
                Response::send(["chat_id"=>$message['from']['id'], "text"=>"Ошибка"]);
                break;
        }
//        Response::send(["chat_id"=>$message['from']['id'], "text"=>"delete"]);
    }

    // Проверка на групповой чат
    private function chatIsGroup(array $message){
        if($message['chat']['id'] >= 0){
            return false;
        }else{
            return true;
        }
    }

    //  Удаление текущей команды
    public function clear(array $message):bool{
        TgWhatCommandIsSet::deleteTgCommandsState($message["from"]["id"]);
        return true;
    }

}
