<?php

namespace App\Modules\TelegramBot\Models;


use GuzzleHttp\Client;

use Telegram;
use Telegram\Bot\Api;

use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\RequestOptions;
//use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use App\Modules\FilesConverter\Models\SupportedMimeTypes;
use Illuminate\Support\Facades\Validator;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;

class TgUploadFiles{

    private $message = null;
    private $savePath = "";
    private $type = "";

    public function setMesage(array $message, string $type){
        $this->message = $message;
        $this->type = $type;
    }

    public function setSavePath(string $savePath){
        $this->savePath = $savePath;
    }

    public function uploadFile() {

        //Получаем ссылку на telegram файл
        $telegram = new Api(\Telegram::getAccessToken());
        $result = $telegram->getFile(['file_id' => $this->message[$this->type]['file_id']]);
        $pathIn = "https://api.telegram.org/file/bot".\Telegram::getAccessToken()."/".$result['file_path'];

        // Полный путь к файлу
        $tmpFilePath = "";
        // Имя файла
        $tmpFileName = "";
        // Расширение
        $expansion = "";
        // Попытки скачивания
        $i = 0;
        while(true) {
            try {


                $stack = HandlerStack::create();
                $stack->push(
                    Middleware::log(
                        new Logger('Logger'),
                        new MessageFormatter('{req_body} - {res_body}')
                    )
                );

                // Чтение расширения файла
                $expansion = $this->message["my_type"];
                // Создание уникального имени
                $tmpFileName = uniqid(strftime('%G-%m-%d'));
                // Создание полного пути к файлу
                $tmpFilePath  = $this->savePath.$tmpFileName.".".$expansion;
                // Открытие файла для записи
                $resource = fopen($tmpFilePath, 'w');
                // Открытие потока
                $stream   = stream_for($resource);
                // Создание нового клиента Telegram
                $client   = new Client();
                Log::Debug("ok_");
                $GuzzleHttpOptions[RequestOptions::SINK] = $stream;
                $GuzzleHttpOptions['handler'] = $stack;
                Log::Debug($GuzzleHttpOptions);
                // Скачивание файла
                $promise = $client->requestAsync('GET', $pathIn, $GuzzleHttpOptions);
                $response = $promise->wait();
                // Завершения записи файла
                $stream->close();
                break;
                // Добавить в исключения
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                Log::error("uploadFile = ".$e);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                Log::error("uploadFile = ".$e);
            } catch (\Telegram\Bot\Exceptions\TelegramSDKException $e) {
                Log::error("uploadFile = ".$e);
            } catch (\Exception $e) {
                Log::error("uploadFile = ".$e);
            }

            if($i == 5){
                return ["error"=>"Error uploadFile after five attempts"];
            }
            sleep(5);
            $i++;
        }

        if ($response->getStatusCode() === 200) {
            return ["start_file_Path" => $tmpFilePath, "start_file_name" => $tmpFileName, "start_extension" => $expansion];
        } else {
            return ["error"=>"Unknown uploadFile error"];
        }
    }


}