<?php
/**
 * Created by PhpStorm.
 * User: niku
 * Date: 22.06.19
 * Time: 12:06
 */

namespace App\Modules\TelegramBot\Models;

use App\Modules\TelegramBot\Models\DB\TgCommandsState;

class TgWhatCommandIsSet
{

    // Какая комманда установлена
    public static function whatCommandIsSet(int $from_id): string {
        $telegramCCS = self::getTgCommandsState($from_id);
        if(empty($telegramCCS->toArray())){
            return "";
        }else{
            return $command = $telegramCCS[0]->command;
        }
    }
    // Чтение комманды из БД
    public static function getTgCommandsState(int $from_id):object {
        return TgCommandsState::where('from_id', $from_id)->get();
    }
    // Удалить комманду из БД
    public static function deleteTgCommandsState(int $from_id){
        return TgCommandsState::where('from_id', $from_id)->delete();
    }

}