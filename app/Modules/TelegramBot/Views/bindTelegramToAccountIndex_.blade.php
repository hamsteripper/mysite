@extends('layouts.mainBlade')

@section('content2')

    <div class="container">
        <div id="mainDiv" class="p-3 mb-2 bg-dark text-white">Write Telegram Name</div>
    </div>
    <form action="{{url('bindTelegramToAccountSendCode_')}}" method="post" enctype="multipart/form-data" id="avatar">
        {{--Обязательный токен--}}
        {{ csrf_field() }}
        <div class="row justify-content-center align-items-center">
            <div class="col-md-10">
                {{--Строка для выбора файла--}}
                <div class="input-group">
                    <input type="text" class="form-control" id="ss" name="telegramUsername">
                </div>
                {{--Строка для кнопки отправки формы--}}
                <div class="row justify-content-center align-items-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </div>
        </div>
    </form>

@endsection