@extends('layouts.mainBlade')

@section('content2')

    @if (Session::has('status'))
        <div class="alert alert-info">
            <span>{{Session::get('status')}}</span>
        </div>
    @endif

    <div class="contrainer">
        <form  action="{{ route('setting.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Url callback для TelegramBot</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Действие <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                            <li><a href="#" onclick="document.getElementById('url_callback_bot').value = '{{url('')}}'">Вставить url</a></li>
                            <li><a href="#" onclick="event.defaultPrevented; document.getElementById('setWebhook').submit();">Отправить url</a></li>
                            <li><a href="#" onclick="event.defaultPrevented; document.getElementById('getWebhookInfo').submit();">Получить информацию</a></li>
                        </ul>
                    </div>
                    <input type="url" class="form-control" name="url_callback_bot" value="{{ $url_callback_bot ?? '' }}">
                </div>
            </div>


            <button class="btn btn-primary" type="submit">Сохранить</button>
        </form>



        <form action="{{ route('setting.setwebhook') }}" id="setWebhook"  method="post" >
            {{ csrf_field() }}
            <input type="hidden" name="url" value="{{ $url_callback_bot ?? '' }}">
        </form>

        <form action="{{ route('setting.getwebhookinfo') }}" id="getWebhookInfo"  method="post" style="display: none;">
            {{ csrf_field() }}
        </form>

    </div>


@endsection
