<?php

namespace App\Modules\YoutubeDownloader\Controllers;

use App\Http\Controllers\Controller;

//use App\Modules\TelegramBot\Models\Setting;
use Illuminate\Http\Request;

use YouTubeDownloader;

use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\Psr7;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Response;

use YoutubeDl\YoutubeDl;
use YoutubeDl\Exception\CopyrightException;
use YoutubeDl\Exception\NotFoundException;
use YoutubeDl\Exception\PrivateVideoException;

use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Modules\YoutubeDownloader\Models\Jobs\DownloadFiles;

class MainController extends Controller
{
    private $savePath;
    private $youtube_dl;

    public function __construct(){
        // Путь сохранения
        $this->savePath = storage_path()."/app/public/YoutubeDownloader/";
        // Путь к dll скачивания
        $this->youtube_dl = '/usr/local/bin/youtube-dl';
        // Создание директорий и поддиректорий
        if(!File::exists($this->savePath)){
            File::makeDirectory($this->savePath, 0775, true, true);
        }
    }

    public function index(){
        // Добавление пользовательской папки
        if(!File::exists($this->savePath.Auth::user()->id."/")){
            File::makeDirectory($this->savePath.Auth::user()->id."/", 0775, true, true);
        }
        // Сохранение пути для текущего пользователя в сессии
        Session::put("YoutubeDownloaderPath", $this->savePath.Auth::user()->id."/");
        return view('YoutubeDownloader::Index');
    }

    /* sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
    * sudo chmod a+rx /usr/local/bin/youtube-dl*/
    // Нахождение всех данных по ссылке
    public function getDate(Request $request){
        // Получение url ссылки
        $url = $request->get('urlYoutube');
        // Если NULL
        if($request->get('urlYoutube') == null){
            return view('YoutubeDownloader::Index');
        }
        // Настройки скачивателя
        $dl = new YoutubeDl([
            'skip-download' => true
        ]);
        // Установка пути к папке для скачивания
        $dl->setDownloadPath(Session::get("YoutubeDownloaderPath"));
        $dl->setBinPath($this->youtube_dl);

        try {
            $video = $dl->download($url);
            $audioArray = [];
            $videoArray = [];
            // Находим все аудио-форматы
            foreach ($video["formats"] as $currentFormat){
                if($currentFormat["format_note"] == "tiny" || $currentFormat["format_note"] == "DASH audio"){
                    array_push($audioArray, $currentFormat->toArray());
                }else{
                    array_push($videoArray, $currentFormat->toArray());
                }
            }
            // Сохранение параметров в сессию
            Session::put('audioArray', $audioArray);
            Session::put('videoArray', $videoArray);
            // Преобразуем массивы видео и аудио
            $audioArrayJson = json_encode($audioArray);
            $videoArrayJson = json_encode($videoArray);
            // Отправка найденных данных форматов на web
            return view('YoutubeDownloader::SelectionParameters',['audioArray' => $audioArrayJson, 'videoArray' => $videoArrayJson]);
        } catch (NotFoundException $e) {
            Log::error($e);
            return view('YoutubeDownloader::Index',['error' => $e]);
        } catch (PrivateVideoException $e) {
            Log::error($e);
            return view('YoutubeDownloader::Index',['error' => $e]);
        } catch (CopyrightException $e) {
            Log::error($e);
            return view('YoutubeDownloader::Index',['error' => $e]);
        } catch (\Exception $e) {
            Log::error($e);
            return view('YoutubeDownloader::Index',['error' => $e]);
        }
    }

    // Обработка выбранных данных
    public function downloader(Request $request){

        // Id пользователя
        $userId = Auth::user()->id;
        // Путь к папке пользователя
        $savePath = Session::get("YoutubeDownloaderPath");
        // Получение списков выбора
        $list1 = $request->get("List1"); // Тип данных
        $list2 = $request->get("List2"); // Тип видео или аудио
        $list3 = $request->get("List3"); // Тип аудио при его отсутствии в видео
        // Получение видео и аудио списков из памяти сессии
        $audioArray = Session::get('audioArray');
        $videoArray = Session::get('videoArray');

        // Подготовка
        if($list1 == null || $list2 == null) { // Если произошла какая-то фигня и первый или второй список вообще пустые
            // TODO
        }elseif($list1 == "Audio" && $list2 != null){ // Если выбрано скачивать аудио
            // Если выбран аудио
            $arrayList2 = explode("_", $list2);
            $audioElement = $audioArray[ $arrayList2[1] ];
            // Получаем параметры выбранного аудио
            $ParamA = $this->getAudioParameters($audioElement);
            // Отправка на скачивание
            DownloadFiles::dispatch($userId, "", $ParamA["url"], "", $ParamA["ext"], $savePath);

        }else if($list1 == "Video" && $list2 != null && $list3 == null){ // Если выбрано скачивать видео, со встроенным аудио
            // Если выбран видео, со встроенным аудио
            $arrayList2 = explode("_", $list2);
            $videoElement = $videoArray[ $arrayList2[1] ];
            // Получаем параметры выбранного видео
            $ParamV = $this->getVideoParameters($videoElement);
            // Отправка на скачивание
            DownloadFiles::dispatch(Auth::user()->id, $ParamV["url"], "", $ParamV["ext"], "", $savePath);

        }else if($list1 == "Video" && $list2 != null && $list3 != null){ // Если выбрано скачивать видео и аудио отдельно
            // Если выбраны аудио и видео
            $arrayList2 = explode("_", $list2);
            $arrayList3 = explode("_", $list3);
            $videoElement = $videoArray[ $arrayList2[1] ];
            $audioElement = $audioArray[ $arrayList3[1] ];
            // Получаем параметры выбранного аудио
            $ParamA = $this->getAudioParameters($audioElement);
            // Получаем параметры выбранного видео
            $ParamV = $this->getVideoParameters($videoElement);
            // Отправка на скачивание
            DownloadFiles::dispatch(Auth::user()->id, $ParamV["url"], $ParamA["url"], $ParamV["ext"], $ParamA["ext"], $savePath);
        }

        return view('YoutubeDownloader::GetFinishUrl');
    }

    // Получение параметров Аудио
    public function getAudioParameters(array $audioElement){
        // Параметры
        $audioUrl = "";
        $audioExtension = "";
        // Конфигурация параметров
        switch ($audioElement["format_note"]){
            case "DASH audio":
                $audioUrl = $audioElement["fragment_base_url"];
                $audioExtension = $audioElement["ext"];
                break;
            default:
                $audioUrl = $audioElement["url"];
                $audioExtension = $audioElement["ext"];
                break;
        }
        return array( "url" => $audioUrl, "ext" => $audioExtension);
    }

    // Получение параметров видео
    public function getVideoParameters(array $videoElement){
        // Параметры
        $videoUrl = "";
        $videoExtension = "";
        // Конфигурация параметров
        $videoUrl = $videoElement["url"];
        $videoExtension = $videoElement["ext"];
        return array( "url" => $videoUrl, "ext" => $videoExtension);
    }
}

