<?php

namespace App\Modules\YoutubeDownloader\Models\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\stream_for;
use GuzzleHttp\Psr7;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

use YoutubeDl\YoutubeDl;
use YoutubeDl\Exception\CopyrightException;
use YoutubeDl\Exception\NotFoundException;
use YoutubeDl\Exception\PrivateVideoException;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Modules\TelegramBot\Models\Response;
use App\Modules\TelegramBot\Models\DB\TgUsers;
use App\Modules\YoutubeDownloader\Models\Jobs\SendUrlToTelegram;
use App\Modules\YoutubeDownloader\Models\Jobs\MergeFiles;

class DownloadFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $videoUrl = "";
    private $audioUrl = "";
    private $userId = "";
    private $videoExt = "";
    private $audioExt = "";
    private $savePath = "";

    public function __construct($userId = "", $videoUrl = "", $audioUrl = "", $videoExt = "", $audioExt = "", $savePath = "")
    {
        $this->videoUrl = $videoUrl;
        $this->audioUrl = $audioUrl;
        $this->userId = $userId;
        $this->videoExt = $videoExt;
        $this->audioExt = $audioExt;
        $this->savePath = $savePath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Очистка файлов по текущему пути
        $this->clear($this->savePath);
        // Получение telegram user id
        $tgArray = TgUsers::where('user_id', $this->userId)->get()->toArray();
        if(count($tgArray) == 0){
            Log::error("Telegram tied to user");
            exit;
        }

        if($this->audioUrl != "" && $this->videoUrl == ""){
            // Скачивание файла
            $result = $this->downloadFile($this->savePath, $this->audioExt, $this->audioUrl);
            // Проверка на исключение
            if(!array_key_exists("error", $result)){
                // Создание url ссылки
                $url = $this->createUrl($result);
                // Отправка url в telegram
                SendUrlToTelegram::dispatch($tgArray[0]['telegram_user_id'], $url);
            }else{
                // Отправка ошибки в telegram
                SendUrlToTelegram::dispatch($tgArray[0]['telegram_user_id'], $result["error"]);
            }
        }elseif($this->audioUrl == "" && $this->videoUrl != ""){
            // Скачивание файла
            $result = $this->downloadFile($this->savePath, $this->videoExt, $this->videoUrl);
            if(!array_key_exists("error", $result)){
                //Создание url
                $url = $this->createUrl($result);
                // Отправка url в telegram
                SendUrlToTelegram::dispatch($tgArray[0]['telegram_user_id'], $url);
            }else{
                // Отправка ошибки в telegram
                SendUrlToTelegram::dispatch($tgArray[0]['telegram_user_id'], $result["error"]);
            }
        }elseif($this->audioUrl != "" && $this->videoUrl != ""){
            // Скачивание видео
            $resultV = $this->downloadFile($this->savePath, $this->videoExt, $this->videoUrl);
            // Скачивание аудио
            $resultA = $this->downloadFile($this->savePath, $this->audioExt, $this->audioUrl);
            // Имя фидео файла
            $start_video_file_name = uniqid(strftime('%G-%m-%d'));
            // Имя аудио файла
            $start_audio_file_name = uniqid(strftime('%G-%m-%d'));
            if(!array_key_exists("error", $resultV) && !array_key_exists("error", $resultA)){
                //Создание url
                $result["newFileName"] = $start_video_file_name . "." . $this->videoExt;
                $url = $this->createUrl($result);
                // Слияние файлов
                MergeFiles::dispatch($this->savePath, $resultV["start_file_Path"], $resultA["start_file_Path"], $start_video_file_name, $start_audio_file_name, $this->videoExt, $this->audioExt, $url, $tgArray[0]['telegram_user_id']);
            }else{
                // Отправка ошибки в telegram
                SendUrlToTelegram::dispatch($tgArray[0]['telegram_user_id'], "Merge download error");
            }
        }
    }

    // Скачивание файла
    private function downloadFile($savePath, $expansion, $url){
        // Имя файла
        $tmpFileName = "";
        // Попытки скачивания
        $i = 0;
        while(true) {
            try {
                // Создание уникального имени
                $tmpFileName = uniqid(strftime('%G-%m-%d'));
                // Создание полного пути к файлу
                $tmpFilePath = $savePath . $tmpFileName . "." . $expansion;
                // Открытие файла для записи
                $resource = fopen($tmpFilePath, 'w');
                // Открытие потока
                $stream = stream_for($resource);
                // Создание нового клиента Telegram
                $client = new Client();
                $GuzzleHttpOptions[RequestOptions::SINK] = $stream;
                $GuzzleHttpOptions[RequestOptions::IDN_CONVERSION] = false;
                // Скачивание файла
                $promise = $client->requestAsync('GET', $url, $GuzzleHttpOptions);
                $response = $promise->wait();
                // Закрытие потока
                $stream->close();
                break;
                // Добавить в исключения
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                Log::error($e);
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                Log::error($e);
            } catch (\Telegram\Bot\Exceptions\TelegramSDKException $e) {
                Log::error($e);
            } catch (\Exception $e) {
                Log::error($e);
            }
            //
            if($i == 5){
                return ["error"=>"Error uploadFile after five attempts"];
            }
            sleep(5);
            $i++;
        }

        // TODO если не скачался
        if ($response->getStatusCode() === 200) {
            return ["start_file_Path" => $tmpFilePath,
                "start_file_name" => $tmpFileName,
                "start_extension" => $expansion,
                "newFileName" => $tmpFileName . "." . $expansion];
        }else{
            return ["error"=>"Unknown uploadFile error"];
        }
    }


    // Создание url ссылки
    private function createUrl(array $parameters):string {

        $str = "YoutubeDownloader/".$this->userId."/".$parameters["newFileName"];
        $url = Storage::url($str);
        $url = env('APP_URL').$url;

        return $url;

    }

    // Очистка папки
    private function clear($path) {
        if (file_exists($path)) {
            foreach (glob($path.'*') as $file) {
                unlink($file);
            }
        }
    }
}
