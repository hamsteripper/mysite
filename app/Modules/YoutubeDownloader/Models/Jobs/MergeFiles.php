<?php

namespace App\Modules\YoutubeDownloader\Models\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Modules\YoutubeDownloader\Models\Jobs\SendUrlToTelegram;
use Illuminate\Support\Facades\Log;

class MergeFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $savePath = "";
    private $videoPath = "";
    private $audioPath = "";
    private $start_extension_video = "";
    private $start_extension_audio = "";
    private $start_video_file_name = "";
    private $start_audio_file_name = "";
    private $final_url = "";
    private $telegram_user_id = "";

    public function __construct($savePath, $videoPath, $audioPath, $start_video_file_name, $start_audio_file_name, $start_extension_video, $start_extension_audio,  $final_url, $telegram_user_id)
    {
        $this->savePath = $savePath;
        $this->videoPath = $videoPath;
        $this->audioPath = $audioPath;
        $this->start_extension_video = $start_extension_video;
        $this->start_extension_audio = $start_extension_audio;
        $this->start_video_file_name = $start_video_file_name;
        $this->start_audio_file_name = $start_audio_file_name;
        $this->final_url = $final_url;
        $this->telegram_user_id = $telegram_user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        // Проверка на типы файлов
        if($this->start_extension_audio == "m4a"){ //если аудио файл имеет расширение m4a
            // Новое расширение файла
            $finish_audio_extension = "webm";
            // Конвертируем аудио файл
            $result1 = $this->audioConvert($this->savePath, $this->audioPath, $finish_audio_extension);
            $this->audioPath = $result1["newFilePath"];
        }
        // Слияние видео и аудил файлов
        $result2 = $this->fileMerging($this->savePath, $this->videoPath, $this->audioPath, $this->start_extension_video, $this->start_video_file_name);
        // Отправка url в telegram
        SendUrlToTelegram::dispatch($this->telegram_user_id, $this->final_url);
    }

    // Предварительная конвертация аудио файла
    public function audioConvert($savePath, $audioPath, $finish_auduo_extension){
        // Генерация нового имени
        $start_file_name = uniqid(strftime('%G-%m-%d'));
        // Создание пути к файлу
        $newFilePath = $savePath . $start_file_name . "." . $finish_auduo_extension;
        // Создание комманды конвертации
        $command = "ffmpeg -i ".$audioPath." -c:a libopus " .$newFilePath. " 2>&1";
        // Выполнение комманды
        $output = shell_exec($command);
        //TODO --- output exception
        //Создание результата
        return ["start_file_name" => $start_file_name, "newFilePath" => $newFilePath, "start_extension" => $finish_auduo_extension, "output" => $output];
    }

    // Слияние видео и аудио
    public function fileMerging($savePath, $videoPath, $audioPath, $finish_extension, $start_file_name){
        // Создание пути к файлу
        $newFilePath = $savePath . $start_file_name . "." . $finish_extension;
        // Создание комманды слияния
        $command = "ffmpeg -i ".$videoPath." -i ".$audioPath." -c:v copy -c:a copy -shortest -strict -2 " .$newFilePath. " -c:v copy -c:a copy 2>&1";
        // Выполнение комманды
        $output = shell_exec($command);
        //TODO --- output exception
        //Создание результата
        return ["start_file_name" => $start_file_name, "newFilePath" => $newFilePath, "start_extension" => $finish_extension, "output" => $output];
    }


}
