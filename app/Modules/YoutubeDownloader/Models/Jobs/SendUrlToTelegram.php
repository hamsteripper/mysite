<?php

namespace App\Modules\YoutubeDownloader\Models\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Modules\TelegramBot\Models\Response;
use Illuminate\Support\Facades\Log;

use Telegram;

use Telegram\Bot\Api;

class SendUrlToTelegram implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $chat_id = "";
    private $url = "";

    public function __construct($chat_id, $url)
    {
        $this->url = $url;
        $this->chat_id = $chat_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug(\Telegram::getAccessToken());
        // Отправка url в telegram
        Response::send(["chat_id"=>$this->chat_id, "text"=>$this->url]);
    }


}
