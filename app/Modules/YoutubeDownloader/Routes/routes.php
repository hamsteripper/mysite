<?php

Route::group(
    [
        'namespace' => 'App\Modules\YoutubeDownloader\Controllers',
        'middleware' => ['web','auth'],
        'as' => 'YoutubeDownloader.'
    ],
    function(){
        Route::get('/YoutubeDownloader', ['uses' => 'MainController@index'])->name('mainControllerIndex');
        Route::post('/YoutubeDownloaderSelect', ['uses' => 'MainController@getDate'])->name('mainControllerGetDate');
        Route::post('/YoutubeDownloaderDownload', ['uses' => 'MainController@downloader'])->name('mainControllerDownloader');
    }
);