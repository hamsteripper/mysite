@extends('layouts.mainBlade')

@section('content1')

    <div class="container">
        <div id="mainDiv" class="p-3 mb-2 bg-dark text-white">.bg-white</div>
    </div>

    <button type="button" onclick="downloadNow()" class="btn btn-primary btn-lg btn-block">Return to Home</button>

    <script>
        document.getElementById("mainDiv").innerHTML = "Expect messages";
        function downloadNow() {
            var link = document.createElement('a');
            link.setAttribute('href', "/home");
            link.click();
            return false;
        }
    </script>


@endsection