@extends('layouts.mainBlade')

@section('content1')

    <style>

        .card{
            border: 0px;
            background: inherit;
            /*background-color: rgba(10,50,50,0.4);*/
            color: white;
        }

        .custom-file-label{
            /*border: 0px;*/
            background-color: rgba(10,50,50,0.5);
            color: white;
        }

        .custom-file{

            /*border: 0px;*/
            background-color: rgba(10,50,50,0.5);
            color: white;

            z-index: 0 !important;
            /*position: relative;*/
            /*display: inline-block;*/
            /*max-width: 100%;*/
            /*height: 2.5rem;*/
            /*cursor: pointer;*/
        }

        input:focus{
            background-color: rgba(10,50,50,0.5) !important;
            color: white !important;
        }

        input::placeholder {
            color: white !important;
            /*font-size: 1.5em;*/
        }


    </style>

    <form action="{{url('YoutubeDownloaderSelect')}}" method="post" enctype="multipart/form-data" id="avatar">
        {{--Обязательный токен--}}
        {{ csrf_field() }}
        <div class="row justify-content-center align-items-center">
            <div class="col-md-10">
                {{--Строка для выбора файла--}}
                <div class="input-group ">
                    <input type="text" class="form-control custom-file" name="urlYoutube" placeholder="https://www.youtube.com/watch?v=g5cAVumGFH8" >
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">set url</span>
                    </div>
                </div>
                {{--Строка для кнопки отправки формы--}}
                <div class="row justify-content-center align-items-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </div>
        </div>
    </form>


@endsection
