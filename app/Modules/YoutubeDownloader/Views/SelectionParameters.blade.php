@extends('layouts.mainBlade')

@section('content1')

    <script type="text/javascript" src="assets/linkedselect.js"></script>

    {{--Преобразование массива php --}}
    <script>
        var audioArray = <?php echo $audioArray ?>;
        var videoArray = <?php echo $videoArray ?>;

        audioArray.forEach(function(item, i, arr) {
            console.log( i + ": " + item + " (массив:" + arr + ")" );
            console.log(item);
        });

        videoArray.forEach(function(item, i, arr) {
            console.log( i + ": " + item + " (массив:" + arr + ")" );
            console.log(item);
        });

    </script>

    <form action="{{url('YoutubeDownloaderDownload')}}" method="post" enctype="multipart/form-data" id="avatar">
        {{--Обязательный токен--}}
        {{ csrf_field() }}

        <div class="form-group">
            <label for="sel1">What are loading ?</label>
            <select class="form-control" id="List1" name="List1">
                <option style="display:none"></option>
                <option value="Video"> Video </option>
                <option value="Audio"> Audio </option>
            </select>
        </div>

        <div class="form-group">
            <label for="sel1">Select audio or video formats</label>
            <select class="form-control" id="List2" name="List2">
                <option style="display:none"></option>
            </select>
        </div>

        <div class="form-group">
            <label for="sel1">Select audio for video</label>
            <select class="form-control" id="List3" name="List3">
                <option style="display:none"></option>
            </select>
        </div>
        {{--</div>--}}

        <div class="row justify-content-center align-items-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>

    </form>

    {{--<button onclick="myFunction('List1', 'List2', 'List3')">Click me</button>--}}

    <script type="text/javascript">
        // Создаем новый объект связанных списков
        var syncList1 = new syncList;

        syncList1.dataList = {
            'Video':{},
            'Audio':{}
        };

        audioArray.forEach(function(item, i, arr) {
            syncList1.dataList['Audio']['audioItem_' + i] = item.format + " (audio_frequency_" + item.asr + ")" + " (bitrate_" + item.tbr + ")";
        });

        videoArray.forEach(function(item, i, arr) {
            syncList1.dataList['Video']['videoItem_' + i] = item.format + " (audio_frequency_" + item.asr + ")" + " (bitrate_" + item.tbr + ")";
            if(item.asr === null){
                console.log("XXXXXXXXXXX");
                syncList1.dataList['videoItem_' + i] = {};
                audioArray.forEach(function(item, j, arr) {
                    syncList1.dataList['videoItem_' + i]['audioForVideoItem_' + j] = item.format + " (audio_frequency_" + item.asr + ")" + " (bitrate_" + item.tbr + ")";
                });
            }
        });

        // Включаем синхронизацию связанных списков
        syncList1.sync("List1","List2","List3");
    </script>

@endsection