<?php
return [
    'modules' => [
        'Dashboard',
        'TelegramBot',
        'FilesConverter',
        'DevicesDatabase',
        'Crud_1',
        'OneToOne',
        'SearchInExcelFile',
        'YoutubeDownloader',
        'Library',
        'BidsDatabase',
        'Reminder',
        'Notebook',
        'Toast',
        'Preloader',
        ],
];
