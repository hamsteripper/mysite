// / отслеживаем нажатие кнопки сохранения файла в базу данных
document.getElementById('save_button').addEventListener('click', function(e) {
    // записываем содержимое нашего редактора в хранилище, при этом ячейка связана с текущим документом
    // localStorage.setItem(N_store + 'text_in_editor', document.getElementById('editor').innerHTML);
    // var h1 = document.getElementById('h1_name').innerHTML;
    // var editor = document.getElementById('editor').innerHTML;

    // Создание формы с отправляемыми параметрами
    var data = new FormData();
    data.append('file_name', "h1");
    // data.append('data', editor);
    data.append("_token", $('meta[name="csrf-token"]').attr('content'));

    // Создание метода отправки и получения асинхронных запросов
    var request = new XMLHttpRequest();
    request.open('POST', '/notebook/save');
    request.send(data);
    request.onreadystatechange = function () { // Дожидаемся ответа
        if (request.readyState == 4 && request.status == 200){// Делаем проверку если ответ получен и страница отдала код 200 (открылась)
            var response = request.responseText; // Получаем ответ как текст (включая html) и сохраням в переменную

            jsonTest(response);
        }
    }
} );


function jsonTest(jsonObj) {
    var jsonObj = JSON.parse(jsonObj);
    console.log(jsonObj); // Выводим данные в консоль
}
