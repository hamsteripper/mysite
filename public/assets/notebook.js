// Аналог jQuery $(document).ready(function() { TO DO });
document.addEventListener("DOMContentLoaded", function(event) {


    /****************************************************************************/
    var fileMenu = document.querySelector("#file-menu");
    var folderMenu = document.querySelector("#folder-menu");
    var folderMainMenu = document.querySelector("#folder-mainMenu");
    var newFolderMenu = document.querySelector("#newFolder-menu");
    var newFileMenu = document.querySelector("#newFile-menu");
    var deleteFileMenu = document.querySelector("#deleteFile-menu");
    var deleteFolderMenu = document.querySelector("#deleteFolder-menu");
    var editFileMenu = document.querySelector("#editFile-menu");
    var file = 0;
    var active = "context-menu--active";
    var currentActiveFileOrFolder = null;
    var currentActiveFile = null;
    /****************************************************************************/
    // Обработчик обхода json объекта (дерева в глабину)
    var listContentsOf = function (tree, container = 'tree_container', depth = '1') {
        // Перебор элементов текущего уровня
        for (var i = 0; i < tree.length; i++) {
            if (tree[i].type == 'file') {
                addFile(depth, tree[i].id, tree[i].name);
            } else if (tree[i].type == 'folder') {
                subID = depth + i;
                addFolder(depth, subID, tree[i].name);
                // Рекурсивный вызов функции
                listContentsOf(tree[i].contents, subID + '_folder', subID);
            }
        }
    };
    listContentsOf(tree);

    function addFolder(depth, subID, tree_name) {
        // Найти элемент ul из уровня выше
        var elementUL = document.getElementById(depth + "_folder");
        // Создать новый элемент li
        var elementLI = document.createElement("li");
        elementLI.classList.add("folder");
        // Создать новый элемент span
        var elementSPAN = document.createElement("span");
        elementSPAN.textContent = ' ' + tree_name;
        elementSPAN.classList.add("fa");
        elementSPAN.classList.add("fa-folder");
        // Создать новый элемент ul
        var elementULnew = document.createElement("ul");
        elementULnew.setAttribute("id", subID + "_folder");
        elementULnew.setAttribute("hidden","true");
        // Положить элементы SPAN и ULnew и в элемент LI
        elementLI.appendChild(elementSPAN);
        elementLI.appendChild(elementULnew);
        // Положить элементы LIи в элемент UL
        elementUL.appendChild(elementLI);
    }

    function addFile(depth, id = null, tree_name) {
        // Найти элемент ul из уровня выше
        var elementUL = document.getElementById(depth + '_folder');
        // Создать новый элемент li
        var elementLI = document.createElement("li");
        if(id!=null) elementLI.setAttribute("id", id + "_" + "file");
        elementLI.classList.add("tdItem");
        elementLI.classList.add("file");
        // Создать новый элемент A
        elementLI.textContent = tree_name;
        // Положить элемент A в элемент LI и в элемент UL
        elementUL.appendChild(elementLI);
        return elementLI;
    }

    /**************************************************************************************************/
    function focusInputElement(currentEl){
        currentEl.focus();
        currentEl.select();
    }

    // function focusInputElementMain(currentEl){
    //     currentEl.focus();
    //     currentEl.select();
    // }

    document.getElementById("newFile").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, newFileMenu, false);
        //
        var currentEl = document.getElementById("newFileNameInput");
        currentEl.value = "";
        focusInputElement(currentEl);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    document.getElementById("renameFile").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, editFileMenu, false);
        // Записываем старое имя в элемент для редактирования имени файла
        var currentEl = document.getElementById("editFileNameInput");
        currentEl.value = currentActiveFileOrFolder.target.textContent;
        focusInputElement(currentEl);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    document.getElementById("deleteFile").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, deleteFileMenu, false);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    document.getElementById("newFolder").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, newFolderMenu, false);
        //
        var currentEl = document.getElementById("newFolderNameInput");
        currentEl.value = "";
        focusInputElement(currentEl);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    document.getElementById("deleteFolder").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, deleteFolderMenu, false);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    document.getElementById("newFile_mainMenu").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, newFileMenu, false);
        //
        var currentEl = document.getElementById("newFileNameInput");
        currentEl.value = "";
        focusInputElement(currentEl);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    document.getElementById("newFolder_mainMenu").onclick = function(event){
        event.stopPropagation();
        // Закрытие меню
        toggleMenuOff(false);
        // Открыть меню
        toggleMenuOn(currentActiveFileOrFolder, newFolderMenu, false);
        //
        var currentEl = document.getElementById("newFolderNameInput");
        currentEl.value = "";
        focusInputElement(currentEl);
        // Позиция меню
        positionMenu(currentActiveFileOrFolder);
    }

    // Обработчик input key
    document.getElementById("newFileNameInput").onkeypress = function(event){
        if(event.key === "Enter"){
            if (currentActiveFileOrFolder.target.tagName == 'SPAN' && (currentActiveFileOrFolder.target.parentNode.classList.contains("mainFolder") || currentActiveFileOrFolder.target.parentNode.classList.contains("folder"))) {
                var newFileNameInput = document.getElementById("newFileNameInput");
                var ul = currentActiveFileOrFolder.target.parentNode.querySelector('ul').children;
                var maxFolderId = 0;
                for (let li of ul) {
                    var em = li.querySelector('ul');
                    if (em !== null) {
                        var id = em.id.split("_")[0];
                        if (id > maxFolderId) {
                            maxFolderId = id;
                        }
                    }
                }
                var ulCurrentId = currentActiveFileOrFolder.target.parentNode.querySelector('ul').id.split("_")[0];
                var subID = ulCurrentId + (++maxFolderId);
                // Создание файла
                var newLI = addFile(String(ulCurrentId), null, newFileNameInput.value);
                // Путь к файлу
                var path = filePathCalculate(newLI, "");
                var d = currentActiveFile;
                // Добавление файла в БД
                addNewFileToDB(path, newFileNameInput.value, currentActiveFileOrFolder, newLI);
                //Делаем новый фал активным
                if(currentActiveFile === null) currentActiveFile = newLI;
                // Очистка значения
                newFileNameInput.value = "";
                // Закрытие меню
                toggleMenuOff();
                //  Показать файл
                activeFile();
            }
        }
    }

    // Обработчик input key
    document.getElementById("editFileNameInput").onkeypress = function(event){
        if(event.key === "Enter"){
            if (currentActiveFileOrFolder.target.tagName == 'LI' && currentActiveFileOrFolder.target.classList.contains("file")) {
                var editFileNameInput = document.getElementById("editFileNameInput");
                var liCurrentId = currentActiveFileOrFolder.target.id.split("_")[0];
                // Изменение имени файла в БД
                renameFileInDB(liCurrentId, editFileNameInput.value);
                // Переименовывание файла
                currentActiveFileOrFolder.target.innerHTML = editFileNameInput.value;
                editFileNameInput.value = "";
                // Закрытие меню
                toggleMenuOff();
            }
        }
    }

    // Обработчик нажатия на меню удаления
    document.getElementById("deleteFile-menu").onclick = function (event){
        if (event.target.tagName == 'BUTTON' && event.target.id == "deleteFileButtonYes" ) {
            // TODO --- сохранить изменения в файле в базу данных
            var editor = document.getElementById("editor");
            var id_file = currentActiveFileOrFolder.target.id;
            var id = id_file.split("_")[0];
            deleteFileFromBD(editor, id, id_file);
            // Закрытие меню
            toggleMenuOff();
        }else if(event.target.tagName == 'BUTTON' && event.target.id == "deleteFileButtonNo"){
            var v = 0;
            // Закрытие меню
            toggleMenuOff();
        }
    }




    // Файл в пределах видимости
    function activeFile() {
        document.getElementById('editor').style.display = "block";
    }
    function deactivateFile() {
        document.getElementById('editor').style.display = "none";
    }
    // Обработчик input key для папки
    document.getElementById("newFolderNameInput").onkeypress = function(event){
        if(event.key === "Enter"){
            if (currentActiveFileOrFolder.target.tagName == 'SPAN' && (currentActiveFileOrFolder.target.parentNode.classList.contains("mainFolder") || currentActiveFileOrFolder.target.parentNode.classList.contains("folder"))) {
                var ul = currentActiveFileOrFolder.target.parentNode.querySelector('ul').children;
                var maxFolderId = 0;
                for (let li of ul) {
                    var em = li.querySelector('ul');
                    if(em !== null){
                        var id = em.id.split("_")[0];
                        if(id > maxFolderId) {
                            maxFolderId = id;
                        }
                    }
                }
                var newFolderNameInput = document.getElementById("newFolderNameInput");
                var ulCurrentId = currentActiveFileOrFolder.target.parentNode.querySelector('ul').id.split("_")[0];
                var subID = ulCurrentId + (++maxFolderId);
                addFolder(String(ulCurrentId), String(subID), newFolderNameInput.value);
                // Очистка значения
                newFolderNameInput.value = "";
                // Закрытие меню
                toggleMenuOff();
            }
        }
    }

    // Рекурсивное удаление всех файлов и папок в текущей папке
    function deleteAllFilesAndFoldersInFolder(li_Folder){
        var li = li_Folder.children;
        var ch = li[1].children;
        for (var i = 0; i < ch.length; i++) {
            if(ch[i].classList.contains("file")){
                // TODO --- сохранить изменения в файле в базу данных
                var editor = document.getElementById("editor");
                var id_file = ch[i].id;
                var id = id_file.split("_")[0];
                deleteFileFromBD(editor, id, id_file, false);
            }else{
                deleteAllFilesAndFoldersInFolder(ch[i]);
            }
        }
    }
    // Обработчик нажатия на меню удаления папки
    document.getElementById("deleteFolder-menu").onclick = function (event){
        if (event.target.tagName == 'BUTTON' && event.target.id == "deleteFileButtonYes" ) {
            // TODO --- сохранить изменения в файле в базу данных
            // var editor = document.getElementById("editor");
            var li_Folder = currentActiveFileOrFolder.target.parentNode;
            deleteAllFilesAndFoldersInFolder(li_Folder);
            li_Folder.remove();
            // Закрытие меню
            toggleMenuOff();
        }else if(event.target.tagName == 'BUTTON' && event.target.id == "deleteFileButtonNo"){
            // Закрытие меню
            toggleMenuOff();
        }
    }

    // Обработчик input key
    document.getElementById("saveFile").onclick = function(event){
        if(currentActiveFile !== null){
            saveFileToDB(currentActiveFile);
        }
    }
    // Вычисление полного пути от элемента
    function filePathCalculate(element, path){

        if(element.parentNode === undefined || element.parentNode.parentNode === undefined || element.parentNode.parentNode.querySelector("span") === undefined){
            // todo
        }else{
            var text = element.parentNode.parentNode.querySelector("span").textContent;
            if(text === "/"){
                return path;
            }else{
                element = element.parentNode.parentNode;
                path = text + "/" + path;
                path = filePathCalculate(element, path);
            }
        }

        return path;
    }

    // Установление id для файла
    function setId(id, newLI){
        newLI.setAttribute('id', id + "_" + "file");
    }
    /**************************************************************************************************/
    function deleteUlElement(id_file){
        document.getElementById(id_file).remove();
    }

    document.body.onkeypress = function (event){
        // Обработка комбинации crtl + shift + s (для сохранения текузего файда в бд)
        if (event.ctrlKey && event.shiftKey && (event.key === "s" || event.key === "S") && (currentActiveFile !== null)) { // T
            saveFileToDB(currentActiveFile);
        }else if(event.key === "Escape"){
            toggleMenuOff();
        }
    }

    document.body.onkeyup = function (event){
        if(event.key === "Escape"){
            toggleMenuOff();
        }
    }

    // Обработчик нажатия на страницу левой кнопкой
    document.body.onclick = function (event){
        // Закрытие меню, если открыто
        if (event.target.tagName == 'SPAN' && event.target.parentNode.classList.contains("folder")) {
            // Нажатие на папку
            // Получение элемента UL рядом со span
            let childrenContainer = event.target.parentNode.querySelector('ul');
            if (!childrenContainer) return; // нет детей
            // Спрятать или открыть подменю
            childrenContainer.hidden = !childrenContainer.hidden;
            // Отображать как открытую или закрытую папку
            if(event.target.classList.contains("fa-folder")){
                event.target.classList.remove("fa-folder");
                event.target.classList.add("fa-folder-open");
            }else if(event.target.classList.contains("fa-folder-open")){
                event.target.classList.remove("fa-folder-open");
                event.target.classList.add("fa-folder");
            }
        }else if(event.target.tagName == "LI" && event.target.classList.contains("file")){
            if(currentActiveFile === null){
                activeFile();
                currentActiveFile = event.target;
                loadFileFromDB(currentActiveFile);
            }else{
                saveFileToDB(currentActiveFile);
                if(currentActiveFile !== event.target) {
                    currentActiveFile = event.target;
                    loadFileFromDB(currentActiveFile);
                }
            }
            document.getElementById('h1_name').innerHTML = currentActiveFile.innerHTML;
        }else if(event.target.parentNode.id == "newFolder-menu" || event.target.id == "newFolder-menu"){
            // Нажатие на файл (открытие файла)
            console.log("file left click");
        }else if(event.target.parentNode.id == "newFile-menu" || event.target.id == "newFile-menu"){
            // Нажатие на файл (открытие файла)
            console.log("file left click");
        }else if(event.target.parentNode.id == "deleteFile-menu" || event.target.id == "deleteFile-menu"){
            // Нажатие на файл (открытие файла)
            console.log("file left click");
        }else if(event.target.parentNode.id == "deleteFolder-menu" || event.target.id == "deleteFolder-menu"){
            // Нажатие на файл (открытие файла)
            console.log("file left click");
        }else if(event.target.parentNode.id == "editFile-menu" || event.target.id == "editFile-menu"){
            // Нажатие на файл (открытие файла)
            console.log("file left click");
        }else{
            // Закрытие меню
            toggleMenuOff();
        }
    };
    // Добавление файла в бд и получение id
    function addNewFileToDB(path, fileName, currentActiveFileOrFolder, newLI){
        // Создание формы с отправляемыми параметрами
        var data = new FormData();
        // Токен
        data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        // Путь к файлу
        data.append('path', path);
        // Имя файла
        data.append('document_name', fileName);
        // Создание метода отправки и получения асинхронных запросов
        var request = new XMLHttpRequest();
        request.open('POST', '/notebook/addNewFileToDB');
        request.send(data);
        request.onreadystatechange = function () { // Дожидаемся ответа
            if (request.readyState == 4 && request.status == 200){// Делаем проверку если ответ получен и страница отдала код 200 (открылась)
                var response = request.responseText; // Получаем ответ как текст (включая html) и сохраням в переменную
                var dates = JSON.parse(response);
                if(dates.id === undefined){
                    // todo
                    return false;
                }else{
                    Toast.add({
                        // header: 'Название заголовка',
                        text: 'Файл создан',
                        body: 'Текст сообщения',
                        color: '#28a745',
                        autohide: true,
                        delay: 3000
                    });
                    setId(dates.id, newLI);
                    return true;
                }
            }
        }
    }
    // Сохранение изменений в базу данных
    function saveFileToDB(currentActiveFile){
        // TODO --- сохранить изменения в файле в базу данных
        var editor = document.getElementById("editor");
        var id = currentActiveFile.id;
        id = id.split("_")[0];
        // Создание формы с отправляемыми параметрами
        var data = new FormData();
        // Токен
        data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        // Имя файла
        data.append('id', id);
        if(editor.innerHTML === undefined){
            data.append('text', "");
        }else{
            data.append('text', editor.innerHTML);
        }
        // Создание метода отправки и получения асинхронных запросов
        var request = new XMLHttpRequest();
        request.open('POST', '/notebook/saveFile');
        request.send(data);
        request.onreadystatechange = function () { // Дожидаемся ответа
            if (request.readyState == 4 && request.status == 200){// Делаем проверку если ответ получен и страница отдала код 200 (открылась)
                var response = request.responseText; // Получаем ответ как текст (включая html) и сохраням в переменную
                console.log("save");
                //TODO
                /*
                Параметры функции add:
                header (строка) - название заголовка
                body (строка) - текст сообщения
                color (строка) - цвет в формате #rrggbb
                autohide (булево) - скрывать ли автоматически всплывающее сообщение
                delay (число) - количество миллисекунд, после которых сообщение будет автоматически скрыто
                */
                Toast.add({
                    header: 'Сохранение файла',
                    text: 'Файл сохранён',
                    color: '#28a745',
                    autohide: true,
                    delay: 1500
                });
            }
        }
    }
    // Загрузка файл из базы данных
    function loadFileFromDB(currentActiveFile){
        var editor = document.getElementById("editor");
        var id = currentActiveFile.id;
        id = id.split("_")[0];
        // Создание формы с отправляемыми параметрами
        var data = new FormData();
        // Токен
        data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        // Имя файла
        data.append('id', id);
        // Создание метода отправки и получения асинхронных запросов
        var request = new XMLHttpRequest();
        request.open('POST', '/notebook/loadFile');
        request.send(data);
        request.onreadystatechange = function () { // Дожидаемся ответа
            if (request.readyState == 4 && request.status == 200){// Делаем проверку если ответ получен и страница отдала код 200 (открылась)
                var response = request.responseText; // Получаем ответ как текст (включая html) и сохраням в переменную
                var dates = JSON.parse(response);
                if(dates.text === null){
                    editor.innerHTML = "";
                    return false;
                }else{
                    editor.innerHTML = dates.text;
                    return true;
                }
            }
        }
    }
    // Редактирование имени файла
    function renameFileInDB(liCurrentId, editFileNameInput){
        // Создание формы с отправляемыми параметрами
        var data = new FormData();
        // Токен
        data.append('_token', $('meta[name="csrf-token"]').attr('content'));
        // id файла
        data.append('id', liCurrentId);
        // Новое имя файла
        data.append('document_name', editFileNameInput);
        // Создание метода отправки и получения асинхронных запросов
        var request = new XMLHttpRequest();
        request.open('POST', '/notebook/renameFile');
        request.send(data);
        request.onreadystatechange = function () { // Дожидаемся ответа
            if (request.readyState == 4 && request.status == 200){// Делаем проверку если ответ получен и страница отдала код 200 (открылась)
                var response = request.responseText; // Получаем ответ как текст (включая html) и сохраням в переменную
                if(response[0] == "raname"){
                    // TODO
                }else{
                    console.log("File not edit");
                }
            }
        }
    }

    function deleteFileFromBD(editor, id, id_file, deleteLI = true){
        // Создание формы с отправляемыми параметрами
        var data = new FormData();
        // Токен
        data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        // id файла
        data.append('id', id);
        // Создание метода отправки и получения асинхронных запросов
        var request = new XMLHttpRequest();
        request.open('POST', '/notebook/deleteFile');
        request.send(data);
        request.onreadystatechange = function () { // Дожидаемся ответа
            if (request.readyState == 4 && request.status == 200){// Делаем проверку если ответ получен и страница отдала код 200 (открылась)
                var response = request.responseText; // Получаем ответ как текст (включая html) и сохраням в переменную
                if(response == 1){
                    if(currentActiveFile !== null && id_file == currentActiveFile.id){
                        editor.textContent = "";
                        deactivateFile();
                        currentActiveFile = null;
                    }
                    if(deleteLI) deleteUlElement(id_file);
                }else{
                    console.log("File not delete");
                }
            }
        }
    }

    // Обработчик нажатия на страницу правой кнопкой
    document.body.oncontextmenu = function (event){
        // Отмена действия по умолчанию
        event.preventDefault();
        // Действия
        if (event.target.tagName == 'SPAN' && event.target.parentNode.classList.contains("folder")) {
            // Закрытие меню
            toggleMenuOff();
            // Открыть меню
            toggleMenuOn(event, folderMenu);
            // Позиция меню
            positionMenu(event);
        }else if (event.target.tagName == 'SPAN' && event.target.parentNode.classList.contains("mainFolder")) {
            // Закрытие меню
            toggleMenuOff();
            // Открыть меню
            toggleMenuOn(event, folderMainMenu);
            // Позиция меню
            positionMenu(event);
        }else if(event.target.tagName == "LI" && event.target.classList.contains("file")){
            // Закрыть меню папок
            toggleMenuOff();
            // Открыть меню файлов
            toggleMenuOn(event, fileMenu);
            // Позиция меню
            positionMenu(event);
        }else{
            // Закрытие меню
            toggleMenuOff();
        }
    }

    // Открыть меню файла
    function toggleMenuOn(event, element, action = true){
        if ( file !== 1 ) {
            file = 1;
            if (action) currentActiveFileOrFolder = event;
            element.classList.add(active);
        }
    }
    // Закрыть меню
    function toggleMenuOff(action = true){
        if ( file !== 0 ) {
            file = 0;
            if (action) currentActiveFileOrFolder = null;
            fileMenu.classList.remove(active);
            folderMenu.classList.remove(active);
            folderMainMenu.classList.remove(active);
            newFolderMenu.classList.remove(active);
            deleteFolderMenu.classList.remove(active);
            newFileMenu.classList.remove(active);
            deleteFileMenu.classList.remove(active);
            editFileMenu.classList.remove(active);
        }
    }
    /**************************************************************************************************/
    // Позиционирование
    var menuPosition;
    var menuPositionX;
    var menuPositionY;

    // Позиционирование
    function getPosition(e) {
        var posx = 0;
        var posy = 0;
        if (!e) var e = window.event;
        if (e.pageX || e.pageY) {
            // Получение параметров отображения
            var display = document.getElementById('nav-accordion').style.display;
            // Подбор размеров
            if(display === ""){
                posx = e.pageX - 210;
            }else if(display === 'block'){
                posx = e.pageX - 210;
            }else if(display === "none"){
                posx = e.pageX + 0;
            }
            posy = e.pageY - 120;
        } else if (e.clientX || e.clientY) {
            // posx = e.clientX + document.body.scrollLeft +
            //     document.documentElement.scrollLeft;
            // posy = e.clientY + document.body.scrollTop +
            //     document.documentElement.scrollTop;
            posx = e.clientX;
            posy = e.clientY;
        }
        return {
            x: posx,
            y: posy
        }
    }

function positionMenu(e){
    menuPosition = getPosition(e);
    menuPositionX = menuPosition.x + "px";
    menuPositionY = menuPosition.y + "px";

    fileMenu.style.left = menuPositionX;
    fileMenu.style.top = menuPositionY;

    folderMenu.style.left = menuPositionX;
    folderMenu.style.top = menuPositionY;

    folderMainMenu.style.left = menuPositionX;
    folderMainMenu.style.top = menuPositionY;

    newFolderMenu.style.left = menuPositionX;
    newFolderMenu.style.top = menuPositionY;

    deleteFolderMenu.style.left = menuPositionX;
    deleteFolderMenu.style.top = menuPositionY;

    newFileMenu.style.left = menuPositionX;
    newFileMenu.style.top = menuPositionY;

    deleteFileMenu.style.left = menuPositionX;
    deleteFileMenu.style.top = menuPositionY;

    editFileMenu.style.left = menuPositionX;
    editFileMenu.style.top = menuPositionY;
}

});
