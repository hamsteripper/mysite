<!DOCTYPE html>
<html>
<head>
    <title>Spreadsheet</title>
    <script src="../spreadsheet.php?parent=gridbox"></script>
</head>
<body>

    <?php

    require_once("api.php");
    require_once("config.php");

    try {
        $res = new PDO($dsn, $db_user, $db_pass);
        $sh = new SpreadSheet($res, "1", array("prefix"=>$db_prefix, "tick"=>$db_tick));
        $sh->setValue("B1", 1);
        $sh->setValue("B2", 2);
        $sh->setValue("B3", "=B2-2");
        $sh->setValue("B4", "=B3-3");

    } catch (\PDOException $exception) {

        die("Could not connect to database: ".$exception->getMessage());
    }

    ?>

    <div id="gridbox" style="width: 800px; height: 400px; background-color:white;"></div>
</body>
</html>




