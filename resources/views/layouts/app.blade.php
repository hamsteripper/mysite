<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hamsteripper</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script src="http://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>

    <link rel="shortcut icon" href="/images/graph.ico" type="image/x-icon">

    <style>

        html, body{
            height:100%;
            width:100%;

            background: #cccccc url("/images/4K-Snow-Wallpaper-UHD.jpg") fixed;

            background-size: cover;

        }


        .dropdown, .dropdown-menu{
            background: inherit;
            background-color: rgba(10,50,50,0.3);
            border-radius: 3px;
        }

        .menu{
            float:left;
        }
        .menu{
            margin:4em 2em;
        }
        .box{
            position:absolute;
            top:-1200px;
            width:100%;
            color:#fff;
            margin:auto;
            padding:0px;
            z-index:999999;
            text-align:right;
            left:0;
        }
        a.boxclose{
            cursor: pointer;
            text-align: center;
            display: block;
            position: absolute;
            top: 46px;
            right: 320px;
        }
        .menu_box_list {
            display: inline-block;
            float: right;
            margin-right: 35em;
        }
        .menu_box_list ul li {
            display:inline-block;
        }
        .menu_box_list li a{
            color: #fff;
            font-size: 1.2em;
            font-weight:400;
            display: block;
            padding:0.5em 0.5em;
            text-decoration:none;
            text-transform: uppercase;
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
            letter-spacing: 0.1em;
        }
        .menu_box_list li a:hover,.menu_box_list ul li.active a{
            color:#E74C3C;
        }
        .menu_box_list ul {
            background:transparent;
            padding:50px;
        }
        .menu_box_list li a > i > img{
            vertical-align:middle;
            padding-right:10px;
        }
        @media (max-width:1280px){
            a.boxclose {
                right: 190px;
            }
            .menu_box_list {
                margin-right: 25em;
            }
        }
        @media (max-width:1024px){
            a.boxclose {
                right: 104px;
            }
            .menu_box_list {
                margin-right: 14em;
            }
        }
        @media (max-width:768px){
            .menu_box_list {
                margin-right: 3em;
            }
            a.boxclose {
                right: 28px;
                top:18px;
            }
            .menu_box_list ul li {
                display:block;
            }.mt40{
            margin-top: 40px;
        }
            .menu_box_list ul {
                background:transparent;
                padding:1em 20em;
                margin:28px 0px;
                text-align:center;
            }
        }
        @media (max-width:640px){
            .menu_box_list ul {
                padding: 1em 15em;
            }
        }
        @media (max-width:480px){
            .menu_box_list ul {
                padding: 1em 9.5em;
            }
        }
        @media (max-width:320px){
            a.boxclose {
                right: 15px;
                top: 4px;
            }
            .menu_box_list ul {
                padding: 0em 5.2em;
                margin: 14px 0px;
            }
            .menu_box_list li a {
                padding: 0.4em 0.5em;
            }
            .menu_box_list {
                margin-right: 1em;
            }
        }


    </style>

    {{--@yield('head')--}}

    {{--@yield('css')--}}

</head>
<body>

<div class="menu">
    <a class="menu_activator" href="#" id="activator"><img src="http://bootstraptema.ru/snippets/menu/2016/button/menu.png" alt="" /></a>
    <div class="box" id="box">
        <div class="box_content">
            <div class="menu_box_list">
                <ul>
                    <li class="nav-item dropdown"><a href="/">Welcome</a></li>

                    <li class="nav-item dropdown "><a href="/home">Home</a></li>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Function <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/FilesConverter">Convert</a>
                            <a class="dropdown-item" href="/setting">Telegram Settings</a>
                            <a class="dropdown-item" href="/devicesDatabase">Devices Database</a>
                            {{--<a class="dropdown-item" href="/laravel-crud-search-sort-ajax">Crud </a>--}}
                            {{--<a class="dropdown-item" href="oneToOne/user">Один ко многим</a>--}}
                            <a class="dropdown-item" href="/searchInExcelFile/selectExcelFile?function=searchInExcel">Search In Excel</a>
                            {{--<a href="/searchInExcelFile/selectExcelFile?function=searchInExcel">Search in excel</a>--}}
                            <a class="dropdown-item" href="/test">test</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Log/Reg <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/login">Login</a>
                            <a class="dropdown-item" href="/register">Registrate</a>
                        </div>
                    </li>

                    {{--<li class="nav-item dropdown"><a href="/setting">Settings</a></li>--}}

                    <li class="nav-item dropdown"><a href="#">Info</a></li>

                    {{--Меню разлогинивания--}}
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest

                            <div class="clearfix"></div>
                </ul>
            </div>
            <a class="boxclose" id="boxclose"><img src="http://bootstraptema.ru/snippets/menu/2016/button/close.png" alt="" /></a>
        </div>
    </div>
</div>

@yield('content')

@yield('js')

<script>
    var $ = jQuery.noConflict();
    $(function() {
        $('#activator').click(function(){
            $('#box').animate({'top':'0px'},500);
        });
        $('#boxclose').click(function(){
            $('#box').animate({'top':'-700px'},500);
        });
    });
</script>

</body>
</html>
