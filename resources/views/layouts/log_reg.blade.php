<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hamsteripper</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('resource/lib/jquery/jquery.min.js')}}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="/images/graph.ico" type="image/x-icon">



    <style>

        html, body{
            height:100%;
            width:100%;

            background: #cccccc url("/images/4K-Snow-Wallpaper-UHD.jpg") fixed;

            background-size: cover;
        }

        input:-webkit-autofill,
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus,
        input:-webkit-autofill:active {
            -webkit-transition: color 9999s ease-out, background-color 9999s ease-out;
            -webkit-transition-delay: 9999s;
        }

        #email, #password, #name, #password-confirm{
            background-color: rgba(10,20,50,0.3);
            color: white;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            #000;*/
        }

        .card{
            background-color: rgba(10,50,50,0.4);
            color: white;
        }

        .background_inherit_struct{
            background: inherit !important;
        }

        .matovoye-steklo {
            /*width: 30rem;*/
            /*height: 20rem;*/
            border: 0px;
            box-shadow: 0 0 1rem 0 rgba(0, 0, 0, .2) !important;
            /*border-radius: 5px;*/
            position: relative; !important;
            z-index: 1 !important;
            background: inherit !important;
            overflow: hidden !important;
            /*margin: 0px !important;*/
        }

        .matovoye-steklo:before {
            content: "" !important;
            position: absolute !important;
            background: inherit !important;
            z-index: -1 !important;
            top: 0 !important;
            left: 0 !important;
            right: 0 !important;
            bottom: 0 !important;
            box-shadow: inset 0 0 2000px rgba(255, 255, 255, .5) !important;
            filter: blur(5px) !important;
            /*margin: -20px !important;*/
        }

        /*.card{*/
            /*background-color: rgba(10,50,50,0.7);*/
            /*color: white;*/
        /*}*/

    </style>

</head>
<body>

{{-- Страница предзагрузки css--}}
<link href="assets_css/preloader.css"  rel="stylesheet">
{{-- Страница предзагрузки html--}}
<div class="preloader">
    <div class="preloader__loader">
        <img src="/images/loader6.gif" alt="" />
    </div>
</div>
{{-- Страница предзагрузки js--}}
<script type="text/javascript" src="assets/preloader.js"></script>

@yield('content')

</body>
</html>
