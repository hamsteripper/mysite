<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hamsteripper</title>
    {{--Обязательный токен--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}"  rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('resource/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('resource/css/style-responsive.css')}}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!--jquery and bootstrap scripts-->
    <script src="{{ asset('resource/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('resource/lib/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--mainMenu scripts-->
    <script class="include" type="text/javascript" src="{{ asset('resource/lib/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{ asset('resource/lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script src="{{ asset('resource/lib/jquery.scrollTo.min.js')}}"></script>

    <script type="text/javascript" src="assets/toast.js"></script>

    <link rel="shortcut icon" href="/images/graph.ico" type="image/x-icon">

    <style>

        html, body{
            height:100%;
            width:100%;
            background: #cccccc url("/images/4K-Snow-Wallpaper-UHD.jpg") fixed;
            background-size: cover;

            z-index: -2 !important;
        }

        .btn {
            z-index: 0 !important;
        }

        .dropdown-item{
            z-index: 2 !important;
        }

        /*************************** Main menu styles**************************************/
        #sidebar, .header {
            /*background-color: rgba(10,50,50,0.4) !important;*/
            color: black !important;
        }

        #sidebar span{
            color: black !important;
        }

        #sidebar a{
            color: black !important;
            border-radius: 7px;
        }

        .sub1{
            color: black;
            margin: -2px 0 0;
            padding: 5%;
            border-radius: 7px;
        }

        .background_inherit_struct{
            background: inherit !important;
        }

        .bp{
            border: 10px solid black;
            padding: 1px;
            margin: 0px;
        }

        [contenteditable] {
            padding: 5px;
            border: 1px solid #eee;
            border-radius: 4px;
        }

        .matovoye-steklo {
            border: 0px !important;
            box-shadow: 0 0 1rem 0 rgba(0, 0, 0, .2) !important;
            position: relative; !important;
            z-index: 1 !important;
            background: inherit !important;
        }

        .matovoye-steklo:before {
            content: "" !important;
            position: absolute !important;
            background: inherit !important;
            z-index: -1 !important;
            top: 0 !important;
            left: 0 !important;
            right: 0 !important;
            bottom: 0 !important;
            box-shadow: inset 0 0 2000px rgba(255, 255, 255, .5) !important;
            filter: blur(5px) !important;
        }

        /*************************** Main menu styles**************************************/
    </style>

    <link href="assets_css/toast.css"  rel="stylesheet">

</head>
<body>

    {{-- Страница предзагрузки css--}}
    <link href="assets_css/preloader.css"  rel="stylesheet">
    {{-- Страница предзагрузки html--}}
    <div class="preloader">
        <div class="preloader__loader">
            <img src="/images/loader6.gif" alt="" />
        </div>
    </div>
    {{-- Страница предзагрузки js--}}
    <script type="text/javascript" src="assets/preloader.js"></script>

    {{--Подключение меню--}}
    @include('layouts.mainMenu')

    {{--Выпадающие сообщения--}}
    <section class="container-fluid">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 ">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 " style="position:static">
                <div class="pull-right" id="toast_container"></div>
            </div>
        </div>
    </section>

    {{--Главный контейнер 1--}}
    <section id="main-content" >
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 main-chart">
                    @yield('css2')
                    @yield('content2')
                    @yield('js2')
                </div>
            </div>
        </section>
    </section>

    {{--Главный контейнер 2--}}
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-10 main-chart">
                    @yield('content1')
                </div>
                <div class="col-lg-2">
                    @yield('rightBar1')
                </div>
            </div>
        </section>
    </section>

@yield('scripts')

<!--mainMenu scripts-->
<script src="{{ asset('resource/lib/common-scripts.js')}}"></script>

</body>
</html>
